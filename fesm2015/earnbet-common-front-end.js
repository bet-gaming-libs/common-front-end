import { trimString, getUrlParameterByName } from 'earnbet-common';

/**
 * @fileoverview added by tsickle
 * Generated from: lib/server/socket-client-base.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/**
 * @abstract
 */
class SocketClientBase {
    // CLASS members:
    // INSTANCE members:
    /**
     * @param {?} config
     * @param {?} nameSpace
     */
    constructor(config, nameSpace) {
        this.config = config;
        this.nameSpace = nameSpace;
        this.onConnect = (/**
         * @return {?}
         */
        () => {
            this.onConnected();
        });
        this.onError = (/**
         * @param {?} error
         * @return {?}
         */
        (error) => {
            console.log('onError');
            console.log(error);
        });
        this.onDisconnect = (/**
         * @return {?}
         */
        () => {
            this.onDisconnected();
        });
    }
    // *** ESTABLISH CONNECTION to SocketServer ***
    /**
     * @protected
     * @return {?}
     */
    connectSocket() {
        // connect scoket
        /** @type {?} */
        const url = 'http' +
            (this.config.ssl === true ? 's' : '') +
            '://' + this.config.host + ':' +
            this.config.port + this.nameSpace;
        /** @type {?} */
        const socket = io(url);
        socket.on('connect', this.onConnect);
        socket.on('disconnect', this.onConnect);
        socket.on('connect_error', this.onError);
        socket.on('error', this.onError);
        return socket;
    }
}
if (false) {
    /**
     * @type {?}
     * @private
     */
    SocketClientBase.prototype.onConnect;
    /**
     * @type {?}
     * @private
     */
    SocketClientBase.prototype.onError;
    /**
     * @type {?}
     * @private
     */
    SocketClientBase.prototype.onDisconnect;
    /**
     * @type {?}
     * @private
     */
    SocketClientBase.prototype.config;
    /**
     * @type {?}
     * @private
     */
    SocketClientBase.prototype.nameSpace;
    /**
     * @abstract
     * @protected
     * @return {?}
     */
    SocketClientBase.prototype.onConnected = function () { };
    /**
     * @abstract
     * @protected
     * @return {?}
     */
    SocketClientBase.prototype.onDisconnected = function () { };
}

/**
 * @fileoverview added by tsickle
 * Generated from: lib/util/buffer-util.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/**
 * @param {?} uint8arr
 * @return {?}
 */
function byteArrayToHexString(uint8arr) {
    if (!uint8arr) {
        return '';
    }
    /** @type {?} */
    var hexStr = '';
    for (var i = 0; i < uint8arr.length; i++) {
        /** @type {?} */
        var hex = (uint8arr[i] & 0xff).toString(16);
        hex = (hex.length === 1) ? '0' + hex : hex;
        hexStr += hex;
    }
    return hexStr.toUpperCase();
}
/**
 * @param {?} str
 * @return {?}
 */
function hexStringToByteArray(str) {
    if (!str) {
        return new Uint8Array(0);
    }
    /** @type {?} */
    var a = [];
    for (var i = 0, len = str.length; i < len; i += 2) {
        a.push(parseInt(str.substr(i, 2), 16));
    }
    return new Uint8Array(a);
}

/**
 * @fileoverview added by tsickle
 * Generated from: lib/util/local-storage-util.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/**
 * @template T
 */
class LocalStorageData {
    /**
     * @param {?} dataKey
     * @param {?} isObject
     */
    constructor(dataKey, isObject) {
        this.dataKey = dataKey;
        this.isObject = isObject;
    }
    /**
     * @return {?}
     */
    isSaved() {
        return this.get() != null;
    }
    /**
     * @return {?}
     */
    get() {
        /** @type {?} */
        const data = localStorage.getItem(this.dataKey);
        return data != null ?
            (this.isObject ?
                JSON.parse(data) :
                data) :
            null;
    }
    /**
     * @param {?} data
     * @return {?}
     */
    save(data) {
        localStorage.setItem(this.dataKey, (this.isObject ?
            JSON.stringify(data) :
            '' + data));
    }
}
if (false) {
    /**
     * @type {?}
     * @private
     */
    LocalStorageData.prototype.dataKey;
    /**
     * @type {?}
     * @private
     */
    LocalStorageData.prototype.isObject;
}

/**
 * @fileoverview added by tsickle
 * Generated from: lib/easy-account/model/data.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/** @type {?} */
const easyAccountDataKey = 'EasyAccountData';
/**
 * @record
 */
function IEasyAccountData() { }
if (false) {
    /** @type {?} */
    IEasyAccountData.prototype.id;
    /** @type {?} */
    IEasyAccountData.prototype.pub;
    /** @type {?} */
    IEasyAccountData.prototype.priv;
    /** @type {?} */
    IEasyAccountData.prototype.name;
    /** @type {?} */
    IEasyAccountData.prototype.isLoggedIn;
}
/**
 * @record
 */
function IEasyAccountTableRow() { }
if (false) {
    /** @type {?} */
    IEasyAccountTableRow.prototype.id;
    /** @type {?} */
    IEasyAccountTableRow.prototype.key;
    /** @type {?} */
    IEasyAccountTableRow.prototype.email_backup;
    /** @type {?} */
    IEasyAccountTableRow.prototype.nonce;
    /** @type {?} */
    IEasyAccountTableRow.prototype.bet_balance;
    /** @type {?} */
    IEasyAccountTableRow.prototype.eos_balance;
    /** @type {?} */
    IEasyAccountTableRow.prototype.eos_wagered;
    /** @type {?} */
    IEasyAccountTableRow.prototype.free_account;
    /** @type {?} */
    IEasyAccountTableRow.prototype.free_transfers;
    /** @type {?} */
    IEasyAccountTableRow.prototype.rewards_balance;
}

/**
 * @fileoverview added by tsickle
 * Generated from: lib/easy-account/data/api-data.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/**
 * @record
 */
function IEasyAccountDataForApi() { }
if (false) {
    /** @type {?} */
    IEasyAccountDataForApi.prototype.id;
    /** @type {?} */
    IEasyAccountDataForApi.prototype.publicKey;
    /** @type {?} */
    IEasyAccountDataForApi.prototype.name;
    /** @type {?} */
    IEasyAccountDataForApi.prototype.email;
    /** @type {?} */
    IEasyAccountDataForApi.prototype.recaptcha;
}

/**
 * @fileoverview added by tsickle
 * Generated from: lib/easy-account/model/forms/fields.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class StringValue {
    /**
     * @param {?} minimumLength
     * @param {?=} maximumLength
     * @param {?=} shouldTrim
     */
    constructor(minimumLength, maximumLength = 256, shouldTrim = false) {
        this.minimumLength = minimumLength;
        this.maximumLength = maximumLength;
        this.shouldTrim = shouldTrim;
        this.value = '';
        this._isInvalid = false;
    }
    /**
     * @return {?}
     */
    validate() {
        this._isInvalid = false;
        if (this.shouldTrim) {
            this.value = trimString(this.value);
        }
        /** @type {?} */
        const isValid = this.value.length >= this.minimumLength &&
            this.value.length <= this.maximumLength;
        this._isInvalid = !isValid;
        return isValid;
    }
    /**
     * @return {?}
     */
    get isInvalid() {
        return this._isInvalid;
    }
}
if (false) {
    /** @type {?} */
    StringValue.prototype.value;
    /**
     * @type {?}
     * @private
     */
    StringValue.prototype._isInvalid;
    /** @type {?} */
    StringValue.prototype.minimumLength;
    /** @type {?} */
    StringValue.prototype.maximumLength;
    /** @type {?} */
    StringValue.prototype.shouldTrim;
}
class RepeatedValue extends StringValue {
    /**
     * @param {?=} minimumLength
     */
    constructor(minimumLength = 1) {
        super(minimumLength);
        this.copy = '';
    }
    /**
     * @return {?}
     */
    validate() {
        return super.validate() &&
            this.value == this.copy;
    }
}
if (false) {
    /** @type {?} */
    RepeatedValue.prototype.copy;
}

/**
 * @fileoverview added by tsickle
 * Generated from: lib/easy-account/model/existing-account.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class ExistingEasyAccount {
    constructor() {
        /*
        setInterval(
            this.getDataFromChain,
            1000*60
        );
        */
    }
    /**
     * @return {?}
     */
    load() {
        this.localData = undefined;
        /** @type {?} */
        const savedAccountData = localStorage.getItem(easyAccountDataKey);
        if (savedAccountData != null) {
            this.localData = JSON.parse(savedAccountData);
        }
    }
    /*
        login(password:string):boolean {
            if (!this.isCreated) {
                return false;
            }
    
            this.password = password;
    
            const derivedPublicKey = eos_ecc.privateToPublic(
                this.privateKey
            );
    
            const isAuthenticated =
                derivedPublicKey == this.publicKey;
    
            this._isLoggedIn = isAuthenticated;
    
            return isAuthenticated;
        }
        */
    /**
     * @return {?}
     */
    login() {
        if (!this.isCreated) {
            return;
        }
        this.localData.isLoggedIn = true;
        this.save();
    }
    /**
     * @return {?}
     */
    logout() {
        if (!this.isCreated) {
            return;
        }
        this.localData.isLoggedIn = false;
        this.save();
    }
    /**
     * @private
     * @return {?}
     */
    save() {
        if (!this.isCreated) {
            return;
        }
        localStorage.setItem(easyAccountDataKey, JSON.stringify(this.localData));
    }
    /**
     * @return {?}
     */
    get eosBalance() {
        return this.onChainData ?
            this.onChainData.eos_balance :
            '0.0000 EOS';
    }
    /**
     * @return {?}
     */
    get id() {
        return this.isCreated ?
            this.localData.id :
            0;
    }
    /**
     * @return {?}
     */
    get name() {
        return this.localData ?
            this.localData.name :
            '';
    }
    /**
     * @return {?}
     */
    get publicKey() {
        return this.localData ?
            this.localData.pub :
            '';
    }
    /*
        get privateKey():string {
            if (!this.isCreated) {
                return '';
            }
    
            return AES.decrypt(
                this.encryptedPrivateKey,
                this.password
            );
        }
        private get encryptedPrivateKey() {
            return this.localData ?
                    this.localData.priv :
                    '';
        }
        */
    /**
     * @return {?}
     */
    get privateKey() {
        return this.localData ?
            this.localData.priv :
            '';
    }
    /**
     * @return {?}
     */
    get isLoggedIn() {
        return this.isCreated ?
            this.localData.isLoggedIn :
            false;
    }
    /**
     * @return {?}
     */
    get isCreated() {
        return this.localData != undefined &&
            this.localData.name != undefined;
    }
}
if (false) {
    /**
     * @type {?}
     * @private
     */
    ExistingEasyAccount.prototype.localData;
    /**
     * @type {?}
     * @private
     */
    ExistingEasyAccount.prototype.onChainData;
}

/**
 * @fileoverview added by tsickle
 * Generated from: lib/easy-account/model/account-manager.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class EasyAccountManager {
    constructor() {
        this.account = new ExistingEasyAccount();
        this.account.load();
    }
}
if (false) {
    /** @type {?} */
    EasyAccountManager.prototype.account;
}

/**
 * @fileoverview added by tsickle
 * Generated from: lib/translations/translation.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class TranslationId {
}
TranslationId.ENGLISH = 'EN';
TranslationId.CHINESE = 'CH';
TranslationId.KOREAN = 'KO';
TranslationId.JAPANESE = 'JP';
if (false) {
    /** @type {?} */
    TranslationId.ENGLISH;
    /** @type {?} */
    TranslationId.CHINESE;
    /** @type {?} */
    TranslationId.KOREAN;
    /** @type {?} */
    TranslationId.JAPANESE;
}
/**
 * @record
 */
function ITranslationBase() { }
if (false) {
    /** @type {?} */
    ITranslationBase.prototype.translationId;
    /** @type {?} */
    ITranslationBase.prototype.altForFlag;
}
/**
 * @record
 */
function ITranslationManager() { }
if (false) {
    /**
     * @return {?}
     */
    ITranslationManager.prototype.english = function () { };
    /**
     * @return {?}
     */
    ITranslationManager.prototype.chinese = function () { };
    /**
     * @return {?}
     */
    ITranslationManager.prototype.lynx = function () { };
}

/**
 * @fileoverview added by tsickle
 * Generated from: lib/translations/translation-manager.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class BrowserLanguageCode {
}
BrowserLanguageCode.ENGLISH = 'en';
BrowserLanguageCode.CHINESE = 'zh';
BrowserLanguageCode.KOREAN = 'ko';
BrowserLanguageCode.JAPANESE = 'ja';
if (false) {
    /** @type {?} */
    BrowserLanguageCode.ENGLISH;
    /** @type {?} */
    BrowserLanguageCode.CHINESE;
    /** @type {?} */
    BrowserLanguageCode.KOREAN;
    /** @type {?} */
    BrowserLanguageCode.JAPANESE;
}
/**
 * @abstract
 * @template T
 */
class TranslationManagerBase {
    constructor() {
        this.savedLanguage = new LocalStorageData('selectedLanguage', false);
    }
    /**
     * @return {?}
     */
    init() {
        /** @type {?} */
        const lang = getUrlParameterByName('lang');
        /** @type {?} */
        const isLanguageInUrl = lang != null;
        if (isLanguageInUrl) {
            this.selectLanguage(lang);
        }
        else {
            /** @type {?} */
            let lang = this.savedLanguage.get();
            if (lang == null) {
                lang = this.getBrowserLanguage();
            }
            this.selectLanguage(lang);
        }
    }
    /**
     * @private
     * @return {?}
     */
    getBrowserLanguage() {
        /** @type {?} */
        let lang = window.navigator.languages ? window.navigator.languages[0] : null;
        lang = lang || window.navigator.language ||
            window.navigator['browserLanguage'] ||
            window.navigator['userLanguage'];
        if (lang.indexOf('-') !== -1)
            lang = lang.split('-')[0];
        if (lang.indexOf('_') !== -1)
            lang = lang.split('_')[0];
        lang = lang.toLowerCase();
        /** @type {?} */
        let id = TranslationId.ENGLISH;
        switch (lang) {
            case BrowserLanguageCode.CHINESE:
                id = TranslationId.CHINESE;
                break;
            case BrowserLanguageCode.KOREAN:
                id = TranslationId.KOREAN;
                break;
            case BrowserLanguageCode.JAPANESE:
                id = TranslationId.JAPANESE;
                break;
        }
        return id;
    }
    /**
     * @param {?} languageId
     * @return {?}
     */
    selectLanguage(languageId) {
        switch (languageId) {
            case TranslationId.CHINESE:
                this.chinese();
                break;
            case TranslationId.KOREAN:
                this.korean();
                break;
            case TranslationId.JAPANESE:
                this.japanese();
                break;
            default:
                this.english();
        }
    }
    /**
     * @return {?}
     */
    english() {
        this._translation = this.getEnglish();
        this.savedLanguage.save(TranslationId.ENGLISH);
    }
    /**
     * @return {?}
     */
    chinese() {
        this._translation = this.getChinese();
        this.savedLanguage.save(TranslationId.CHINESE);
    }
    /**
     * @return {?}
     */
    korean() {
        this._translation = this.getKorean();
        this.savedLanguage.save(TranslationId.KOREAN);
    }
    /**
     * @return {?}
     */
    japanese() {
        this._translation = this.getJapanese();
        this.savedLanguage.save(TranslationId.JAPANESE);
    }
    /**
     * @return {?}
     */
    lynx() {
        this._translation = this.getLynx();
    }
    /**
     * @return {?}
     */
    get isEnglish() {
        return this.translation.translationId ==
            TranslationId.ENGLISH;
    }
    /**
     * @return {?}
     */
    get isChinese() {
        return this.translation.translationId ==
            TranslationId.CHINESE;
    }
    /**
     * @return {?}
     */
    get isKorean() {
        return this.translation.translationId ==
            TranslationId.KOREAN;
    }
    /**
     * @return {?}
     */
    get isJapanese() {
        return this.translation.translationId ==
            TranslationId.JAPANESE;
    }
    /**
     * @return {?}
     */
    get translation() {
        return this._translation;
    }
}
if (false) {
    /**
     * @type {?}
     * @private
     */
    TranslationManagerBase.prototype.savedLanguage;
    /**
     * @type {?}
     * @private
     */
    TranslationManagerBase.prototype._translation;
    /**
     * @abstract
     * @protected
     * @return {?}
     */
    TranslationManagerBase.prototype.getEnglish = function () { };
    /**
     * @abstract
     * @protected
     * @return {?}
     */
    TranslationManagerBase.prototype.getChinese = function () { };
    /**
     * @abstract
     * @protected
     * @return {?}
     */
    TranslationManagerBase.prototype.getKorean = function () { };
    /**
     * @abstract
     * @protected
     * @return {?}
     */
    TranslationManagerBase.prototype.getJapanese = function () { };
    /**
     * @abstract
     * @protected
     * @return {?}
     */
    TranslationManagerBase.prototype.getLynx = function () { };
}

/**
 * @fileoverview added by tsickle
 * Generated from: front-end-api.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */

/**
 * @fileoverview added by tsickle
 * Generated from: earnbet-common-front-end.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */

export { EasyAccountManager, ExistingEasyAccount, LocalStorageData, RepeatedValue, SocketClientBase, StringValue, TranslationId, TranslationManagerBase, byteArrayToHexString, easyAccountDataKey, hexStringToByteArray };
//# sourceMappingURL=earnbet-common-front-end.js.map
