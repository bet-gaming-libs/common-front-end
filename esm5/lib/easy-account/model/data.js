/**
 * @fileoverview added by tsickle
 * Generated from: lib/easy-account/model/data.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/** @type {?} */
export var easyAccountDataKey = 'EasyAccountData';
/**
 * @record
 */
export function IEasyAccountData() { }
if (false) {
    /** @type {?} */
    IEasyAccountData.prototype.id;
    /** @type {?} */
    IEasyAccountData.prototype.pub;
    /** @type {?} */
    IEasyAccountData.prototype.priv;
    /** @type {?} */
    IEasyAccountData.prototype.name;
    /** @type {?} */
    IEasyAccountData.prototype.isLoggedIn;
}
/**
 * @record
 */
export function IEasyAccountTableRow() { }
if (false) {
    /** @type {?} */
    IEasyAccountTableRow.prototype.id;
    /** @type {?} */
    IEasyAccountTableRow.prototype.key;
    /** @type {?} */
    IEasyAccountTableRow.prototype.email_backup;
    /** @type {?} */
    IEasyAccountTableRow.prototype.nonce;
    /** @type {?} */
    IEasyAccountTableRow.prototype.bet_balance;
    /** @type {?} */
    IEasyAccountTableRow.prototype.eos_balance;
    /** @type {?} */
    IEasyAccountTableRow.prototype.eos_wagered;
    /** @type {?} */
    IEasyAccountTableRow.prototype.free_account;
    /** @type {?} */
    IEasyAccountTableRow.prototype.free_transfers;
    /** @type {?} */
    IEasyAccountTableRow.prototype.rewards_balance;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZGF0YS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL2Vhcm5iZXQtY29tbW9uLWZyb250LWVuZC8iLCJzb3VyY2VzIjpbImxpYi9lYXN5LWFjY291bnQvbW9kZWwvZGF0YS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7QUFBQSxNQUFNLEtBQU8sa0JBQWtCLEdBQUcsaUJBQWlCOzs7O0FBRW5ELHNDQU9DOzs7SUFMRyw4QkFBVTs7SUFDViwrQkFBVzs7SUFDWCxnQ0FBWTs7SUFDWixnQ0FBWTs7SUFDWixzQ0FBbUI7Ozs7O0FBR3ZCLDBDQWlCQzs7O0lBZkcsa0NBQVU7O0lBQ1YsbUNBQVc7O0lBRVgsNENBQW9COztJQUVwQixxQ0FBYTs7SUFFYiwyQ0FBbUI7O0lBRW5CLDJDQUFtQjs7SUFDbkIsMkNBQW1COztJQUNuQiw0Q0FBb0I7O0lBQ3BCLDhDQUFzQjs7SUFFdEIsK0NBQXVCIiwic291cmNlc0NvbnRlbnQiOlsiZXhwb3J0IGNvbnN0IGVhc3lBY2NvdW50RGF0YUtleSA9ICdFYXN5QWNjb3VudERhdGEnO1xuXG5leHBvcnQgaW50ZXJmYWNlIElFYXN5QWNjb3VudERhdGFcbntcbiAgICBpZDpudW1iZXI7XG4gICAgcHViOnN0cmluZztcbiAgICBwcml2OnN0cmluZztcbiAgICBuYW1lOnN0cmluZztcbiAgICBpc0xvZ2dlZEluOmJvb2xlYW47XG59XG5cbmV4cG9ydCBpbnRlcmZhY2UgSUVhc3lBY2NvdW50VGFibGVSb3dcbntcbiAgICBpZDpzdHJpbmc7XG4gICAga2V5OnN0cmluZztcblxuICAgIGVtYWlsX2JhY2t1cDpudW1iZXI7XG5cbiAgICBub25jZTpudW1iZXI7XG5cbiAgICBiZXRfYmFsYW5jZTpzdHJpbmc7XG4gICAgXG4gICAgZW9zX2JhbGFuY2U6c3RyaW5nO1xuICAgIGVvc193YWdlcmVkOnN0cmluZztcbiAgICBmcmVlX2FjY291bnQ6bnVtYmVyO1xuICAgIGZyZWVfdHJhbnNmZXJzOm51bWJlcjtcbiAgIFxuICAgIHJld2FyZHNfYmFsYW5jZTpzdHJpbmc7XG59Il19