/**
 * @fileoverview added by tsickle
 * Generated from: lib/easy-account/model/forms/fields.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import * as tslib_1 from "tslib";
import { trimString } from "earnbet-common";
var StringValue = /** @class */ (function () {
    function StringValue(minimumLength, maximumLength, shouldTrim) {
        if (maximumLength === void 0) { maximumLength = 256; }
        if (shouldTrim === void 0) { shouldTrim = false; }
        this.minimumLength = minimumLength;
        this.maximumLength = maximumLength;
        this.shouldTrim = shouldTrim;
        this.value = '';
        this._isInvalid = false;
    }
    /**
     * @return {?}
     */
    StringValue.prototype.validate = /**
     * @return {?}
     */
    function () {
        this._isInvalid = false;
        if (this.shouldTrim) {
            this.value = trimString(this.value);
        }
        /** @type {?} */
        var isValid = this.value.length >= this.minimumLength &&
            this.value.length <= this.maximumLength;
        this._isInvalid = !isValid;
        return isValid;
    };
    Object.defineProperty(StringValue.prototype, "isInvalid", {
        get: /**
         * @return {?}
         */
        function () {
            return this._isInvalid;
        },
        enumerable: true,
        configurable: true
    });
    return StringValue;
}());
export { StringValue };
if (false) {
    /** @type {?} */
    StringValue.prototype.value;
    /**
     * @type {?}
     * @private
     */
    StringValue.prototype._isInvalid;
    /** @type {?} */
    StringValue.prototype.minimumLength;
    /** @type {?} */
    StringValue.prototype.maximumLength;
    /** @type {?} */
    StringValue.prototype.shouldTrim;
}
var RepeatedValue = /** @class */ (function (_super) {
    tslib_1.__extends(RepeatedValue, _super);
    function RepeatedValue(minimumLength) {
        if (minimumLength === void 0) { minimumLength = 1; }
        var _this = _super.call(this, minimumLength) || this;
        _this.copy = '';
        return _this;
    }
    /**
     * @return {?}
     */
    RepeatedValue.prototype.validate = /**
     * @return {?}
     */
    function () {
        return _super.prototype.validate.call(this) &&
            this.value == this.copy;
    };
    return RepeatedValue;
}(StringValue));
export { RepeatedValue };
if (false) {
    /** @type {?} */
    RepeatedValue.prototype.copy;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZmllbGRzLmpzIiwic291cmNlUm9vdCI6Im5nOi8vZWFybmJldC1jb21tb24tZnJvbnQtZW5kLyIsInNvdXJjZXMiOlsibGliL2Vhc3ktYWNjb3VudC9tb2RlbC9mb3Jtcy9maWVsZHMudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7O0FBQUEsT0FBTyxFQUFFLFVBQVUsRUFBRSxNQUFNLGdCQUFnQixDQUFDO0FBRzVDO0lBTUkscUJBQ2EsYUFBb0IsRUFDcEIsYUFBMEIsRUFDMUIsVUFBMEI7UUFEMUIsOEJBQUEsRUFBQSxtQkFBMEI7UUFDMUIsMkJBQUEsRUFBQSxrQkFBMEI7UUFGMUIsa0JBQWEsR0FBYixhQUFhLENBQU87UUFDcEIsa0JBQWEsR0FBYixhQUFhLENBQWE7UUFDMUIsZUFBVSxHQUFWLFVBQVUsQ0FBZ0I7UUFQdkMsVUFBSyxHQUFHLEVBQUUsQ0FBQztRQUVILGVBQVUsR0FBRyxLQUFLLENBQUM7SUFPM0IsQ0FBQzs7OztJQUVELDhCQUFROzs7SUFBUjtRQUVJLElBQUksQ0FBQyxVQUFVLEdBQUcsS0FBSyxDQUFDO1FBRXhCLElBQUksSUFBSSxDQUFDLFVBQVUsRUFBRTtZQUNqQixJQUFJLENBQUMsS0FBSyxHQUFHLFVBQVUsQ0FDbkIsSUFBSSxDQUFDLEtBQUssQ0FDYixDQUFDO1NBQ0w7O1lBRUssT0FBTyxHQUNULElBQUksQ0FBQyxLQUFLLENBQUMsTUFBTSxJQUFJLElBQUksQ0FBQyxhQUFhO1lBQ3ZDLElBQUksQ0FBQyxLQUFLLENBQUMsTUFBTSxJQUFJLElBQUksQ0FBQyxhQUFhO1FBRTNDLElBQUksQ0FBQyxVQUFVLEdBQUcsQ0FBQyxPQUFPLENBQUM7UUFFM0IsT0FBTyxPQUFPLENBQUM7SUFDbkIsQ0FBQztJQUVELHNCQUFJLGtDQUFTOzs7O1FBQWI7WUFDSSxPQUFPLElBQUksQ0FBQyxVQUFVLENBQUM7UUFDM0IsQ0FBQzs7O09BQUE7SUFDTCxrQkFBQztBQUFELENBQUMsQUFuQ0QsSUFtQ0M7Ozs7SUFqQ0csNEJBQVc7Ozs7O0lBRVgsaUNBQTJCOztJQUd2QixvQ0FBNkI7O0lBQzdCLG9DQUFtQzs7SUFDbkMsaUNBQW1DOztBQTZCM0M7SUFBbUMseUNBQVc7SUFJMUMsdUJBQVksYUFBaUI7UUFBakIsOEJBQUEsRUFBQSxpQkFBaUI7UUFBN0IsWUFDSSxrQkFBTSxhQUFhLENBQUMsU0FDdkI7UUFKRCxVQUFJLEdBQUcsRUFBRSxDQUFDOztJQUlWLENBQUM7Ozs7SUFFRCxnQ0FBUTs7O0lBQVI7UUFDSSxPQUFPLGlCQUFNLFFBQVEsV0FBRTtZQUNmLElBQUksQ0FBQyxLQUFLLElBQUksSUFBSSxDQUFDLElBQUksQ0FBQztJQUNwQyxDQUFDO0lBQ0wsb0JBQUM7QUFBRCxDQUFDLEFBWkQsQ0FBbUMsV0FBVyxHQVk3Qzs7OztJQVZHLDZCQUFVIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgdHJpbVN0cmluZyB9IGZyb20gXCJlYXJuYmV0LWNvbW1vblwiO1xuXG5cbmV4cG9ydCBjbGFzcyBTdHJpbmdWYWx1ZVxue1xuICAgIHZhbHVlID0gJyc7XG5cbiAgICBwcml2YXRlIF9pc0ludmFsaWQgPSBmYWxzZTtcblxuICAgIGNvbnN0cnVjdG9yKFxuICAgICAgICByZWFkb25seSBtaW5pbXVtTGVuZ3RoOm51bWJlcixcbiAgICAgICAgcmVhZG9ubHkgbWF4aW11bUxlbmd0aDpudW1iZXIgPSAyNTYsXG4gICAgICAgIHJlYWRvbmx5IHNob3VsZFRyaW06Ym9vbGVhbiA9IGZhbHNlXG4gICAgKSB7XG4gICAgfVxuXG4gICAgdmFsaWRhdGUoKTpib29sZWFuXG4gICAge1xuICAgICAgICB0aGlzLl9pc0ludmFsaWQgPSBmYWxzZTtcblxuICAgICAgICBpZiAodGhpcy5zaG91bGRUcmltKSB7XG4gICAgICAgICAgICB0aGlzLnZhbHVlID0gdHJpbVN0cmluZyhcbiAgICAgICAgICAgICAgICB0aGlzLnZhbHVlXG4gICAgICAgICAgICApO1xuICAgICAgICB9XG5cbiAgICAgICAgY29uc3QgaXNWYWxpZCA9IFxuICAgICAgICAgICAgdGhpcy52YWx1ZS5sZW5ndGggPj0gdGhpcy5taW5pbXVtTGVuZ3RoICYmXG4gICAgICAgICAgICB0aGlzLnZhbHVlLmxlbmd0aCA8PSB0aGlzLm1heGltdW1MZW5ndGhcbiAgICAgICAgXG4gICAgICAgIHRoaXMuX2lzSW52YWxpZCA9ICFpc1ZhbGlkO1xuXG4gICAgICAgIHJldHVybiBpc1ZhbGlkO1xuICAgIH1cblxuICAgIGdldCBpc0ludmFsaWQoKSB7XG4gICAgICAgIHJldHVybiB0aGlzLl9pc0ludmFsaWQ7XG4gICAgfVxufVxuXG5cbmV4cG9ydCBjbGFzcyBSZXBlYXRlZFZhbHVlIGV4dGVuZHMgU3RyaW5nVmFsdWVcbntcbiAgICBjb3B5ID0gJyc7XG5cbiAgICBjb25zdHJ1Y3RvcihtaW5pbXVtTGVuZ3RoID0gMSkge1xuICAgICAgICBzdXBlcihtaW5pbXVtTGVuZ3RoKTtcbiAgICB9XG5cbiAgICB2YWxpZGF0ZSgpOmJvb2xlYW4ge1xuICAgICAgICByZXR1cm4gc3VwZXIudmFsaWRhdGUoKSAmJlxuICAgICAgICAgICAgICAgIHRoaXMudmFsdWUgPT0gdGhpcy5jb3B5O1xuICAgIH1cbn0iXX0=