/**
 * @fileoverview added by tsickle
 * Generated from: lib/easy-account/model/existing-account.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { easyAccountDataKey } from "./data";
var ExistingEasyAccount = /** @class */ (function () {
    function ExistingEasyAccount() {
        /*
        setInterval(
            this.getDataFromChain,
            1000*60
        );
        */
    }
    /**
     * @return {?}
     */
    ExistingEasyAccount.prototype.load = /**
     * @return {?}
     */
    function () {
        this.localData = undefined;
        /** @type {?} */
        var savedAccountData = localStorage.getItem(easyAccountDataKey);
        if (savedAccountData != null) {
            this.localData = JSON.parse(savedAccountData);
        }
    };
    /*
    login(password:string):boolean {
        if (!this.isCreated) {
            return false;
        }

        this.password = password;

        const derivedPublicKey = eos_ecc.privateToPublic(
            this.privateKey
        );

        const isAuthenticated =
            derivedPublicKey == this.publicKey;

        this._isLoggedIn = isAuthenticated;

        return isAuthenticated;
    }
    */
    /*
        login(password:string):boolean {
            if (!this.isCreated) {
                return false;
            }
    
            this.password = password;
    
            const derivedPublicKey = eos_ecc.privateToPublic(
                this.privateKey
            );
    
            const isAuthenticated =
                derivedPublicKey == this.publicKey;
    
            this._isLoggedIn = isAuthenticated;
    
            return isAuthenticated;
        }
        */
    /**
     * @return {?}
     */
    ExistingEasyAccount.prototype.login = /*
        login(password:string):boolean {
            if (!this.isCreated) {
                return false;
            }
    
            this.password = password;
    
            const derivedPublicKey = eos_ecc.privateToPublic(
                this.privateKey
            );
    
            const isAuthenticated =
                derivedPublicKey == this.publicKey;
    
            this._isLoggedIn = isAuthenticated;
    
            return isAuthenticated;
        }
        */
    /**
     * @return {?}
     */
    function () {
        if (!this.isCreated) {
            return;
        }
        this.localData.isLoggedIn = true;
        this.save();
    };
    /**
     * @return {?}
     */
    ExistingEasyAccount.prototype.logout = /**
     * @return {?}
     */
    function () {
        if (!this.isCreated) {
            return;
        }
        this.localData.isLoggedIn = false;
        this.save();
    };
    /**
     * @private
     * @return {?}
     */
    ExistingEasyAccount.prototype.save = /**
     * @private
     * @return {?}
     */
    function () {
        if (!this.isCreated) {
            return;
        }
        localStorage.setItem(easyAccountDataKey, JSON.stringify(this.localData));
    };
    Object.defineProperty(ExistingEasyAccount.prototype, "eosBalance", {
        get: /**
         * @return {?}
         */
        function () {
            return this.onChainData ?
                this.onChainData.eos_balance :
                '0.0000 EOS';
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(ExistingEasyAccount.prototype, "id", {
        get: /**
         * @return {?}
         */
        function () {
            return this.isCreated ?
                this.localData.id :
                0;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(ExistingEasyAccount.prototype, "name", {
        get: /**
         * @return {?}
         */
        function () {
            return this.localData ?
                this.localData.name :
                '';
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(ExistingEasyAccount.prototype, "publicKey", {
        get: /**
         * @return {?}
         */
        function () {
            return this.localData ?
                this.localData.pub :
                '';
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(ExistingEasyAccount.prototype, "privateKey", {
        /*
        get privateKey():string {
            if (!this.isCreated) {
                return '';
            }
    
            return AES.decrypt(
                this.encryptedPrivateKey,
                this.password
            );
        }
        private get encryptedPrivateKey() {
            return this.localData ?
                    this.localData.priv :
                    '';
        }
        */
        get: /*
            get privateKey():string {
                if (!this.isCreated) {
                    return '';
                }
        
                return AES.decrypt(
                    this.encryptedPrivateKey,
                    this.password
                );
            }
            private get encryptedPrivateKey() {
                return this.localData ?
                        this.localData.priv :
                        '';
            }
            */
        /**
         * @return {?}
         */
        function () {
            return this.localData ?
                this.localData.priv :
                '';
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(ExistingEasyAccount.prototype, "isLoggedIn", {
        get: /**
         * @return {?}
         */
        function () {
            return this.isCreated ?
                this.localData.isLoggedIn :
                false;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(ExistingEasyAccount.prototype, "isCreated", {
        get: /**
         * @return {?}
         */
        function () {
            return this.localData != undefined &&
                this.localData.name != undefined;
        },
        enumerable: true,
        configurable: true
    });
    return ExistingEasyAccount;
}());
export { ExistingEasyAccount };
if (false) {
    /**
     * @type {?}
     * @private
     */
    ExistingEasyAccount.prototype.localData;
    /**
     * @type {?}
     * @private
     */
    ExistingEasyAccount.prototype.onChainData;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZXhpc3RpbmctYWNjb3VudC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL2Vhcm5iZXQtY29tbW9uLWZyb250LWVuZC8iLCJzb3VyY2VzIjpbImxpYi9lYXN5LWFjY291bnQvbW9kZWwvZXhpc3RpbmctYWNjb3VudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7OztBQUFBLE9BQU8sRUFBRSxrQkFBa0IsRUFBMEMsTUFDNUQsUUFBUSxDQUFDO0FBR2xCO0lBT0k7UUFDSTs7Ozs7VUFLRTtJQUNOLENBQUM7Ozs7SUFFRCxrQ0FBSTs7O0lBQUo7UUFDSSxJQUFJLENBQUMsU0FBUyxHQUFHLFNBQVMsQ0FBQzs7WUFHckIsZ0JBQWdCLEdBQ2xCLFlBQVksQ0FBQyxPQUFPLENBQUMsa0JBQWtCLENBQUM7UUFFNUMsSUFBSSxnQkFBZ0IsSUFBSSxJQUFJLEVBQUU7WUFDMUIsSUFBSSxDQUFDLFNBQVMsR0FBRyxJQUFJLENBQUMsS0FBSyxDQUN2QixnQkFBZ0IsQ0FDbkIsQ0FBQztTQUNMO0lBQ0wsQ0FBQztJQUVEOzs7Ozs7Ozs7Ozs7Ozs7Ozs7O01BbUJFOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7SUFDRixtQ0FBSzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7SUFBTDtRQUNJLElBQUksQ0FBQyxJQUFJLENBQUMsU0FBUyxFQUFFO1lBQ2pCLE9BQU87U0FDVjtRQUdELElBQUksQ0FBQyxTQUFTLENBQUMsVUFBVSxHQUFHLElBQUksQ0FBQztRQUNqQyxJQUFJLENBQUMsSUFBSSxFQUFFLENBQUM7SUFDaEIsQ0FBQzs7OztJQUNELG9DQUFNOzs7SUFBTjtRQUNJLElBQUksQ0FBQyxJQUFJLENBQUMsU0FBUyxFQUFFO1lBQ2pCLE9BQU87U0FDVjtRQUdELElBQUksQ0FBQyxTQUFTLENBQUMsVUFBVSxHQUFHLEtBQUssQ0FBQztRQUNsQyxJQUFJLENBQUMsSUFBSSxFQUFFLENBQUM7SUFDaEIsQ0FBQzs7Ozs7SUFFTyxrQ0FBSTs7OztJQUFaO1FBQ0ksSUFBSSxDQUFDLElBQUksQ0FBQyxTQUFTLEVBQUU7WUFDakIsT0FBTztTQUNWO1FBR0QsWUFBWSxDQUFDLE9BQU8sQ0FDaEIsa0JBQWtCLEVBQ2xCLElBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxDQUNqQyxDQUFDO0lBQ04sQ0FBQztJQUdELHNCQUFJLDJDQUFVOzs7O1FBQWQ7WUFDSSxPQUFPLElBQUksQ0FBQyxXQUFXLENBQUMsQ0FBQztnQkFDakIsSUFBSSxDQUFDLFdBQVcsQ0FBQyxXQUFXLENBQUMsQ0FBQztnQkFDOUIsWUFBWSxDQUFDO1FBQ3pCLENBQUM7OztPQUFBO0lBR0Qsc0JBQUksbUNBQUU7Ozs7UUFBTjtZQUNJLE9BQU8sSUFBSSxDQUFDLFNBQVMsQ0FBQyxDQUFDO2dCQUNmLElBQUksQ0FBQyxTQUFTLENBQUMsRUFBRSxDQUFDLENBQUM7Z0JBQ25CLENBQUMsQ0FBQztRQUNkLENBQUM7OztPQUFBO0lBRUQsc0JBQUkscUNBQUk7Ozs7UUFBUjtZQUNJLE9BQU8sSUFBSSxDQUFDLFNBQVMsQ0FBQyxDQUFDO2dCQUNmLElBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLENBQUM7Z0JBQ3JCLEVBQUUsQ0FBQztRQUNmLENBQUM7OztPQUFBO0lBRUQsc0JBQUksMENBQVM7Ozs7UUFBYjtZQUNJLE9BQU8sSUFBSSxDQUFDLFNBQVMsQ0FBQyxDQUFDO2dCQUNmLElBQUksQ0FBQyxTQUFTLENBQUMsR0FBRyxDQUFDLENBQUM7Z0JBQ3BCLEVBQUUsQ0FBQztRQUNmLENBQUM7OztPQUFBO0lBbUJELHNCQUFJLDJDQUFVO1FBakJkOzs7Ozs7Ozs7Ozs7Ozs7O1VBZ0JFOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7UUFDRjtZQUNJLE9BQU8sSUFBSSxDQUFDLFNBQVMsQ0FBQyxDQUFDO2dCQUNmLElBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLENBQUM7Z0JBQ3JCLEVBQUUsQ0FBQztRQUNmLENBQUM7OztPQUFBO0lBRUQsc0JBQUksMkNBQVU7Ozs7UUFBZDtZQUNJLE9BQU8sSUFBSSxDQUFDLFNBQVMsQ0FBQyxDQUFDO2dCQUNmLElBQUksQ0FBQyxTQUFTLENBQUMsVUFBVSxDQUFDLENBQUM7Z0JBQzNCLEtBQUssQ0FBQztRQUNsQixDQUFDOzs7T0FBQTtJQUVELHNCQUFJLDBDQUFTOzs7O1FBQWI7WUFFSSxPQUFPLElBQUksQ0FBQyxTQUFTLElBQUksU0FBUztnQkFDMUIsSUFBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJLElBQUksU0FBUyxDQUFDO1FBQzdDLENBQUM7OztPQUFBO0lBQ0wsMEJBQUM7QUFBRCxDQUFDLEFBN0lELElBNklDOzs7Ozs7O0lBeklHLHdDQUFtQzs7Ozs7SUFDbkMsMENBQXlDIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgZWFzeUFjY291bnREYXRhS2V5LCBJRWFzeUFjY291bnREYXRhLCBJRWFzeUFjY291bnRUYWJsZVJvdyB9IFxuICAgIGZyb20gXCIuL2RhdGFcIjtcblxuXG5leHBvcnQgY2xhc3MgRXhpc3RpbmdFYXN5QWNjb3VudFxue1xuICAgIC8vcHJpdmF0ZSBwYXNzd29yZDpzdHJpbmc7XG5cbiAgICBwcml2YXRlIGxvY2FsRGF0YTpJRWFzeUFjY291bnREYXRhO1xuICAgIHByaXZhdGUgb25DaGFpbkRhdGE6SUVhc3lBY2NvdW50VGFibGVSb3c7XG5cbiAgICBjb25zdHJ1Y3RvcigpIHtcbiAgICAgICAgLypcbiAgICAgICAgc2V0SW50ZXJ2YWwoXG4gICAgICAgICAgICB0aGlzLmdldERhdGFGcm9tQ2hhaW4sXG4gICAgICAgICAgICAxMDAwKjYwXG4gICAgICAgICk7XG4gICAgICAgICovXG4gICAgfVxuXG4gICAgbG9hZCgpIHtcbiAgICAgICAgdGhpcy5sb2NhbERhdGEgPSB1bmRlZmluZWQ7XG5cblxuICAgICAgICBjb25zdCBzYXZlZEFjY291bnREYXRhID0gXG4gICAgICAgICAgICBsb2NhbFN0b3JhZ2UuZ2V0SXRlbShlYXN5QWNjb3VudERhdGFLZXkpO1xuXG4gICAgICAgIGlmIChzYXZlZEFjY291bnREYXRhICE9IG51bGwpIHtcbiAgICAgICAgICAgIHRoaXMubG9jYWxEYXRhID0gSlNPTi5wYXJzZShcbiAgICAgICAgICAgICAgICBzYXZlZEFjY291bnREYXRhXG4gICAgICAgICAgICApO1xuICAgICAgICB9XG4gICAgfVxuXG4gICAgLypcbiAgICBsb2dpbihwYXNzd29yZDpzdHJpbmcpOmJvb2xlYW4ge1xuICAgICAgICBpZiAoIXRoaXMuaXNDcmVhdGVkKSB7XG4gICAgICAgICAgICByZXR1cm4gZmFsc2U7XG4gICAgICAgIH1cblxuICAgICAgICB0aGlzLnBhc3N3b3JkID0gcGFzc3dvcmQ7XG5cbiAgICAgICAgY29uc3QgZGVyaXZlZFB1YmxpY0tleSA9IGVvc19lY2MucHJpdmF0ZVRvUHVibGljKFxuICAgICAgICAgICAgdGhpcy5wcml2YXRlS2V5XG4gICAgICAgICk7XG5cbiAgICAgICAgY29uc3QgaXNBdXRoZW50aWNhdGVkID0gXG4gICAgICAgICAgICBkZXJpdmVkUHVibGljS2V5ID09IHRoaXMucHVibGljS2V5O1xuXG4gICAgICAgIHRoaXMuX2lzTG9nZ2VkSW4gPSBpc0F1dGhlbnRpY2F0ZWQ7XG5cbiAgICAgICAgcmV0dXJuIGlzQXV0aGVudGljYXRlZDtcbiAgICB9XG4gICAgKi9cbiAgICBsb2dpbigpIHtcbiAgICAgICAgaWYgKCF0aGlzLmlzQ3JlYXRlZCkge1xuICAgICAgICAgICAgcmV0dXJuO1xuICAgICAgICB9XG5cblxuICAgICAgICB0aGlzLmxvY2FsRGF0YS5pc0xvZ2dlZEluID0gdHJ1ZTtcbiAgICAgICAgdGhpcy5zYXZlKCk7XG4gICAgfVxuICAgIGxvZ291dCgpIHtcbiAgICAgICAgaWYgKCF0aGlzLmlzQ3JlYXRlZCkge1xuICAgICAgICAgICAgcmV0dXJuO1xuICAgICAgICB9XG5cblxuICAgICAgICB0aGlzLmxvY2FsRGF0YS5pc0xvZ2dlZEluID0gZmFsc2U7XG4gICAgICAgIHRoaXMuc2F2ZSgpO1xuICAgIH1cblxuICAgIHByaXZhdGUgc2F2ZSgpIHtcbiAgICAgICAgaWYgKCF0aGlzLmlzQ3JlYXRlZCkge1xuICAgICAgICAgICAgcmV0dXJuO1xuICAgICAgICB9XG5cblxuICAgICAgICBsb2NhbFN0b3JhZ2Uuc2V0SXRlbShcbiAgICAgICAgICAgIGVhc3lBY2NvdW50RGF0YUtleSxcbiAgICAgICAgICAgIEpTT04uc3RyaW5naWZ5KHRoaXMubG9jYWxEYXRhKVxuICAgICAgICApO1xuICAgIH1cblxuICAgIFxuICAgIGdldCBlb3NCYWxhbmNlKCkge1xuICAgICAgICByZXR1cm4gdGhpcy5vbkNoYWluRGF0YSA/XG4gICAgICAgICAgICAgICAgdGhpcy5vbkNoYWluRGF0YS5lb3NfYmFsYW5jZSA6XG4gICAgICAgICAgICAgICAgJzAuMDAwMCBFT1MnO1xuICAgIH1cblxuXG4gICAgZ2V0IGlkKCkge1xuICAgICAgICByZXR1cm4gdGhpcy5pc0NyZWF0ZWQgP1xuICAgICAgICAgICAgICAgIHRoaXMubG9jYWxEYXRhLmlkIDpcbiAgICAgICAgICAgICAgICAwO1xuICAgIH1cblxuICAgIGdldCBuYW1lKCkge1xuICAgICAgICByZXR1cm4gdGhpcy5sb2NhbERhdGEgP1xuICAgICAgICAgICAgICAgIHRoaXMubG9jYWxEYXRhLm5hbWUgOlxuICAgICAgICAgICAgICAgICcnO1xuICAgIH1cblxuICAgIGdldCBwdWJsaWNLZXkoKSB7XG4gICAgICAgIHJldHVybiB0aGlzLmxvY2FsRGF0YSA/XG4gICAgICAgICAgICAgICAgdGhpcy5sb2NhbERhdGEucHViIDpcbiAgICAgICAgICAgICAgICAnJztcbiAgICB9XG5cbiAgICAvKlxuICAgIGdldCBwcml2YXRlS2V5KCk6c3RyaW5nIHtcbiAgICAgICAgaWYgKCF0aGlzLmlzQ3JlYXRlZCkge1xuICAgICAgICAgICAgcmV0dXJuICcnO1xuICAgICAgICB9XG5cbiAgICAgICAgcmV0dXJuIEFFUy5kZWNyeXB0KFxuICAgICAgICAgICAgdGhpcy5lbmNyeXB0ZWRQcml2YXRlS2V5LFxuICAgICAgICAgICAgdGhpcy5wYXNzd29yZFxuICAgICAgICApO1xuICAgIH1cbiAgICBwcml2YXRlIGdldCBlbmNyeXB0ZWRQcml2YXRlS2V5KCkge1xuICAgICAgICByZXR1cm4gdGhpcy5sb2NhbERhdGEgP1xuICAgICAgICAgICAgICAgIHRoaXMubG9jYWxEYXRhLnByaXYgOlxuICAgICAgICAgICAgICAgICcnO1xuICAgIH1cbiAgICAqL1xuICAgIGdldCBwcml2YXRlS2V5KCkge1xuICAgICAgICByZXR1cm4gdGhpcy5sb2NhbERhdGEgP1xuICAgICAgICAgICAgICAgIHRoaXMubG9jYWxEYXRhLnByaXYgOlxuICAgICAgICAgICAgICAgICcnO1xuICAgIH1cblxuICAgIGdldCBpc0xvZ2dlZEluKCkge1xuICAgICAgICByZXR1cm4gdGhpcy5pc0NyZWF0ZWQgP1xuICAgICAgICAgICAgICAgIHRoaXMubG9jYWxEYXRhLmlzTG9nZ2VkSW4gOlxuICAgICAgICAgICAgICAgIGZhbHNlO1xuICAgIH1cblxuICAgIGdldCBpc0NyZWF0ZWQoKTpib29sZWFuXG4gICAge1xuICAgICAgICByZXR1cm4gdGhpcy5sb2NhbERhdGEgIT0gdW5kZWZpbmVkICYmXG4gICAgICAgICAgICAgICAgdGhpcy5sb2NhbERhdGEubmFtZSAhPSB1bmRlZmluZWQ7XG4gICAgfVxufSJdfQ==