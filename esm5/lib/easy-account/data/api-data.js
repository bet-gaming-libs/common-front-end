/**
 * @fileoverview added by tsickle
 * Generated from: lib/easy-account/data/api-data.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/**
 * @record
 */
export function IEasyAccountDataForApi() { }
if (false) {
    /** @type {?} */
    IEasyAccountDataForApi.prototype.id;
    /** @type {?} */
    IEasyAccountDataForApi.prototype.publicKey;
    /** @type {?} */
    IEasyAccountDataForApi.prototype.name;
    /** @type {?} */
    IEasyAccountDataForApi.prototype.email;
    /** @type {?} */
    IEasyAccountDataForApi.prototype.recaptcha;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYXBpLWRhdGEuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9lYXJuYmV0LWNvbW1vbi1mcm9udC1lbmQvIiwic291cmNlcyI6WyJsaWIvZWFzeS1hY2NvdW50L2RhdGEvYXBpLWRhdGEudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7QUFBQSw0Q0FRQzs7O0lBTkcsb0NBQVU7O0lBQ1YsMkNBQWlCOztJQUNqQixzQ0FBWTs7SUFFWix1Q0FBYTs7SUFDYiwyQ0FBaUIiLCJzb3VyY2VzQ29udGVudCI6WyJleHBvcnQgaW50ZXJmYWNlIElFYXN5QWNjb3VudERhdGFGb3JBcGlcbntcbiAgICBpZDpudW1iZXI7XG4gICAgcHVibGljS2V5OnN0cmluZztcbiAgICBuYW1lOnN0cmluZztcbiAgICBcbiAgICBlbWFpbDpzdHJpbmc7XG4gICAgcmVjYXB0Y2hhOnN0cmluZztcbn0iXX0=