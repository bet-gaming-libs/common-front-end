/**
 * @fileoverview added by tsickle
 * Generated from: lib/translations/translation-manager.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { getUrlParameterByName } from "earnbet-common";
import { TranslationId } from "./translation";
import { LocalStorageData } from "../util/local-storage-util";
var BrowserLanguageCode = /** @class */ (function () {
    function BrowserLanguageCode() {
    }
    BrowserLanguageCode.ENGLISH = 'en';
    BrowserLanguageCode.CHINESE = 'zh';
    BrowserLanguageCode.KOREAN = 'ko';
    BrowserLanguageCode.JAPANESE = 'ja';
    return BrowserLanguageCode;
}());
if (false) {
    /** @type {?} */
    BrowserLanguageCode.ENGLISH;
    /** @type {?} */
    BrowserLanguageCode.CHINESE;
    /** @type {?} */
    BrowserLanguageCode.KOREAN;
    /** @type {?} */
    BrowserLanguageCode.JAPANESE;
}
/**
 * @abstract
 * @template T
 */
var /**
 * @abstract
 * @template T
 */
TranslationManagerBase = /** @class */ (function () {
    function TranslationManagerBase() {
        this.savedLanguage = new LocalStorageData('selectedLanguage', false);
    }
    /**
     * @return {?}
     */
    TranslationManagerBase.prototype.init = /**
     * @return {?}
     */
    function () {
        /** @type {?} */
        var lang = getUrlParameterByName('lang');
        /** @type {?} */
        var isLanguageInUrl = lang != null;
        if (isLanguageInUrl) {
            this.selectLanguage(lang);
        }
        else {
            /** @type {?} */
            var lang_1 = this.savedLanguage.get();
            if (lang_1 == null) {
                lang_1 = this.getBrowserLanguage();
            }
            this.selectLanguage(lang_1);
        }
    };
    /**
     * @private
     * @return {?}
     */
    TranslationManagerBase.prototype.getBrowserLanguage = /**
     * @private
     * @return {?}
     */
    function () {
        /** @type {?} */
        var lang = window.navigator.languages ? window.navigator.languages[0] : null;
        lang = lang || window.navigator.language ||
            window.navigator['browserLanguage'] ||
            window.navigator['userLanguage'];
        if (lang.indexOf('-') !== -1)
            lang = lang.split('-')[0];
        if (lang.indexOf('_') !== -1)
            lang = lang.split('_')[0];
        lang = lang.toLowerCase();
        /** @type {?} */
        var id = TranslationId.ENGLISH;
        switch (lang) {
            case BrowserLanguageCode.CHINESE:
                id = TranslationId.CHINESE;
                break;
            case BrowserLanguageCode.KOREAN:
                id = TranslationId.KOREAN;
                break;
            case BrowserLanguageCode.JAPANESE:
                id = TranslationId.JAPANESE;
                break;
        }
        return id;
    };
    /**
     * @param {?} languageId
     * @return {?}
     */
    TranslationManagerBase.prototype.selectLanguage = /**
     * @param {?} languageId
     * @return {?}
     */
    function (languageId) {
        switch (languageId) {
            case TranslationId.CHINESE:
                this.chinese();
                break;
            case TranslationId.KOREAN:
                this.korean();
                break;
            case TranslationId.JAPANESE:
                this.japanese();
                break;
            default:
                this.english();
        }
    };
    /**
     * @return {?}
     */
    TranslationManagerBase.prototype.english = /**
     * @return {?}
     */
    function () {
        this._translation = this.getEnglish();
        this.savedLanguage.save(TranslationId.ENGLISH);
    };
    /**
     * @return {?}
     */
    TranslationManagerBase.prototype.chinese = /**
     * @return {?}
     */
    function () {
        this._translation = this.getChinese();
        this.savedLanguage.save(TranslationId.CHINESE);
    };
    /**
     * @return {?}
     */
    TranslationManagerBase.prototype.korean = /**
     * @return {?}
     */
    function () {
        this._translation = this.getKorean();
        this.savedLanguage.save(TranslationId.KOREAN);
    };
    /**
     * @return {?}
     */
    TranslationManagerBase.prototype.japanese = /**
     * @return {?}
     */
    function () {
        this._translation = this.getJapanese();
        this.savedLanguage.save(TranslationId.JAPANESE);
    };
    /**
     * @return {?}
     */
    TranslationManagerBase.prototype.lynx = /**
     * @return {?}
     */
    function () {
        this._translation = this.getLynx();
    };
    Object.defineProperty(TranslationManagerBase.prototype, "isEnglish", {
        get: /**
         * @return {?}
         */
        function () {
            return this.translation.translationId ==
                TranslationId.ENGLISH;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(TranslationManagerBase.prototype, "isChinese", {
        get: /**
         * @return {?}
         */
        function () {
            return this.translation.translationId ==
                TranslationId.CHINESE;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(TranslationManagerBase.prototype, "isKorean", {
        get: /**
         * @return {?}
         */
        function () {
            return this.translation.translationId ==
                TranslationId.KOREAN;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(TranslationManagerBase.prototype, "isJapanese", {
        get: /**
         * @return {?}
         */
        function () {
            return this.translation.translationId ==
                TranslationId.JAPANESE;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(TranslationManagerBase.prototype, "translation", {
        get: /**
         * @return {?}
         */
        function () {
            return this._translation;
        },
        enumerable: true,
        configurable: true
    });
    return TranslationManagerBase;
}());
/**
 * @abstract
 * @template T
 */
export { TranslationManagerBase };
if (false) {
    /**
     * @type {?}
     * @private
     */
    TranslationManagerBase.prototype.savedLanguage;
    /**
     * @type {?}
     * @private
     */
    TranslationManagerBase.prototype._translation;
    /**
     * @abstract
     * @protected
     * @return {?}
     */
    TranslationManagerBase.prototype.getEnglish = function () { };
    /**
     * @abstract
     * @protected
     * @return {?}
     */
    TranslationManagerBase.prototype.getChinese = function () { };
    /**
     * @abstract
     * @protected
     * @return {?}
     */
    TranslationManagerBase.prototype.getKorean = function () { };
    /**
     * @abstract
     * @protected
     * @return {?}
     */
    TranslationManagerBase.prototype.getJapanese = function () { };
    /**
     * @abstract
     * @protected
     * @return {?}
     */
    TranslationManagerBase.prototype.getLynx = function () { };
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidHJhbnNsYXRpb24tbWFuYWdlci5qcyIsInNvdXJjZVJvb3QiOiJuZzovL2Vhcm5iZXQtY29tbW9uLWZyb250LWVuZC8iLCJzb3VyY2VzIjpbImxpYi90cmFuc2xhdGlvbnMvdHJhbnNsYXRpb24tbWFuYWdlci50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7OztBQUFBLE9BQU8sRUFBRSxxQkFBcUIsRUFBRSxNQUFNLGdCQUFnQixDQUFDO0FBRXZELE9BQU8sRUFBRSxhQUFhLEVBQXlDLE1BQU0sZUFBZSxDQUFDO0FBQ3JGLE9BQU8sRUFBRSxnQkFBZ0IsRUFBRSxNQUFNLDRCQUE0QixDQUFDO0FBRzlEO0lBQUE7SUFNQSxDQUFDO0lBSlUsMkJBQU8sR0FBRyxJQUFJLENBQUM7SUFDZiwyQkFBTyxHQUFHLElBQUksQ0FBQztJQUNmLDBCQUFNLEdBQUcsSUFBSSxDQUFDO0lBQ2QsNEJBQVEsR0FBRyxJQUFJLENBQUM7SUFDM0IsMEJBQUM7Q0FBQSxBQU5ELElBTUM7OztJQUpHLDRCQUFzQjs7SUFDdEIsNEJBQXNCOztJQUN0QiwyQkFBcUI7O0lBQ3JCLDZCQUF1Qjs7Ozs7O0FBSTNCOzs7OztJQVVJO1FBUFEsa0JBQWEsR0FBNEIsSUFBSSxnQkFBZ0IsQ0FDakUsa0JBQWtCLEVBQ2xCLEtBQUssQ0FDUixDQUFDO0lBS0YsQ0FBQzs7OztJQUNELHFDQUFJOzs7SUFBSjs7WUFDVSxJQUFJLEdBQUcscUJBQXFCLENBQUMsTUFBTSxDQUFDOztZQUVwQyxlQUFlLEdBQUcsSUFBSSxJQUFJLElBQUk7UUFFcEMsSUFBSSxlQUFlLEVBQUU7WUFDakIsSUFBSSxDQUFDLGNBQWMsQ0FBQyxJQUFJLENBQUMsQ0FBQztTQUM3QjthQUFNOztnQkFDQyxNQUFJLEdBQUcsSUFBSSxDQUFDLGFBQWEsQ0FBQyxHQUFHLEVBQUU7WUFFbkMsSUFBSSxNQUFJLElBQUksSUFBSSxFQUFFO2dCQUNkLE1BQUksR0FBRyxJQUFJLENBQUMsa0JBQWtCLEVBQUUsQ0FBQzthQUNwQztZQUVELElBQUksQ0FBQyxjQUFjLENBQUMsTUFBSSxDQUFDLENBQUM7U0FDN0I7SUFDTCxDQUFDOzs7OztJQUVPLG1EQUFrQjs7OztJQUExQjs7WUFDUSxJQUFJLEdBQUcsTUFBTSxDQUFDLFNBQVMsQ0FBQyxTQUFTLENBQUMsQ0FBQyxDQUFDLE1BQU0sQ0FBQyxTQUFTLENBQUMsU0FBUyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJO1FBRTVFLElBQUksR0FBRyxJQUFJLElBQUksTUFBTSxDQUFDLFNBQVMsQ0FBQyxRQUFRO1lBQ2hDLE1BQU0sQ0FBQyxTQUFTLENBQUMsaUJBQWlCLENBQUM7WUFDbkMsTUFBTSxDQUFDLFNBQVMsQ0FBQyxjQUFjLENBQUMsQ0FBQztRQUV6QyxJQUFJLElBQUksQ0FBQyxPQUFPLENBQUMsR0FBRyxDQUFDLEtBQUssQ0FBQyxDQUFDO1lBQ3hCLElBQUksR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDO1FBRTlCLElBQUksSUFBSSxDQUFDLE9BQU8sQ0FBQyxHQUFHLENBQUMsS0FBSyxDQUFDLENBQUM7WUFDeEIsSUFBSSxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7UUFFOUIsSUFBSSxHQUFHLElBQUksQ0FBQyxXQUFXLEVBQUUsQ0FBQzs7WUFHdEIsRUFBRSxHQUFHLGFBQWEsQ0FBQyxPQUFPO1FBRTlCLFFBQVEsSUFBSSxFQUFFO1lBQ1YsS0FBSyxtQkFBbUIsQ0FBQyxPQUFPO2dCQUM1QixFQUFFLEdBQUcsYUFBYSxDQUFDLE9BQU8sQ0FBQztnQkFDL0IsTUFBTTtZQUVOLEtBQUssbUJBQW1CLENBQUMsTUFBTTtnQkFDM0IsRUFBRSxHQUFHLGFBQWEsQ0FBQyxNQUFNLENBQUM7Z0JBQzlCLE1BQU07WUFFTixLQUFLLG1CQUFtQixDQUFDLFFBQVE7Z0JBQzdCLEVBQUUsR0FBRyxhQUFhLENBQUMsUUFBUSxDQUFDO2dCQUNoQyxNQUFNO1NBQ1Q7UUFFRCxPQUFPLEVBQUUsQ0FBQztJQUNkLENBQUM7Ozs7O0lBR0QsK0NBQWM7Ozs7SUFBZCxVQUFlLFVBQWlCO1FBQzVCLFFBQVEsVUFBVSxFQUFFO1lBQ2hCLEtBQUssYUFBYSxDQUFDLE9BQU87Z0JBQ3RCLElBQUksQ0FBQyxPQUFPLEVBQUUsQ0FBQztnQkFDbkIsTUFBTTtZQUVOLEtBQUssYUFBYSxDQUFDLE1BQU07Z0JBQ3JCLElBQUksQ0FBQyxNQUFNLEVBQUUsQ0FBQztnQkFDbEIsTUFBTTtZQUVOLEtBQUssYUFBYSxDQUFDLFFBQVE7Z0JBQ3ZCLElBQUksQ0FBQyxRQUFRLEVBQUUsQ0FBQztnQkFDcEIsTUFBTTtZQUVOO2dCQUNJLElBQUksQ0FBQyxPQUFPLEVBQUUsQ0FBQztTQUN0QjtJQUNMLENBQUM7Ozs7SUFHRCx3Q0FBTzs7O0lBQVA7UUFDSSxJQUFJLENBQUMsWUFBWSxHQUFHLElBQUksQ0FBQyxVQUFVLEVBQUUsQ0FBQztRQUN0QyxJQUFJLENBQUMsYUFBYSxDQUFDLElBQUksQ0FBQyxhQUFhLENBQUMsT0FBTyxDQUFDLENBQUM7SUFDbkQsQ0FBQzs7OztJQUdELHdDQUFPOzs7SUFBUDtRQUNJLElBQUksQ0FBQyxZQUFZLEdBQUcsSUFBSSxDQUFDLFVBQVUsRUFBRSxDQUFDO1FBQ3RDLElBQUksQ0FBQyxhQUFhLENBQUMsSUFBSSxDQUFDLGFBQWEsQ0FBQyxPQUFPLENBQUMsQ0FBQztJQUNuRCxDQUFDOzs7O0lBR0QsdUNBQU07OztJQUFOO1FBQ0ksSUFBSSxDQUFDLFlBQVksR0FBRyxJQUFJLENBQUMsU0FBUyxFQUFFLENBQUM7UUFDckMsSUFBSSxDQUFDLGFBQWEsQ0FBQyxJQUFJLENBQUMsYUFBYSxDQUFDLE1BQU0sQ0FBQyxDQUFDO0lBQ2xELENBQUM7Ozs7SUFHRCx5Q0FBUTs7O0lBQVI7UUFDSSxJQUFJLENBQUMsWUFBWSxHQUFHLElBQUksQ0FBQyxXQUFXLEVBQUUsQ0FBQztRQUN2QyxJQUFJLENBQUMsYUFBYSxDQUFDLElBQUksQ0FBQyxhQUFhLENBQUMsUUFBUSxDQUFDLENBQUM7SUFDcEQsQ0FBQzs7OztJQUdELHFDQUFJOzs7SUFBSjtRQUNJLElBQUksQ0FBQyxZQUFZLEdBQUcsSUFBSSxDQUFDLE9BQU8sRUFBRSxDQUFDO0lBQ3ZDLENBQUM7SUFJRCxzQkFBSSw2Q0FBUzs7OztRQUFiO1lBQ0ksT0FBTyxJQUFJLENBQUMsV0FBVyxDQUFDLGFBQWE7Z0JBQzdCLGFBQWEsQ0FBQyxPQUFPLENBQUM7UUFDbEMsQ0FBQzs7O09BQUE7SUFDRCxzQkFBSSw2Q0FBUzs7OztRQUFiO1lBQ0ksT0FBTyxJQUFJLENBQUMsV0FBVyxDQUFDLGFBQWE7Z0JBQzdCLGFBQWEsQ0FBQyxPQUFPLENBQUM7UUFDbEMsQ0FBQzs7O09BQUE7SUFDRCxzQkFBSSw0Q0FBUTs7OztRQUFaO1lBQ0ksT0FBTyxJQUFJLENBQUMsV0FBVyxDQUFDLGFBQWE7Z0JBQzdCLGFBQWEsQ0FBQyxNQUFNLENBQUM7UUFDakMsQ0FBQzs7O09BQUE7SUFDRCxzQkFBSSw4Q0FBVTs7OztRQUFkO1lBQ0ksT0FBTyxJQUFJLENBQUMsV0FBVyxDQUFDLGFBQWE7Z0JBQzdCLGFBQWEsQ0FBQyxRQUFRLENBQUM7UUFDbkMsQ0FBQzs7O09BQUE7SUFHRCxzQkFBSSwrQ0FBVzs7OztRQUFmO1lBQ0ksT0FBTyxJQUFJLENBQUMsWUFBWSxDQUFDO1FBQzdCLENBQUM7OztPQUFBO0lBQ0wsNkJBQUM7QUFBRCxDQUFDLEFBeklELElBeUlDOzs7Ozs7Ozs7OztJQXRJRywrQ0FHRTs7Ozs7SUFFRiw4Q0FBdUI7Ozs7OztJQWtGdkIsOERBQWtDOzs7Ozs7SUFNbEMsOERBQWtDOzs7Ozs7SUFNbEMsNkRBQWlDOzs7Ozs7SUFNakMsK0RBQW1DOzs7Ozs7SUFLbkMsMkRBQStCIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgZ2V0VXJsUGFyYW1ldGVyQnlOYW1lIH0gZnJvbSBcImVhcm5iZXQtY29tbW9uXCI7XG5cbmltcG9ydCB7IFRyYW5zbGF0aW9uSWQsIElUcmFuc2xhdGlvbkJhc2UsIElUcmFuc2xhdGlvbk1hbmFnZXIgfSBmcm9tIFwiLi90cmFuc2xhdGlvblwiO1xuaW1wb3J0IHsgTG9jYWxTdG9yYWdlRGF0YSB9IGZyb20gXCIuLi91dGlsL2xvY2FsLXN0b3JhZ2UtdXRpbFwiO1xuXG5cbmNsYXNzIEJyb3dzZXJMYW5ndWFnZUNvZGVcbntcbiAgICBzdGF0aWMgRU5HTElTSCA9ICdlbic7XG4gICAgc3RhdGljIENISU5FU0UgPSAnemgnO1xuICAgIHN0YXRpYyBLT1JFQU4gPSAna28nO1xuICAgIHN0YXRpYyBKQVBBTkVTRSA9ICdqYSc7XG59XG5cblxuZXhwb3J0IGFic3RyYWN0IGNsYXNzIFRyYW5zbGF0aW9uTWFuYWdlckJhc2U8VCBleHRlbmRzIElUcmFuc2xhdGlvbkJhc2U+XG4gICAgaW1wbGVtZW50cyBJVHJhbnNsYXRpb25NYW5hZ2VyXG57XG4gICAgcHJpdmF0ZSBzYXZlZExhbmd1YWdlOkxvY2FsU3RvcmFnZURhdGE8c3RyaW5nPiA9IG5ldyBMb2NhbFN0b3JhZ2VEYXRhKFxuICAgICAgICAnc2VsZWN0ZWRMYW5ndWFnZScsXG4gICAgICAgIGZhbHNlXG4gICAgKTtcbiAgICBcbiAgICBwcml2YXRlIF90cmFuc2xhdGlvbjpUO1xuXG4gICAgY29uc3RydWN0b3IoKSB7XG4gICAgfVxuICAgIGluaXQoKSB7XG4gICAgICAgIGNvbnN0IGxhbmcgPSBnZXRVcmxQYXJhbWV0ZXJCeU5hbWUoJ2xhbmcnKTtcblxuICAgICAgICBjb25zdCBpc0xhbmd1YWdlSW5VcmwgPSBsYW5nICE9IG51bGw7XG5cbiAgICAgICAgaWYgKGlzTGFuZ3VhZ2VJblVybCkge1xuICAgICAgICAgICAgdGhpcy5zZWxlY3RMYW5ndWFnZShsYW5nKTtcbiAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgIGxldCBsYW5nID0gdGhpcy5zYXZlZExhbmd1YWdlLmdldCgpO1xuXG4gICAgICAgICAgICBpZiAobGFuZyA9PSBudWxsKSB7XG4gICAgICAgICAgICAgICAgbGFuZyA9IHRoaXMuZ2V0QnJvd3Nlckxhbmd1YWdlKCk7XG4gICAgICAgICAgICB9XG5cbiAgICAgICAgICAgIHRoaXMuc2VsZWN0TGFuZ3VhZ2UobGFuZyk7XG4gICAgICAgIH1cbiAgICB9XG5cbiAgICBwcml2YXRlIGdldEJyb3dzZXJMYW5ndWFnZSgpIHtcbiAgICAgICAgbGV0IGxhbmcgPSB3aW5kb3cubmF2aWdhdG9yLmxhbmd1YWdlcyA/IHdpbmRvdy5uYXZpZ2F0b3IubGFuZ3VhZ2VzWzBdIDogbnVsbDtcblxuICAgICAgICBsYW5nID0gbGFuZyB8fCB3aW5kb3cubmF2aWdhdG9yLmxhbmd1YWdlIHx8IFxuICAgICAgICAgICAgICAgIHdpbmRvdy5uYXZpZ2F0b3JbJ2Jyb3dzZXJMYW5ndWFnZSddIHx8IFxuICAgICAgICAgICAgICAgIHdpbmRvdy5uYXZpZ2F0b3JbJ3VzZXJMYW5ndWFnZSddO1xuICAgICAgICBcbiAgICAgICAgaWYgKGxhbmcuaW5kZXhPZignLScpICE9PSAtMSlcbiAgICAgICAgICAgIGxhbmcgPSBsYW5nLnNwbGl0KCctJylbMF07XG5cbiAgICAgICAgaWYgKGxhbmcuaW5kZXhPZignXycpICE9PSAtMSlcbiAgICAgICAgICAgIGxhbmcgPSBsYW5nLnNwbGl0KCdfJylbMF07XG4gICAgICAgIFxuICAgICAgICBsYW5nID0gbGFuZy50b0xvd2VyQ2FzZSgpO1xuXG5cbiAgICAgICAgbGV0IGlkID0gVHJhbnNsYXRpb25JZC5FTkdMSVNIO1xuXG4gICAgICAgIHN3aXRjaCAobGFuZykge1xuICAgICAgICAgICAgY2FzZSBCcm93c2VyTGFuZ3VhZ2VDb2RlLkNISU5FU0U6XG4gICAgICAgICAgICAgICAgaWQgPSBUcmFuc2xhdGlvbklkLkNISU5FU0U7XG4gICAgICAgICAgICBicmVhaztcblxuICAgICAgICAgICAgY2FzZSBCcm93c2VyTGFuZ3VhZ2VDb2RlLktPUkVBTjpcbiAgICAgICAgICAgICAgICBpZCA9IFRyYW5zbGF0aW9uSWQuS09SRUFOO1xuICAgICAgICAgICAgYnJlYWs7XG5cbiAgICAgICAgICAgIGNhc2UgQnJvd3Nlckxhbmd1YWdlQ29kZS5KQVBBTkVTRTpcbiAgICAgICAgICAgICAgICBpZCA9IFRyYW5zbGF0aW9uSWQuSkFQQU5FU0U7XG4gICAgICAgICAgICBicmVhaztcbiAgICAgICAgfVxuXG4gICAgICAgIHJldHVybiBpZDtcbiAgICB9XG4gICAgXG4gICAgXG4gICAgc2VsZWN0TGFuZ3VhZ2UobGFuZ3VhZ2VJZDpzdHJpbmcpIHtcbiAgICAgICAgc3dpdGNoIChsYW5ndWFnZUlkKSB7XG4gICAgICAgICAgICBjYXNlIFRyYW5zbGF0aW9uSWQuQ0hJTkVTRTpcbiAgICAgICAgICAgICAgICB0aGlzLmNoaW5lc2UoKTtcbiAgICAgICAgICAgIGJyZWFrO1xuXG4gICAgICAgICAgICBjYXNlIFRyYW5zbGF0aW9uSWQuS09SRUFOOlxuICAgICAgICAgICAgICAgIHRoaXMua29yZWFuKCk7XG4gICAgICAgICAgICBicmVhaztcblxuICAgICAgICAgICAgY2FzZSBUcmFuc2xhdGlvbklkLkpBUEFORVNFOlxuICAgICAgICAgICAgICAgIHRoaXMuamFwYW5lc2UoKTtcbiAgICAgICAgICAgIGJyZWFrO1xuXG4gICAgICAgICAgICBkZWZhdWx0OlxuICAgICAgICAgICAgICAgIHRoaXMuZW5nbGlzaCgpO1xuICAgICAgICB9XG4gICAgfVxuXG5cbiAgICBlbmdsaXNoKCkge1xuICAgICAgICB0aGlzLl90cmFuc2xhdGlvbiA9IHRoaXMuZ2V0RW5nbGlzaCgpO1xuICAgICAgICB0aGlzLnNhdmVkTGFuZ3VhZ2Uuc2F2ZShUcmFuc2xhdGlvbklkLkVOR0xJU0gpO1xuICAgIH1cbiAgICBwcm90ZWN0ZWQgYWJzdHJhY3QgZ2V0RW5nbGlzaCgpOlQ7XG5cbiAgICBjaGluZXNlKCkge1xuICAgICAgICB0aGlzLl90cmFuc2xhdGlvbiA9IHRoaXMuZ2V0Q2hpbmVzZSgpO1xuICAgICAgICB0aGlzLnNhdmVkTGFuZ3VhZ2Uuc2F2ZShUcmFuc2xhdGlvbklkLkNISU5FU0UpO1xuICAgIH1cbiAgICBwcm90ZWN0ZWQgYWJzdHJhY3QgZ2V0Q2hpbmVzZSgpOlQ7XG5cbiAgICBrb3JlYW4oKSB7XG4gICAgICAgIHRoaXMuX3RyYW5zbGF0aW9uID0gdGhpcy5nZXRLb3JlYW4oKTtcbiAgICAgICAgdGhpcy5zYXZlZExhbmd1YWdlLnNhdmUoVHJhbnNsYXRpb25JZC5LT1JFQU4pO1xuICAgIH1cbiAgICBwcm90ZWN0ZWQgYWJzdHJhY3QgZ2V0S29yZWFuKCk6VDtcblxuICAgIGphcGFuZXNlKCkge1xuICAgICAgICB0aGlzLl90cmFuc2xhdGlvbiA9IHRoaXMuZ2V0SmFwYW5lc2UoKTtcbiAgICAgICAgdGhpcy5zYXZlZExhbmd1YWdlLnNhdmUoVHJhbnNsYXRpb25JZC5KQVBBTkVTRSk7XG4gICAgfVxuICAgIHByb3RlY3RlZCBhYnN0cmFjdCBnZXRKYXBhbmVzZSgpOlQ7XG5cbiAgICBseW54KCkge1xuICAgICAgICB0aGlzLl90cmFuc2xhdGlvbiA9IHRoaXMuZ2V0THlueCgpO1xuICAgIH1cbiAgICBwcm90ZWN0ZWQgYWJzdHJhY3QgZ2V0THlueCgpOlQ7XG5cblxuICAgIGdldCBpc0VuZ2xpc2goKSB7XG4gICAgICAgIHJldHVybiB0aGlzLnRyYW5zbGF0aW9uLnRyYW5zbGF0aW9uSWQgPT1cbiAgICAgICAgICAgICAgICBUcmFuc2xhdGlvbklkLkVOR0xJU0g7XG4gICAgfVxuICAgIGdldCBpc0NoaW5lc2UoKSB7XG4gICAgICAgIHJldHVybiB0aGlzLnRyYW5zbGF0aW9uLnRyYW5zbGF0aW9uSWQgPT1cbiAgICAgICAgICAgICAgICBUcmFuc2xhdGlvbklkLkNISU5FU0U7XG4gICAgfVxuICAgIGdldCBpc0tvcmVhbigpIHtcbiAgICAgICAgcmV0dXJuIHRoaXMudHJhbnNsYXRpb24udHJhbnNsYXRpb25JZCA9PVxuICAgICAgICAgICAgICAgIFRyYW5zbGF0aW9uSWQuS09SRUFOO1xuICAgIH1cbiAgICBnZXQgaXNKYXBhbmVzZSgpIHtcbiAgICAgICAgcmV0dXJuIHRoaXMudHJhbnNsYXRpb24udHJhbnNsYXRpb25JZCA9PVxuICAgICAgICAgICAgICAgIFRyYW5zbGF0aW9uSWQuSkFQQU5FU0U7XG4gICAgfVxuXG5cbiAgICBnZXQgdHJhbnNsYXRpb24oKSB7XG4gICAgICAgIHJldHVybiB0aGlzLl90cmFuc2xhdGlvbjtcbiAgICB9XG59Il19