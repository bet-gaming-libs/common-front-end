/**
 * @fileoverview added by tsickle
 * Generated from: lib/util/buffer-util.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/**
 * @param {?} uint8arr
 * @return {?}
 */
export function byteArrayToHexString(uint8arr) {
    if (!uint8arr) {
        return '';
    }
    /** @type {?} */
    var hexStr = '';
    for (var i = 0; i < uint8arr.length; i++) {
        /** @type {?} */
        var hex = (uint8arr[i] & 0xff).toString(16);
        hex = (hex.length === 1) ? '0' + hex : hex;
        hexStr += hex;
    }
    return hexStr.toUpperCase();
}
/**
 * @param {?} str
 * @return {?}
 */
export function hexStringToByteArray(str) {
    if (!str) {
        return new Uint8Array(0);
    }
    /** @type {?} */
    var a = [];
    for (var i = 0, len = str.length; i < len; i += 2) {
        a.push(parseInt(str.substr(i, 2), 16));
    }
    return new Uint8Array(a);
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYnVmZmVyLXV0aWwuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9lYXJuYmV0LWNvbW1vbi1mcm9udC1lbmQvIiwic291cmNlcyI6WyJsaWIvdXRpbC9idWZmZXItdXRpbC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7QUFBQSxNQUFNLFVBQVUsb0JBQW9CLENBQUMsUUFBbUI7SUFDcEQsSUFBSSxDQUFDLFFBQVEsRUFBRTtRQUNYLE9BQU8sRUFBRSxDQUFDO0tBQ2I7O1FBRUcsTUFBTSxHQUFHLEVBQUU7SUFDZixLQUFLLElBQUksQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDLEdBQUcsUUFBUSxDQUFDLE1BQU0sRUFBRSxDQUFDLEVBQUUsRUFBRTs7WUFDbEMsR0FBRyxHQUFHLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQyxHQUFHLElBQUksQ0FBQyxDQUFDLFFBQVEsQ0FBQyxFQUFFLENBQUM7UUFDM0MsR0FBRyxHQUFHLENBQUMsR0FBRyxDQUFDLE1BQU0sS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsR0FBRyxHQUFHLEdBQUcsQ0FBQyxDQUFDLENBQUMsR0FBRyxDQUFDO1FBQzNDLE1BQU0sSUFBSSxHQUFHLENBQUM7S0FDakI7SUFFRCxPQUFPLE1BQU0sQ0FBQyxXQUFXLEVBQUUsQ0FBQztBQUNoQyxDQUFDOzs7OztBQUVELE1BQU0sVUFBVSxvQkFBb0IsQ0FBQyxHQUFVO0lBQzNDLElBQUksQ0FBQyxHQUFHLEVBQUU7UUFDTixPQUFPLElBQUksVUFBVSxDQUFDLENBQUMsQ0FBQyxDQUFDO0tBQzVCOztRQUVHLENBQUMsR0FBRyxFQUFFO0lBQ1YsS0FBSyxJQUFJLENBQUMsR0FBRyxDQUFDLEVBQUUsR0FBRyxHQUFHLEdBQUcsQ0FBQyxNQUFNLEVBQUUsQ0FBQyxHQUFHLEdBQUcsRUFBRSxDQUFDLElBQUUsQ0FBQyxFQUFFO1FBQzdDLENBQUMsQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLEdBQUcsQ0FBQyxNQUFNLENBQUMsQ0FBQyxFQUFDLENBQUMsQ0FBQyxFQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUM7S0FDeEM7SUFFRCxPQUFPLElBQUksVUFBVSxDQUFDLENBQUMsQ0FBQyxDQUFDO0FBQzdCLENBQUMiLCJzb3VyY2VzQ29udGVudCI6WyJleHBvcnQgZnVuY3Rpb24gYnl0ZUFycmF5VG9IZXhTdHJpbmcodWludDhhcnI6VWludDhBcnJheSkge1xuICAgIGlmICghdWludDhhcnIpIHtcbiAgICAgICAgcmV0dXJuICcnO1xuICAgIH1cblxuICAgIHZhciBoZXhTdHIgPSAnJztcbiAgICBmb3IgKHZhciBpID0gMDsgaSA8IHVpbnQ4YXJyLmxlbmd0aDsgaSsrKSB7XG4gICAgICAgIHZhciBoZXggPSAodWludDhhcnJbaV0gJiAweGZmKS50b1N0cmluZygxNik7XG4gICAgICAgIGhleCA9IChoZXgubGVuZ3RoID09PSAxKSA/ICcwJyArIGhleCA6IGhleDtcbiAgICAgICAgaGV4U3RyICs9IGhleDtcbiAgICB9XG5cbiAgICByZXR1cm4gaGV4U3RyLnRvVXBwZXJDYXNlKCk7XG59XG5cbmV4cG9ydCBmdW5jdGlvbiBoZXhTdHJpbmdUb0J5dGVBcnJheShzdHI6c3RyaW5nKTpVaW50OEFycmF5IHtcbiAgICBpZiAoIXN0cikge1xuICAgICAgICByZXR1cm4gbmV3IFVpbnQ4QXJyYXkoMCk7XG4gICAgfVxuXG4gICAgdmFyIGEgPSBbXTtcbiAgICBmb3IgKHZhciBpID0gMCwgbGVuID0gc3RyLmxlbmd0aDsgaSA8IGxlbjsgaSs9Mikge1xuICAgICAgICBhLnB1c2gocGFyc2VJbnQoc3RyLnN1YnN0cihpLDIpLDE2KSk7XG4gICAgfVxuXG4gICAgcmV0dXJuIG5ldyBVaW50OEFycmF5KGEpO1xufSJdfQ==