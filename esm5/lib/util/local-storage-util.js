/**
 * @fileoverview added by tsickle
 * Generated from: lib/util/local-storage-util.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/**
 * @template T
 */
var /**
 * @template T
 */
LocalStorageData = /** @class */ (function () {
    function LocalStorageData(dataKey, isObject) {
        this.dataKey = dataKey;
        this.isObject = isObject;
    }
    /**
     * @return {?}
     */
    LocalStorageData.prototype.isSaved = /**
     * @return {?}
     */
    function () {
        return this.get() != null;
    };
    /**
     * @return {?}
     */
    LocalStorageData.prototype.get = /**
     * @return {?}
     */
    function () {
        /** @type {?} */
        var data = localStorage.getItem(this.dataKey);
        return data != null ?
            (this.isObject ?
                JSON.parse(data) :
                data) :
            null;
    };
    /**
     * @param {?} data
     * @return {?}
     */
    LocalStorageData.prototype.save = /**
     * @param {?} data
     * @return {?}
     */
    function (data) {
        localStorage.setItem(this.dataKey, (this.isObject ?
            JSON.stringify(data) :
            '' + data));
    };
    return LocalStorageData;
}());
/**
 * @template T
 */
export { LocalStorageData };
if (false) {
    /**
     * @type {?}
     * @private
     */
    LocalStorageData.prototype.dataKey;
    /**
     * @type {?}
     * @private
     */
    LocalStorageData.prototype.isObject;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibG9jYWwtc3RvcmFnZS11dGlsLmpzIiwic291cmNlUm9vdCI6Im5nOi8vZWFybmJldC1jb21tb24tZnJvbnQtZW5kLyIsInNvdXJjZXMiOlsibGliL3V0aWwvbG9jYWwtc3RvcmFnZS11dGlsLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7O0FBQUE7Ozs7SUFFSSwwQkFDWSxPQUFjLEVBQ2QsUUFBZ0I7UUFEaEIsWUFBTyxHQUFQLE9BQU8sQ0FBTztRQUNkLGFBQVEsR0FBUixRQUFRLENBQVE7SUFFNUIsQ0FBQzs7OztJQUVELGtDQUFPOzs7SUFBUDtRQUNJLE9BQU8sSUFBSSxDQUFDLEdBQUcsRUFBRSxJQUFJLElBQUksQ0FBQztJQUM5QixDQUFDOzs7O0lBRUQsOEJBQUc7OztJQUFIOztZQUNVLElBQUksR0FBRyxZQUFZLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUM7UUFFL0MsT0FBTyxJQUFJLElBQUksSUFBSSxDQUFDLENBQUM7WUFDYixDQUNJLElBQUksQ0FBQyxRQUFRLENBQUMsQ0FBQztnQkFDWCxJQUFJLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUM7Z0JBQ2xCLElBQUksQ0FDWCxDQUFDLENBQUM7WUFDSCxJQUFJLENBQUM7SUFDakIsQ0FBQzs7Ozs7SUFFRCwrQkFBSTs7OztJQUFKLFVBQUssSUFBTTtRQUNQLFlBQVksQ0FBQyxPQUFPLENBQ2hCLElBQUksQ0FBQyxPQUFPLEVBQ1osQ0FDSSxJQUFJLENBQUMsUUFBUSxDQUFDLENBQUM7WUFDZixJQUFJLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUM7WUFDdEIsRUFBRSxHQUFDLElBQUksQ0FDVixDQUNKLENBQUM7SUFDTixDQUFDO0lBQ0wsdUJBQUM7QUFBRCxDQUFDLEFBbENELElBa0NDOzs7Ozs7Ozs7O0lBL0JPLG1DQUFzQjs7Ozs7SUFDdEIsb0NBQXdCIiwic291cmNlc0NvbnRlbnQiOlsiZXhwb3J0IGNsYXNzIExvY2FsU3RvcmFnZURhdGE8VD5cbntcbiAgICBjb25zdHJ1Y3RvcihcbiAgICAgICAgcHJpdmF0ZSBkYXRhS2V5OnN0cmluZyxcbiAgICAgICAgcHJpdmF0ZSBpc09iamVjdDpib29sZWFuXG4gICAgKSB7XG4gICAgfVxuXG4gICAgaXNTYXZlZCgpOmJvb2xlYW4ge1xuICAgICAgICByZXR1cm4gdGhpcy5nZXQoKSAhPSBudWxsO1xuICAgIH1cblxuICAgIGdldCgpOlQge1xuICAgICAgICBjb25zdCBkYXRhID0gbG9jYWxTdG9yYWdlLmdldEl0ZW0odGhpcy5kYXRhS2V5KTtcblxuICAgICAgICByZXR1cm4gZGF0YSAhPSBudWxsID9cbiAgICAgICAgICAgICAgICAoXG4gICAgICAgICAgICAgICAgICAgIHRoaXMuaXNPYmplY3QgP1xuICAgICAgICAgICAgICAgICAgICAgICAgSlNPTi5wYXJzZShkYXRhKSA6XG4gICAgICAgICAgICAgICAgICAgICAgICBkYXRhXG4gICAgICAgICAgICAgICAgKSA6XG4gICAgICAgICAgICAgICAgbnVsbDtcbiAgICB9XG5cbiAgICBzYXZlKGRhdGE6VCkge1xuICAgICAgICBsb2NhbFN0b3JhZ2Uuc2V0SXRlbShcbiAgICAgICAgICAgIHRoaXMuZGF0YUtleSxcbiAgICAgICAgICAgIChcbiAgICAgICAgICAgICAgICB0aGlzLmlzT2JqZWN0ID9cbiAgICAgICAgICAgICAgICBKU09OLnN0cmluZ2lmeShkYXRhKSA6XG4gICAgICAgICAgICAgICAgJycrZGF0YVxuICAgICAgICAgICAgKVxuICAgICAgICApO1xuICAgIH1cbn0iXX0=