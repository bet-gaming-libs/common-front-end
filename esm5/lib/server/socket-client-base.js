/**
 * @fileoverview added by tsickle
 * Generated from: lib/server/socket-client-base.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/**
 * @abstract
 */
var /**
 * @abstract
 */
SocketClientBase = /** @class */ (function () {
    // CLASS members:
    // INSTANCE members:
    function SocketClientBase(config, nameSpace) {
        var _this = this;
        this.config = config;
        this.nameSpace = nameSpace;
        this.onConnect = (/**
         * @return {?}
         */
        function () {
            _this.onConnected();
        });
        this.onError = (/**
         * @param {?} error
         * @return {?}
         */
        function (error) {
            console.log('onError');
            console.log(error);
        });
        this.onDisconnect = (/**
         * @return {?}
         */
        function () {
            _this.onDisconnected();
        });
    }
    // *** ESTABLISH CONNECTION to SocketServer ***
    // *** ESTABLISH CONNECTION to SocketServer ***
    /**
     * @protected
     * @return {?}
     */
    SocketClientBase.prototype.connectSocket = 
    // *** ESTABLISH CONNECTION to SocketServer ***
    /**
     * @protected
     * @return {?}
     */
    function () {
        // connect scoket
        /** @type {?} */
        var url = 'http' +
            (this.config.ssl === true ? 's' : '') +
            '://' + this.config.host + ':' +
            this.config.port + this.nameSpace;
        /** @type {?} */
        var socket = io(url);
        socket.on('connect', this.onConnect);
        socket.on('disconnect', this.onConnect);
        socket.on('connect_error', this.onError);
        socket.on('error', this.onError);
        return socket;
    };
    return SocketClientBase;
}());
/**
 * @abstract
 */
export { SocketClientBase };
if (false) {
    /**
     * @type {?}
     * @private
     */
    SocketClientBase.prototype.onConnect;
    /**
     * @type {?}
     * @private
     */
    SocketClientBase.prototype.onError;
    /**
     * @type {?}
     * @private
     */
    SocketClientBase.prototype.onDisconnect;
    /**
     * @type {?}
     * @private
     */
    SocketClientBase.prototype.config;
    /**
     * @type {?}
     * @private
     */
    SocketClientBase.prototype.nameSpace;
    /**
     * @abstract
     * @protected
     * @return {?}
     */
    SocketClientBase.prototype.onConnected = function () { };
    /**
     * @abstract
     * @protected
     * @return {?}
     */
    SocketClientBase.prototype.onDisconnected = function () { };
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic29ja2V0LWNsaWVudC1iYXNlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vZWFybmJldC1jb21tb24tZnJvbnQtZW5kLyIsInNvdXJjZXMiOlsibGliL3NlcnZlci9zb2NrZXQtY2xpZW50LWJhc2UudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7QUFRQTs7OztJQUVJLGlCQUFpQjtJQUdqQixvQkFBb0I7SUFDcEIsMEJBQ1ksTUFBb0IsRUFDcEIsU0FBZ0I7UUFGNUIsaUJBSUM7UUFIVyxXQUFNLEdBQU4sTUFBTSxDQUFjO1FBQ3BCLGNBQVMsR0FBVCxTQUFTLENBQU87UUF1QnBCLGNBQVM7OztRQUFHO1lBQ2hCLEtBQUksQ0FBQyxXQUFXLEVBQUUsQ0FBQztRQUN2QixDQUFDLEVBQUE7UUFFTyxZQUFPOzs7O1FBQUcsVUFBQyxLQUFLO1lBQ3BCLE9BQU8sQ0FBQyxHQUFHLENBQUMsU0FBUyxDQUFDLENBQUM7WUFDdkIsT0FBTyxDQUFDLEdBQUcsQ0FBQyxLQUFLLENBQUMsQ0FBQztRQUN2QixDQUFDLEVBQUE7UUFDTyxpQkFBWTs7O1FBQUc7WUFDbkIsS0FBSSxDQUFDLGNBQWMsRUFBRSxDQUFDO1FBQzFCLENBQUMsRUFBQTtJQS9CRCxDQUFDO0lBRUQsK0NBQStDOzs7Ozs7SUFDckMsd0NBQWE7Ozs7OztJQUF2Qjs7O1lBR1UsR0FBRyxHQUFHLE1BQU07WUFDZCxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsR0FBRyxLQUFLLElBQUksQ0FBQyxDQUFDLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUM7WUFDckMsS0FBSyxHQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsSUFBSSxHQUFDLEdBQUc7WUFDdEIsSUFBSSxDQUFDLE1BQU0sQ0FBQyxJQUFJLEdBQUcsSUFBSSxDQUFDLFNBQVM7O1lBRW5DLE1BQU0sR0FBRyxFQUFFLENBQUMsR0FBRyxDQUFDO1FBRXRCLE1BQU0sQ0FBQyxFQUFFLENBQUMsU0FBUyxFQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQztRQUNwQyxNQUFNLENBQUMsRUFBRSxDQUFDLFlBQVksRUFBQyxJQUFJLENBQUMsU0FBUyxDQUFDLENBQUM7UUFFdkMsTUFBTSxDQUFDLEVBQUUsQ0FBQyxlQUFlLEVBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxDQUFDO1FBQ3hDLE1BQU0sQ0FBQyxFQUFFLENBQUMsT0FBTyxFQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsQ0FBQztRQUVoQyxPQUFPLE1BQU0sQ0FBQztJQUNsQixDQUFDO0lBYUwsdUJBQUM7QUFBRCxDQUFDLEFBM0NELElBMkNDOzs7Ozs7Ozs7O0lBWkcscUNBRUM7Ozs7O0lBRUQsbUNBR0M7Ozs7O0lBQ0Qsd0NBRUM7Ozs7O0lBbENHLGtDQUE0Qjs7Ozs7SUFDNUIscUNBQXdCOzs7Ozs7SUEwQjVCLHlEQUFzQzs7Ozs7O0lBUXRDLDREQUF5QyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7SVNlcnZlckNvbmZpZyB9IGZyb20gJ2Vhcm5iZXQtY29tbW9uJztcblxuXG5cbmRlY2xhcmUgdmFyIGlvO1xuXG5cblxuZXhwb3J0IGFic3RyYWN0IGNsYXNzIFNvY2tldENsaWVudEJhc2VcbntcbiAgICAvLyBDTEFTUyBtZW1iZXJzOlxuICAgIFxuICAgIFxuICAgIC8vIElOU1RBTkNFIG1lbWJlcnM6XG4gICAgY29uc3RydWN0b3IoXG4gICAgICAgIHByaXZhdGUgY29uZmlnOklTZXJ2ZXJDb25maWcsXG4gICAgICAgIHByaXZhdGUgbmFtZVNwYWNlOnN0cmluZ1xuICAgICkge1xuICAgIH1cblxuICAgIC8vICoqKiBFU1RBQkxJU0ggQ09OTkVDVElPTiB0byBTb2NrZXRTZXJ2ZXIgKioqXG4gICAgcHJvdGVjdGVkIGNvbm5lY3RTb2NrZXQoKTpTb2NrZXRJT0NsaWVudC5Tb2NrZXRcbiAgICB7XG4gICAgICAgIC8vIGNvbm5lY3Qgc2Nva2V0XG4gICAgICAgIGNvbnN0IHVybCA9ICdodHRwJytcbiAgICAgICAgICAgICh0aGlzLmNvbmZpZy5zc2wgPT09IHRydWUgPyAncycgOiAnJykgK1xuICAgICAgICAgICAgJzovLycrdGhpcy5jb25maWcuaG9zdCsnOicrXG4gICAgICAgICAgICAgICAgdGhpcy5jb25maWcucG9ydCArIHRoaXMubmFtZVNwYWNlO1xuXG4gICAgICAgIGNvbnN0IHNvY2tldCA9IGlvKHVybCk7XG5cbiAgICAgICAgc29ja2V0Lm9uKCdjb25uZWN0Jyx0aGlzLm9uQ29ubmVjdCk7XG4gICAgICAgIHNvY2tldC5vbignZGlzY29ubmVjdCcsdGhpcy5vbkNvbm5lY3QpO1xuICAgICAgICBcbiAgICAgICAgc29ja2V0Lm9uKCdjb25uZWN0X2Vycm9yJyx0aGlzLm9uRXJyb3IpO1xuICAgICAgICBzb2NrZXQub24oJ2Vycm9yJyx0aGlzLm9uRXJyb3IpO1xuXG4gICAgICAgIHJldHVybiBzb2NrZXQ7XG4gICAgfVxuICAgIHByaXZhdGUgb25Db25uZWN0ID0gKCkgPT4ge1xuICAgICAgICB0aGlzLm9uQ29ubmVjdGVkKCk7XG4gICAgfVxuICAgIHByb3RlY3RlZCBhYnN0cmFjdCBvbkNvbm5lY3RlZCgpOnZvaWQ7XG4gICAgcHJpdmF0ZSBvbkVycm9yID0gKGVycm9yKSA9PiB7XG4gICAgICAgIGNvbnNvbGUubG9nKCdvbkVycm9yJyk7XG4gICAgICAgIGNvbnNvbGUubG9nKGVycm9yKTtcbiAgICB9XG4gICAgcHJpdmF0ZSBvbkRpc2Nvbm5lY3QgPSAoKSA9PiB7XG4gICAgICAgIHRoaXMub25EaXNjb25uZWN0ZWQoKTtcbiAgICB9XG4gICAgcHJvdGVjdGVkIGFic3RyYWN0IG9uRGlzY29ubmVjdGVkKCk6dm9pZDtcbn0iXX0=