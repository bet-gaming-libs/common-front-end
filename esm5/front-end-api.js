/**
 * @fileoverview added by tsickle
 * Generated from: front-end-api.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/*
 * Public API Surface of earnbet-common-front-end
 */
export { SocketClientBase } from './lib/server/socket-client-base';
export { byteArrayToHexString, hexStringToByteArray } from './lib/util/buffer-util';
export { LocalStorageData } from './lib/util/local-storage-util';
export { easyAccountDataKey } from './lib/easy-account/model/data';
export {} from './lib/easy-account/data/api-data';
export { StringValue, RepeatedValue } from './lib/easy-account/model/forms/fields';
export { ExistingEasyAccount } from './lib/easy-account/model/existing-account';
export { EasyAccountManager } from './lib/easy-account/model/account-manager';
export {} from './lib/easy-account/model/data';
export {} from './lib/easy-account/data/api-data';
export { TranslationId } from './lib/translations/translation';
export { TranslationManagerBase } from './lib/translations/translation-manager';
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZnJvbnQtZW5kLWFwaS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL2Vhcm5iZXQtY29tbW9uLWZyb250LWVuZC8iLCJzb3VyY2VzIjpbImZyb250LWVuZC1hcGkudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7QUFHQSxpQ0FBYyxpQ0FBaUMsQ0FBQztBQUVoRCwyREFBYyx3QkFBd0IsQ0FBQztBQUN2QyxpQ0FBYywrQkFBK0IsQ0FBQztBQUc5QyxtQ0FBYywrQkFBK0IsQ0FBQztBQUM5QyxlQUFjLGtDQUFrQyxDQUFDO0FBQ2pELDJDQUFjLHVDQUF1QyxDQUFDO0FBQ3RELG9DQUFjLDJDQUEyQyxDQUFDO0FBQzFELG1DQUFjLDBDQUEwQyxDQUFDO0FBQ3pELGVBQWMsK0JBQStCLENBQUM7QUFDOUMsZUFBYyxrQ0FBa0MsQ0FBQztBQUdqRCw4QkFBYyxnQ0FBZ0MsQ0FBQztBQUMvQyx1Q0FBYyx3Q0FBd0MsQ0FBQyIsInNvdXJjZXNDb250ZW50IjpbIi8qXG4gKiBQdWJsaWMgQVBJIFN1cmZhY2Ugb2YgZWFybmJldC1jb21tb24tZnJvbnQtZW5kXG4gKi9cbmV4cG9ydCAqIGZyb20gJy4vbGliL3NlcnZlci9zb2NrZXQtY2xpZW50LWJhc2UnO1xuXG5leHBvcnQgKiBmcm9tICcuL2xpYi91dGlsL2J1ZmZlci11dGlsJztcbmV4cG9ydCAqIGZyb20gJy4vbGliL3V0aWwvbG9jYWwtc3RvcmFnZS11dGlsJztcblxuXG5leHBvcnQgKiBmcm9tICcuL2xpYi9lYXN5LWFjY291bnQvbW9kZWwvZGF0YSc7XG5leHBvcnQgKiBmcm9tICcuL2xpYi9lYXN5LWFjY291bnQvZGF0YS9hcGktZGF0YSc7XG5leHBvcnQgKiBmcm9tICcuL2xpYi9lYXN5LWFjY291bnQvbW9kZWwvZm9ybXMvZmllbGRzJztcbmV4cG9ydCAqIGZyb20gJy4vbGliL2Vhc3ktYWNjb3VudC9tb2RlbC9leGlzdGluZy1hY2NvdW50JztcbmV4cG9ydCAqIGZyb20gJy4vbGliL2Vhc3ktYWNjb3VudC9tb2RlbC9hY2NvdW50LW1hbmFnZXInO1xuZXhwb3J0ICogZnJvbSAnLi9saWIvZWFzeS1hY2NvdW50L21vZGVsL2RhdGEnO1xuZXhwb3J0ICogZnJvbSAnLi9saWIvZWFzeS1hY2NvdW50L2RhdGEvYXBpLWRhdGEnO1xuXG5cbmV4cG9ydCAqIGZyb20gJy4vbGliL3RyYW5zbGF0aW9ucy90cmFuc2xhdGlvbic7XG5leHBvcnQgKiBmcm9tICcuL2xpYi90cmFuc2xhdGlvbnMvdHJhbnNsYXRpb24tbWFuYWdlcic7Il19