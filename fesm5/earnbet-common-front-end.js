import { __extends } from 'tslib';
import { trimString, getUrlParameterByName } from 'earnbet-common';

/**
 * @fileoverview added by tsickle
 * Generated from: lib/server/socket-client-base.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/**
 * @abstract
 */
var  /**
 * @abstract
 */
SocketClientBase = /** @class */ (function () {
    // CLASS members:
    // INSTANCE members:
    function SocketClientBase(config, nameSpace) {
        var _this = this;
        this.config = config;
        this.nameSpace = nameSpace;
        this.onConnect = (/**
         * @return {?}
         */
        function () {
            _this.onConnected();
        });
        this.onError = (/**
         * @param {?} error
         * @return {?}
         */
        function (error) {
            console.log('onError');
            console.log(error);
        });
        this.onDisconnect = (/**
         * @return {?}
         */
        function () {
            _this.onDisconnected();
        });
    }
    // *** ESTABLISH CONNECTION to SocketServer ***
    // *** ESTABLISH CONNECTION to SocketServer ***
    /**
     * @protected
     * @return {?}
     */
    SocketClientBase.prototype.connectSocket = 
    // *** ESTABLISH CONNECTION to SocketServer ***
    /**
     * @protected
     * @return {?}
     */
    function () {
        // connect scoket
        /** @type {?} */
        var url = 'http' +
            (this.config.ssl === true ? 's' : '') +
            '://' + this.config.host + ':' +
            this.config.port + this.nameSpace;
        /** @type {?} */
        var socket = io(url);
        socket.on('connect', this.onConnect);
        socket.on('disconnect', this.onConnect);
        socket.on('connect_error', this.onError);
        socket.on('error', this.onError);
        return socket;
    };
    return SocketClientBase;
}());
if (false) {
    /**
     * @type {?}
     * @private
     */
    SocketClientBase.prototype.onConnect;
    /**
     * @type {?}
     * @private
     */
    SocketClientBase.prototype.onError;
    /**
     * @type {?}
     * @private
     */
    SocketClientBase.prototype.onDisconnect;
    /**
     * @type {?}
     * @private
     */
    SocketClientBase.prototype.config;
    /**
     * @type {?}
     * @private
     */
    SocketClientBase.prototype.nameSpace;
    /**
     * @abstract
     * @protected
     * @return {?}
     */
    SocketClientBase.prototype.onConnected = function () { };
    /**
     * @abstract
     * @protected
     * @return {?}
     */
    SocketClientBase.prototype.onDisconnected = function () { };
}

/**
 * @fileoverview added by tsickle
 * Generated from: lib/util/buffer-util.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/**
 * @param {?} uint8arr
 * @return {?}
 */
function byteArrayToHexString(uint8arr) {
    if (!uint8arr) {
        return '';
    }
    /** @type {?} */
    var hexStr = '';
    for (var i = 0; i < uint8arr.length; i++) {
        /** @type {?} */
        var hex = (uint8arr[i] & 0xff).toString(16);
        hex = (hex.length === 1) ? '0' + hex : hex;
        hexStr += hex;
    }
    return hexStr.toUpperCase();
}
/**
 * @param {?} str
 * @return {?}
 */
function hexStringToByteArray(str) {
    if (!str) {
        return new Uint8Array(0);
    }
    /** @type {?} */
    var a = [];
    for (var i = 0, len = str.length; i < len; i += 2) {
        a.push(parseInt(str.substr(i, 2), 16));
    }
    return new Uint8Array(a);
}

/**
 * @fileoverview added by tsickle
 * Generated from: lib/util/local-storage-util.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/**
 * @template T
 */
var  /**
 * @template T
 */
LocalStorageData = /** @class */ (function () {
    function LocalStorageData(dataKey, isObject) {
        this.dataKey = dataKey;
        this.isObject = isObject;
    }
    /**
     * @return {?}
     */
    LocalStorageData.prototype.isSaved = /**
     * @return {?}
     */
    function () {
        return this.get() != null;
    };
    /**
     * @return {?}
     */
    LocalStorageData.prototype.get = /**
     * @return {?}
     */
    function () {
        /** @type {?} */
        var data = localStorage.getItem(this.dataKey);
        return data != null ?
            (this.isObject ?
                JSON.parse(data) :
                data) :
            null;
    };
    /**
     * @param {?} data
     * @return {?}
     */
    LocalStorageData.prototype.save = /**
     * @param {?} data
     * @return {?}
     */
    function (data) {
        localStorage.setItem(this.dataKey, (this.isObject ?
            JSON.stringify(data) :
            '' + data));
    };
    return LocalStorageData;
}());
if (false) {
    /**
     * @type {?}
     * @private
     */
    LocalStorageData.prototype.dataKey;
    /**
     * @type {?}
     * @private
     */
    LocalStorageData.prototype.isObject;
}

/**
 * @fileoverview added by tsickle
 * Generated from: lib/easy-account/model/data.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/** @type {?} */
var easyAccountDataKey = 'EasyAccountData';
/**
 * @record
 */
function IEasyAccountData() { }
if (false) {
    /** @type {?} */
    IEasyAccountData.prototype.id;
    /** @type {?} */
    IEasyAccountData.prototype.pub;
    /** @type {?} */
    IEasyAccountData.prototype.priv;
    /** @type {?} */
    IEasyAccountData.prototype.name;
    /** @type {?} */
    IEasyAccountData.prototype.isLoggedIn;
}
/**
 * @record
 */
function IEasyAccountTableRow() { }
if (false) {
    /** @type {?} */
    IEasyAccountTableRow.prototype.id;
    /** @type {?} */
    IEasyAccountTableRow.prototype.key;
    /** @type {?} */
    IEasyAccountTableRow.prototype.email_backup;
    /** @type {?} */
    IEasyAccountTableRow.prototype.nonce;
    /** @type {?} */
    IEasyAccountTableRow.prototype.bet_balance;
    /** @type {?} */
    IEasyAccountTableRow.prototype.eos_balance;
    /** @type {?} */
    IEasyAccountTableRow.prototype.eos_wagered;
    /** @type {?} */
    IEasyAccountTableRow.prototype.free_account;
    /** @type {?} */
    IEasyAccountTableRow.prototype.free_transfers;
    /** @type {?} */
    IEasyAccountTableRow.prototype.rewards_balance;
}

/**
 * @fileoverview added by tsickle
 * Generated from: lib/easy-account/data/api-data.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/**
 * @record
 */
function IEasyAccountDataForApi() { }
if (false) {
    /** @type {?} */
    IEasyAccountDataForApi.prototype.id;
    /** @type {?} */
    IEasyAccountDataForApi.prototype.publicKey;
    /** @type {?} */
    IEasyAccountDataForApi.prototype.name;
    /** @type {?} */
    IEasyAccountDataForApi.prototype.email;
    /** @type {?} */
    IEasyAccountDataForApi.prototype.recaptcha;
}

/**
 * @fileoverview added by tsickle
 * Generated from: lib/easy-account/model/forms/fields.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
var StringValue = /** @class */ (function () {
    function StringValue(minimumLength, maximumLength, shouldTrim) {
        if (maximumLength === void 0) { maximumLength = 256; }
        if (shouldTrim === void 0) { shouldTrim = false; }
        this.minimumLength = minimumLength;
        this.maximumLength = maximumLength;
        this.shouldTrim = shouldTrim;
        this.value = '';
        this._isInvalid = false;
    }
    /**
     * @return {?}
     */
    StringValue.prototype.validate = /**
     * @return {?}
     */
    function () {
        this._isInvalid = false;
        if (this.shouldTrim) {
            this.value = trimString(this.value);
        }
        /** @type {?} */
        var isValid = this.value.length >= this.minimumLength &&
            this.value.length <= this.maximumLength;
        this._isInvalid = !isValid;
        return isValid;
    };
    Object.defineProperty(StringValue.prototype, "isInvalid", {
        get: /**
         * @return {?}
         */
        function () {
            return this._isInvalid;
        },
        enumerable: true,
        configurable: true
    });
    return StringValue;
}());
if (false) {
    /** @type {?} */
    StringValue.prototype.value;
    /**
     * @type {?}
     * @private
     */
    StringValue.prototype._isInvalid;
    /** @type {?} */
    StringValue.prototype.minimumLength;
    /** @type {?} */
    StringValue.prototype.maximumLength;
    /** @type {?} */
    StringValue.prototype.shouldTrim;
}
var RepeatedValue = /** @class */ (function (_super) {
    __extends(RepeatedValue, _super);
    function RepeatedValue(minimumLength) {
        if (minimumLength === void 0) { minimumLength = 1; }
        var _this = _super.call(this, minimumLength) || this;
        _this.copy = '';
        return _this;
    }
    /**
     * @return {?}
     */
    RepeatedValue.prototype.validate = /**
     * @return {?}
     */
    function () {
        return _super.prototype.validate.call(this) &&
            this.value == this.copy;
    };
    return RepeatedValue;
}(StringValue));
if (false) {
    /** @type {?} */
    RepeatedValue.prototype.copy;
}

/**
 * @fileoverview added by tsickle
 * Generated from: lib/easy-account/model/existing-account.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
var ExistingEasyAccount = /** @class */ (function () {
    function ExistingEasyAccount() {
        /*
        setInterval(
            this.getDataFromChain,
            1000*60
        );
        */
    }
    /**
     * @return {?}
     */
    ExistingEasyAccount.prototype.load = /**
     * @return {?}
     */
    function () {
        this.localData = undefined;
        /** @type {?} */
        var savedAccountData = localStorage.getItem(easyAccountDataKey);
        if (savedAccountData != null) {
            this.localData = JSON.parse(savedAccountData);
        }
    };
    /*
    login(password:string):boolean {
        if (!this.isCreated) {
            return false;
        }

        this.password = password;

        const derivedPublicKey = eos_ecc.privateToPublic(
            this.privateKey
        );

        const isAuthenticated =
            derivedPublicKey == this.publicKey;

        this._isLoggedIn = isAuthenticated;

        return isAuthenticated;
    }
    */
    /*
        login(password:string):boolean {
            if (!this.isCreated) {
                return false;
            }
    
            this.password = password;
    
            const derivedPublicKey = eos_ecc.privateToPublic(
                this.privateKey
            );
    
            const isAuthenticated =
                derivedPublicKey == this.publicKey;
    
            this._isLoggedIn = isAuthenticated;
    
            return isAuthenticated;
        }
        */
    /**
     * @return {?}
     */
    ExistingEasyAccount.prototype.login = /*
        login(password:string):boolean {
            if (!this.isCreated) {
                return false;
            }
    
            this.password = password;
    
            const derivedPublicKey = eos_ecc.privateToPublic(
                this.privateKey
            );
    
            const isAuthenticated =
                derivedPublicKey == this.publicKey;
    
            this._isLoggedIn = isAuthenticated;
    
            return isAuthenticated;
        }
        */
    /**
     * @return {?}
     */
    function () {
        if (!this.isCreated) {
            return;
        }
        this.localData.isLoggedIn = true;
        this.save();
    };
    /**
     * @return {?}
     */
    ExistingEasyAccount.prototype.logout = /**
     * @return {?}
     */
    function () {
        if (!this.isCreated) {
            return;
        }
        this.localData.isLoggedIn = false;
        this.save();
    };
    /**
     * @private
     * @return {?}
     */
    ExistingEasyAccount.prototype.save = /**
     * @private
     * @return {?}
     */
    function () {
        if (!this.isCreated) {
            return;
        }
        localStorage.setItem(easyAccountDataKey, JSON.stringify(this.localData));
    };
    Object.defineProperty(ExistingEasyAccount.prototype, "eosBalance", {
        get: /**
         * @return {?}
         */
        function () {
            return this.onChainData ?
                this.onChainData.eos_balance :
                '0.0000 EOS';
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(ExistingEasyAccount.prototype, "id", {
        get: /**
         * @return {?}
         */
        function () {
            return this.isCreated ?
                this.localData.id :
                0;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(ExistingEasyAccount.prototype, "name", {
        get: /**
         * @return {?}
         */
        function () {
            return this.localData ?
                this.localData.name :
                '';
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(ExistingEasyAccount.prototype, "publicKey", {
        get: /**
         * @return {?}
         */
        function () {
            return this.localData ?
                this.localData.pub :
                '';
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(ExistingEasyAccount.prototype, "privateKey", {
        /*
        get privateKey():string {
            if (!this.isCreated) {
                return '';
            }
    
            return AES.decrypt(
                this.encryptedPrivateKey,
                this.password
            );
        }
        private get encryptedPrivateKey() {
            return this.localData ?
                    this.localData.priv :
                    '';
        }
        */
        get: /*
            get privateKey():string {
                if (!this.isCreated) {
                    return '';
                }
        
                return AES.decrypt(
                    this.encryptedPrivateKey,
                    this.password
                );
            }
            private get encryptedPrivateKey() {
                return this.localData ?
                        this.localData.priv :
                        '';
            }
            */
        /**
         * @return {?}
         */
        function () {
            return this.localData ?
                this.localData.priv :
                '';
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(ExistingEasyAccount.prototype, "isLoggedIn", {
        get: /**
         * @return {?}
         */
        function () {
            return this.isCreated ?
                this.localData.isLoggedIn :
                false;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(ExistingEasyAccount.prototype, "isCreated", {
        get: /**
         * @return {?}
         */
        function () {
            return this.localData != undefined &&
                this.localData.name != undefined;
        },
        enumerable: true,
        configurable: true
    });
    return ExistingEasyAccount;
}());
if (false) {
    /**
     * @type {?}
     * @private
     */
    ExistingEasyAccount.prototype.localData;
    /**
     * @type {?}
     * @private
     */
    ExistingEasyAccount.prototype.onChainData;
}

/**
 * @fileoverview added by tsickle
 * Generated from: lib/easy-account/model/account-manager.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
var EasyAccountManager = /** @class */ (function () {
    function EasyAccountManager() {
        this.account = new ExistingEasyAccount();
        this.account.load();
    }
    return EasyAccountManager;
}());
if (false) {
    /** @type {?} */
    EasyAccountManager.prototype.account;
}

/**
 * @fileoverview added by tsickle
 * Generated from: lib/translations/translation.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
var TranslationId = /** @class */ (function () {
    function TranslationId() {
    }
    TranslationId.ENGLISH = 'EN';
    TranslationId.CHINESE = 'CH';
    TranslationId.KOREAN = 'KO';
    TranslationId.JAPANESE = 'JP';
    return TranslationId;
}());
if (false) {
    /** @type {?} */
    TranslationId.ENGLISH;
    /** @type {?} */
    TranslationId.CHINESE;
    /** @type {?} */
    TranslationId.KOREAN;
    /** @type {?} */
    TranslationId.JAPANESE;
}
/**
 * @record
 */
function ITranslationBase() { }
if (false) {
    /** @type {?} */
    ITranslationBase.prototype.translationId;
    /** @type {?} */
    ITranslationBase.prototype.altForFlag;
}
/**
 * @record
 */
function ITranslationManager() { }
if (false) {
    /**
     * @return {?}
     */
    ITranslationManager.prototype.english = function () { };
    /**
     * @return {?}
     */
    ITranslationManager.prototype.chinese = function () { };
    /**
     * @return {?}
     */
    ITranslationManager.prototype.lynx = function () { };
}

/**
 * @fileoverview added by tsickle
 * Generated from: lib/translations/translation-manager.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
var BrowserLanguageCode = /** @class */ (function () {
    function BrowserLanguageCode() {
    }
    BrowserLanguageCode.ENGLISH = 'en';
    BrowserLanguageCode.CHINESE = 'zh';
    BrowserLanguageCode.KOREAN = 'ko';
    BrowserLanguageCode.JAPANESE = 'ja';
    return BrowserLanguageCode;
}());
if (false) {
    /** @type {?} */
    BrowserLanguageCode.ENGLISH;
    /** @type {?} */
    BrowserLanguageCode.CHINESE;
    /** @type {?} */
    BrowserLanguageCode.KOREAN;
    /** @type {?} */
    BrowserLanguageCode.JAPANESE;
}
/**
 * @abstract
 * @template T
 */
var  /**
 * @abstract
 * @template T
 */
TranslationManagerBase = /** @class */ (function () {
    function TranslationManagerBase() {
        this.savedLanguage = new LocalStorageData('selectedLanguage', false);
    }
    /**
     * @return {?}
     */
    TranslationManagerBase.prototype.init = /**
     * @return {?}
     */
    function () {
        /** @type {?} */
        var lang = getUrlParameterByName('lang');
        /** @type {?} */
        var isLanguageInUrl = lang != null;
        if (isLanguageInUrl) {
            this.selectLanguage(lang);
        }
        else {
            /** @type {?} */
            var lang_1 = this.savedLanguage.get();
            if (lang_1 == null) {
                lang_1 = this.getBrowserLanguage();
            }
            this.selectLanguage(lang_1);
        }
    };
    /**
     * @private
     * @return {?}
     */
    TranslationManagerBase.prototype.getBrowserLanguage = /**
     * @private
     * @return {?}
     */
    function () {
        /** @type {?} */
        var lang = window.navigator.languages ? window.navigator.languages[0] : null;
        lang = lang || window.navigator.language ||
            window.navigator['browserLanguage'] ||
            window.navigator['userLanguage'];
        if (lang.indexOf('-') !== -1)
            lang = lang.split('-')[0];
        if (lang.indexOf('_') !== -1)
            lang = lang.split('_')[0];
        lang = lang.toLowerCase();
        /** @type {?} */
        var id = TranslationId.ENGLISH;
        switch (lang) {
            case BrowserLanguageCode.CHINESE:
                id = TranslationId.CHINESE;
                break;
            case BrowserLanguageCode.KOREAN:
                id = TranslationId.KOREAN;
                break;
            case BrowserLanguageCode.JAPANESE:
                id = TranslationId.JAPANESE;
                break;
        }
        return id;
    };
    /**
     * @param {?} languageId
     * @return {?}
     */
    TranslationManagerBase.prototype.selectLanguage = /**
     * @param {?} languageId
     * @return {?}
     */
    function (languageId) {
        switch (languageId) {
            case TranslationId.CHINESE:
                this.chinese();
                break;
            case TranslationId.KOREAN:
                this.korean();
                break;
            case TranslationId.JAPANESE:
                this.japanese();
                break;
            default:
                this.english();
        }
    };
    /**
     * @return {?}
     */
    TranslationManagerBase.prototype.english = /**
     * @return {?}
     */
    function () {
        this._translation = this.getEnglish();
        this.savedLanguage.save(TranslationId.ENGLISH);
    };
    /**
     * @return {?}
     */
    TranslationManagerBase.prototype.chinese = /**
     * @return {?}
     */
    function () {
        this._translation = this.getChinese();
        this.savedLanguage.save(TranslationId.CHINESE);
    };
    /**
     * @return {?}
     */
    TranslationManagerBase.prototype.korean = /**
     * @return {?}
     */
    function () {
        this._translation = this.getKorean();
        this.savedLanguage.save(TranslationId.KOREAN);
    };
    /**
     * @return {?}
     */
    TranslationManagerBase.prototype.japanese = /**
     * @return {?}
     */
    function () {
        this._translation = this.getJapanese();
        this.savedLanguage.save(TranslationId.JAPANESE);
    };
    /**
     * @return {?}
     */
    TranslationManagerBase.prototype.lynx = /**
     * @return {?}
     */
    function () {
        this._translation = this.getLynx();
    };
    Object.defineProperty(TranslationManagerBase.prototype, "isEnglish", {
        get: /**
         * @return {?}
         */
        function () {
            return this.translation.translationId ==
                TranslationId.ENGLISH;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(TranslationManagerBase.prototype, "isChinese", {
        get: /**
         * @return {?}
         */
        function () {
            return this.translation.translationId ==
                TranslationId.CHINESE;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(TranslationManagerBase.prototype, "isKorean", {
        get: /**
         * @return {?}
         */
        function () {
            return this.translation.translationId ==
                TranslationId.KOREAN;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(TranslationManagerBase.prototype, "isJapanese", {
        get: /**
         * @return {?}
         */
        function () {
            return this.translation.translationId ==
                TranslationId.JAPANESE;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(TranslationManagerBase.prototype, "translation", {
        get: /**
         * @return {?}
         */
        function () {
            return this._translation;
        },
        enumerable: true,
        configurable: true
    });
    return TranslationManagerBase;
}());
if (false) {
    /**
     * @type {?}
     * @private
     */
    TranslationManagerBase.prototype.savedLanguage;
    /**
     * @type {?}
     * @private
     */
    TranslationManagerBase.prototype._translation;
    /**
     * @abstract
     * @protected
     * @return {?}
     */
    TranslationManagerBase.prototype.getEnglish = function () { };
    /**
     * @abstract
     * @protected
     * @return {?}
     */
    TranslationManagerBase.prototype.getChinese = function () { };
    /**
     * @abstract
     * @protected
     * @return {?}
     */
    TranslationManagerBase.prototype.getKorean = function () { };
    /**
     * @abstract
     * @protected
     * @return {?}
     */
    TranslationManagerBase.prototype.getJapanese = function () { };
    /**
     * @abstract
     * @protected
     * @return {?}
     */
    TranslationManagerBase.prototype.getLynx = function () { };
}

/**
 * @fileoverview added by tsickle
 * Generated from: front-end-api.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */

/**
 * @fileoverview added by tsickle
 * Generated from: earnbet-common-front-end.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */

export { EasyAccountManager, ExistingEasyAccount, LocalStorageData, RepeatedValue, SocketClientBase, StringValue, TranslationId, TranslationManagerBase, byteArrayToHexString, easyAccountDataKey, hexStringToByteArray };
//# sourceMappingURL=earnbet-common-front-end.js.map
