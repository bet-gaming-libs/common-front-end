(function (global, factory) {
    typeof exports === 'object' && typeof module !== 'undefined' ? factory(exports, require('earnbet-common')) :
    typeof define === 'function' && define.amd ? define('earnbet-common-front-end', ['exports', 'earnbet-common'], factory) :
    (global = global || self, factory(global['earnbet-common-front-end'] = {}, global.earnbetCommon));
}(this, (function (exports, earnbetCommon) { 'use strict';

    /*! *****************************************************************************
    Copyright (c) Microsoft Corporation.

    Permission to use, copy, modify, and/or distribute this software for any
    purpose with or without fee is hereby granted.

    THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH
    REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY
    AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT,
    INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM
    LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR
    OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
    PERFORMANCE OF THIS SOFTWARE.
    ***************************************************************************** */
    /* global Reflect, Promise */

    var extendStatics = function(d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };

    function __extends(d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    }

    var __assign = function() {
        __assign = Object.assign || function __assign(t) {
            for (var s, i = 1, n = arguments.length; i < n; i++) {
                s = arguments[i];
                for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p)) t[p] = s[p];
            }
            return t;
        };
        return __assign.apply(this, arguments);
    };

    function __rest(s, e) {
        var t = {};
        for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p) && e.indexOf(p) < 0)
            t[p] = s[p];
        if (s != null && typeof Object.getOwnPropertySymbols === "function")
            for (var i = 0, p = Object.getOwnPropertySymbols(s); i < p.length; i++) {
                if (e.indexOf(p[i]) < 0 && Object.prototype.propertyIsEnumerable.call(s, p[i]))
                    t[p[i]] = s[p[i]];
            }
        return t;
    }

    function __decorate(decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    }

    function __param(paramIndex, decorator) {
        return function (target, key) { decorator(target, key, paramIndex); }
    }

    function __metadata(metadataKey, metadataValue) {
        if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(metadataKey, metadataValue);
    }

    function __awaiter(thisArg, _arguments, P, generator) {
        function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
        return new (P || (P = Promise))(function (resolve, reject) {
            function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
            function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
            function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
            step((generator = generator.apply(thisArg, _arguments || [])).next());
        });
    }

    function __generator(thisArg, body) {
        var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
        return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
        function verb(n) { return function (v) { return step([n, v]); }; }
        function step(op) {
            if (f) throw new TypeError("Generator is already executing.");
            while (_) try {
                if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
                if (y = 0, t) op = [op[0] & 2, t.value];
                switch (op[0]) {
                    case 0: case 1: t = op; break;
                    case 4: _.label++; return { value: op[1], done: false };
                    case 5: _.label++; y = op[1]; op = [0]; continue;
                    case 7: op = _.ops.pop(); _.trys.pop(); continue;
                    default:
                        if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                        if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                        if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                        if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                        if (t[2]) _.ops.pop();
                        _.trys.pop(); continue;
                }
                op = body.call(thisArg, _);
            } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
            if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
        }
    }

    function __createBinding(o, m, k, k2) {
        if (k2 === undefined) k2 = k;
        o[k2] = m[k];
    }

    function __exportStar(m, exports) {
        for (var p in m) if (p !== "default" && !exports.hasOwnProperty(p)) exports[p] = m[p];
    }

    function __values(o) {
        var s = typeof Symbol === "function" && Symbol.iterator, m = s && o[s], i = 0;
        if (m) return m.call(o);
        if (o && typeof o.length === "number") return {
            next: function () {
                if (o && i >= o.length) o = void 0;
                return { value: o && o[i++], done: !o };
            }
        };
        throw new TypeError(s ? "Object is not iterable." : "Symbol.iterator is not defined.");
    }

    function __read(o, n) {
        var m = typeof Symbol === "function" && o[Symbol.iterator];
        if (!m) return o;
        var i = m.call(o), r, ar = [], e;
        try {
            while ((n === void 0 || n-- > 0) && !(r = i.next()).done) ar.push(r.value);
        }
        catch (error) { e = { error: error }; }
        finally {
            try {
                if (r && !r.done && (m = i["return"])) m.call(i);
            }
            finally { if (e) throw e.error; }
        }
        return ar;
    }

    function __spread() {
        for (var ar = [], i = 0; i < arguments.length; i++)
            ar = ar.concat(__read(arguments[i]));
        return ar;
    }

    function __spreadArrays() {
        for (var s = 0, i = 0, il = arguments.length; i < il; i++) s += arguments[i].length;
        for (var r = Array(s), k = 0, i = 0; i < il; i++)
            for (var a = arguments[i], j = 0, jl = a.length; j < jl; j++, k++)
                r[k] = a[j];
        return r;
    };

    function __await(v) {
        return this instanceof __await ? (this.v = v, this) : new __await(v);
    }

    function __asyncGenerator(thisArg, _arguments, generator) {
        if (!Symbol.asyncIterator) throw new TypeError("Symbol.asyncIterator is not defined.");
        var g = generator.apply(thisArg, _arguments || []), i, q = [];
        return i = {}, verb("next"), verb("throw"), verb("return"), i[Symbol.asyncIterator] = function () { return this; }, i;
        function verb(n) { if (g[n]) i[n] = function (v) { return new Promise(function (a, b) { q.push([n, v, a, b]) > 1 || resume(n, v); }); }; }
        function resume(n, v) { try { step(g[n](v)); } catch (e) { settle(q[0][3], e); } }
        function step(r) { r.value instanceof __await ? Promise.resolve(r.value.v).then(fulfill, reject) : settle(q[0][2], r); }
        function fulfill(value) { resume("next", value); }
        function reject(value) { resume("throw", value); }
        function settle(f, v) { if (f(v), q.shift(), q.length) resume(q[0][0], q[0][1]); }
    }

    function __asyncDelegator(o) {
        var i, p;
        return i = {}, verb("next"), verb("throw", function (e) { throw e; }), verb("return"), i[Symbol.iterator] = function () { return this; }, i;
        function verb(n, f) { i[n] = o[n] ? function (v) { return (p = !p) ? { value: __await(o[n](v)), done: n === "return" } : f ? f(v) : v; } : f; }
    }

    function __asyncValues(o) {
        if (!Symbol.asyncIterator) throw new TypeError("Symbol.asyncIterator is not defined.");
        var m = o[Symbol.asyncIterator], i;
        return m ? m.call(o) : (o = typeof __values === "function" ? __values(o) : o[Symbol.iterator](), i = {}, verb("next"), verb("throw"), verb("return"), i[Symbol.asyncIterator] = function () { return this; }, i);
        function verb(n) { i[n] = o[n] && function (v) { return new Promise(function (resolve, reject) { v = o[n](v), settle(resolve, reject, v.done, v.value); }); }; }
        function settle(resolve, reject, d, v) { Promise.resolve(v).then(function(v) { resolve({ value: v, done: d }); }, reject); }
    }

    function __makeTemplateObject(cooked, raw) {
        if (Object.defineProperty) { Object.defineProperty(cooked, "raw", { value: raw }); } else { cooked.raw = raw; }
        return cooked;
    };

    function __importStar(mod) {
        if (mod && mod.__esModule) return mod;
        var result = {};
        if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
        result.default = mod;
        return result;
    }

    function __importDefault(mod) {
        return (mod && mod.__esModule) ? mod : { default: mod };
    }

    function __classPrivateFieldGet(receiver, privateMap) {
        if (!privateMap.has(receiver)) {
            throw new TypeError("attempted to get private field on non-instance");
        }
        return privateMap.get(receiver);
    }

    function __classPrivateFieldSet(receiver, privateMap, value) {
        if (!privateMap.has(receiver)) {
            throw new TypeError("attempted to set private field on non-instance");
        }
        privateMap.set(receiver, value);
        return value;
    }

    /**
     * @fileoverview added by tsickle
     * Generated from: lib/server/socket-client-base.ts
     * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    /**
     * @abstract
     */
    var   /**
     * @abstract
     */
    SocketClientBase = /** @class */ (function () {
        // CLASS members:
        // INSTANCE members:
        function SocketClientBase(config, nameSpace) {
            var _this = this;
            this.config = config;
            this.nameSpace = nameSpace;
            this.onConnect = (/**
             * @return {?}
             */
            function () {
                _this.onConnected();
            });
            this.onError = (/**
             * @param {?} error
             * @return {?}
             */
            function (error) {
                console.log('onError');
                console.log(error);
            });
            this.onDisconnect = (/**
             * @return {?}
             */
            function () {
                _this.onDisconnected();
            });
        }
        // *** ESTABLISH CONNECTION to SocketServer ***
        // *** ESTABLISH CONNECTION to SocketServer ***
        /**
         * @protected
         * @return {?}
         */
        SocketClientBase.prototype.connectSocket = 
        // *** ESTABLISH CONNECTION to SocketServer ***
        /**
         * @protected
         * @return {?}
         */
        function () {
            // connect scoket
            /** @type {?} */
            var url = 'http' +
                (this.config.ssl === true ? 's' : '') +
                '://' + this.config.host + ':' +
                this.config.port + this.nameSpace;
            /** @type {?} */
            var socket = io(url);
            socket.on('connect', this.onConnect);
            socket.on('disconnect', this.onConnect);
            socket.on('connect_error', this.onError);
            socket.on('error', this.onError);
            return socket;
        };
        return SocketClientBase;
    }());
    if (false) {
        /**
         * @type {?}
         * @private
         */
        SocketClientBase.prototype.onConnect;
        /**
         * @type {?}
         * @private
         */
        SocketClientBase.prototype.onError;
        /**
         * @type {?}
         * @private
         */
        SocketClientBase.prototype.onDisconnect;
        /**
         * @type {?}
         * @private
         */
        SocketClientBase.prototype.config;
        /**
         * @type {?}
         * @private
         */
        SocketClientBase.prototype.nameSpace;
        /**
         * @abstract
         * @protected
         * @return {?}
         */
        SocketClientBase.prototype.onConnected = function () { };
        /**
         * @abstract
         * @protected
         * @return {?}
         */
        SocketClientBase.prototype.onDisconnected = function () { };
    }

    /**
     * @fileoverview added by tsickle
     * Generated from: lib/util/buffer-util.ts
     * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    /**
     * @param {?} uint8arr
     * @return {?}
     */
    function byteArrayToHexString(uint8arr) {
        if (!uint8arr) {
            return '';
        }
        /** @type {?} */
        var hexStr = '';
        for (var i = 0; i < uint8arr.length; i++) {
            /** @type {?} */
            var hex = (uint8arr[i] & 0xff).toString(16);
            hex = (hex.length === 1) ? '0' + hex : hex;
            hexStr += hex;
        }
        return hexStr.toUpperCase();
    }
    /**
     * @param {?} str
     * @return {?}
     */
    function hexStringToByteArray(str) {
        if (!str) {
            return new Uint8Array(0);
        }
        /** @type {?} */
        var a = [];
        for (var i = 0, len = str.length; i < len; i += 2) {
            a.push(parseInt(str.substr(i, 2), 16));
        }
        return new Uint8Array(a);
    }

    /**
     * @fileoverview added by tsickle
     * Generated from: lib/util/local-storage-util.ts
     * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    /**
     * @template T
     */
    var   /**
     * @template T
     */
    LocalStorageData = /** @class */ (function () {
        function LocalStorageData(dataKey, isObject) {
            this.dataKey = dataKey;
            this.isObject = isObject;
        }
        /**
         * @return {?}
         */
        LocalStorageData.prototype.isSaved = /**
         * @return {?}
         */
        function () {
            return this.get() != null;
        };
        /**
         * @return {?}
         */
        LocalStorageData.prototype.get = /**
         * @return {?}
         */
        function () {
            /** @type {?} */
            var data = localStorage.getItem(this.dataKey);
            return data != null ?
                (this.isObject ?
                    JSON.parse(data) :
                    data) :
                null;
        };
        /**
         * @param {?} data
         * @return {?}
         */
        LocalStorageData.prototype.save = /**
         * @param {?} data
         * @return {?}
         */
        function (data) {
            localStorage.setItem(this.dataKey, (this.isObject ?
                JSON.stringify(data) :
                '' + data));
        };
        return LocalStorageData;
    }());
    if (false) {
        /**
         * @type {?}
         * @private
         */
        LocalStorageData.prototype.dataKey;
        /**
         * @type {?}
         * @private
         */
        LocalStorageData.prototype.isObject;
    }

    /**
     * @fileoverview added by tsickle
     * Generated from: lib/easy-account/model/data.ts
     * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    /** @type {?} */
    var easyAccountDataKey = 'EasyAccountData';
    /**
     * @record
     */
    function IEasyAccountData() { }
    if (false) {
        /** @type {?} */
        IEasyAccountData.prototype.id;
        /** @type {?} */
        IEasyAccountData.prototype.pub;
        /** @type {?} */
        IEasyAccountData.prototype.priv;
        /** @type {?} */
        IEasyAccountData.prototype.name;
        /** @type {?} */
        IEasyAccountData.prototype.isLoggedIn;
    }
    /**
     * @record
     */
    function IEasyAccountTableRow() { }
    if (false) {
        /** @type {?} */
        IEasyAccountTableRow.prototype.id;
        /** @type {?} */
        IEasyAccountTableRow.prototype.key;
        /** @type {?} */
        IEasyAccountTableRow.prototype.email_backup;
        /** @type {?} */
        IEasyAccountTableRow.prototype.nonce;
        /** @type {?} */
        IEasyAccountTableRow.prototype.bet_balance;
        /** @type {?} */
        IEasyAccountTableRow.prototype.eos_balance;
        /** @type {?} */
        IEasyAccountTableRow.prototype.eos_wagered;
        /** @type {?} */
        IEasyAccountTableRow.prototype.free_account;
        /** @type {?} */
        IEasyAccountTableRow.prototype.free_transfers;
        /** @type {?} */
        IEasyAccountTableRow.prototype.rewards_balance;
    }

    /**
     * @fileoverview added by tsickle
     * Generated from: lib/easy-account/data/api-data.ts
     * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    /**
     * @record
     */
    function IEasyAccountDataForApi() { }
    if (false) {
        /** @type {?} */
        IEasyAccountDataForApi.prototype.id;
        /** @type {?} */
        IEasyAccountDataForApi.prototype.publicKey;
        /** @type {?} */
        IEasyAccountDataForApi.prototype.name;
        /** @type {?} */
        IEasyAccountDataForApi.prototype.email;
        /** @type {?} */
        IEasyAccountDataForApi.prototype.recaptcha;
    }

    /**
     * @fileoverview added by tsickle
     * Generated from: lib/easy-account/model/forms/fields.ts
     * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    var StringValue = /** @class */ (function () {
        function StringValue(minimumLength, maximumLength, shouldTrim) {
            if (maximumLength === void 0) { maximumLength = 256; }
            if (shouldTrim === void 0) { shouldTrim = false; }
            this.minimumLength = minimumLength;
            this.maximumLength = maximumLength;
            this.shouldTrim = shouldTrim;
            this.value = '';
            this._isInvalid = false;
        }
        /**
         * @return {?}
         */
        StringValue.prototype.validate = /**
         * @return {?}
         */
        function () {
            this._isInvalid = false;
            if (this.shouldTrim) {
                this.value = earnbetCommon.trimString(this.value);
            }
            /** @type {?} */
            var isValid = this.value.length >= this.minimumLength &&
                this.value.length <= this.maximumLength;
            this._isInvalid = !isValid;
            return isValid;
        };
        Object.defineProperty(StringValue.prototype, "isInvalid", {
            get: /**
             * @return {?}
             */
            function () {
                return this._isInvalid;
            },
            enumerable: true,
            configurable: true
        });
        return StringValue;
    }());
    if (false) {
        /** @type {?} */
        StringValue.prototype.value;
        /**
         * @type {?}
         * @private
         */
        StringValue.prototype._isInvalid;
        /** @type {?} */
        StringValue.prototype.minimumLength;
        /** @type {?} */
        StringValue.prototype.maximumLength;
        /** @type {?} */
        StringValue.prototype.shouldTrim;
    }
    var RepeatedValue = /** @class */ (function (_super) {
        __extends(RepeatedValue, _super);
        function RepeatedValue(minimumLength) {
            if (minimumLength === void 0) { minimumLength = 1; }
            var _this = _super.call(this, minimumLength) || this;
            _this.copy = '';
            return _this;
        }
        /**
         * @return {?}
         */
        RepeatedValue.prototype.validate = /**
         * @return {?}
         */
        function () {
            return _super.prototype.validate.call(this) &&
                this.value == this.copy;
        };
        return RepeatedValue;
    }(StringValue));
    if (false) {
        /** @type {?} */
        RepeatedValue.prototype.copy;
    }

    /**
     * @fileoverview added by tsickle
     * Generated from: lib/easy-account/model/existing-account.ts
     * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    var ExistingEasyAccount = /** @class */ (function () {
        function ExistingEasyAccount() {
            /*
            setInterval(
                this.getDataFromChain,
                1000*60
            );
            */
        }
        /**
         * @return {?}
         */
        ExistingEasyAccount.prototype.load = /**
         * @return {?}
         */
        function () {
            this.localData = undefined;
            /** @type {?} */
            var savedAccountData = localStorage.getItem(easyAccountDataKey);
            if (savedAccountData != null) {
                this.localData = JSON.parse(savedAccountData);
            }
        };
        /*
        login(password:string):boolean {
            if (!this.isCreated) {
                return false;
            }

            this.password = password;

            const derivedPublicKey = eos_ecc.privateToPublic(
                this.privateKey
            );

            const isAuthenticated =
                derivedPublicKey == this.publicKey;

            this._isLoggedIn = isAuthenticated;

            return isAuthenticated;
        }
        */
        /*
            login(password:string):boolean {
                if (!this.isCreated) {
                    return false;
                }
        
                this.password = password;
        
                const derivedPublicKey = eos_ecc.privateToPublic(
                    this.privateKey
                );
        
                const isAuthenticated =
                    derivedPublicKey == this.publicKey;
        
                this._isLoggedIn = isAuthenticated;
        
                return isAuthenticated;
            }
            */
        /**
         * @return {?}
         */
        ExistingEasyAccount.prototype.login = /*
            login(password:string):boolean {
                if (!this.isCreated) {
                    return false;
                }
        
                this.password = password;
        
                const derivedPublicKey = eos_ecc.privateToPublic(
                    this.privateKey
                );
        
                const isAuthenticated =
                    derivedPublicKey == this.publicKey;
        
                this._isLoggedIn = isAuthenticated;
        
                return isAuthenticated;
            }
            */
        /**
         * @return {?}
         */
        function () {
            if (!this.isCreated) {
                return;
            }
            this.localData.isLoggedIn = true;
            this.save();
        };
        /**
         * @return {?}
         */
        ExistingEasyAccount.prototype.logout = /**
         * @return {?}
         */
        function () {
            if (!this.isCreated) {
                return;
            }
            this.localData.isLoggedIn = false;
            this.save();
        };
        /**
         * @private
         * @return {?}
         */
        ExistingEasyAccount.prototype.save = /**
         * @private
         * @return {?}
         */
        function () {
            if (!this.isCreated) {
                return;
            }
            localStorage.setItem(easyAccountDataKey, JSON.stringify(this.localData));
        };
        Object.defineProperty(ExistingEasyAccount.prototype, "eosBalance", {
            get: /**
             * @return {?}
             */
            function () {
                return this.onChainData ?
                    this.onChainData.eos_balance :
                    '0.0000 EOS';
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ExistingEasyAccount.prototype, "id", {
            get: /**
             * @return {?}
             */
            function () {
                return this.isCreated ?
                    this.localData.id :
                    0;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ExistingEasyAccount.prototype, "name", {
            get: /**
             * @return {?}
             */
            function () {
                return this.localData ?
                    this.localData.name :
                    '';
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ExistingEasyAccount.prototype, "publicKey", {
            get: /**
             * @return {?}
             */
            function () {
                return this.localData ?
                    this.localData.pub :
                    '';
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ExistingEasyAccount.prototype, "privateKey", {
            /*
            get privateKey():string {
                if (!this.isCreated) {
                    return '';
                }
        
                return AES.decrypt(
                    this.encryptedPrivateKey,
                    this.password
                );
            }
            private get encryptedPrivateKey() {
                return this.localData ?
                        this.localData.priv :
                        '';
            }
            */
            get: /*
                get privateKey():string {
                    if (!this.isCreated) {
                        return '';
                    }
            
                    return AES.decrypt(
                        this.encryptedPrivateKey,
                        this.password
                    );
                }
                private get encryptedPrivateKey() {
                    return this.localData ?
                            this.localData.priv :
                            '';
                }
                */
            /**
             * @return {?}
             */
            function () {
                return this.localData ?
                    this.localData.priv :
                    '';
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ExistingEasyAccount.prototype, "isLoggedIn", {
            get: /**
             * @return {?}
             */
            function () {
                return this.isCreated ?
                    this.localData.isLoggedIn :
                    false;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ExistingEasyAccount.prototype, "isCreated", {
            get: /**
             * @return {?}
             */
            function () {
                return this.localData != undefined &&
                    this.localData.name != undefined;
            },
            enumerable: true,
            configurable: true
        });
        return ExistingEasyAccount;
    }());
    if (false) {
        /**
         * @type {?}
         * @private
         */
        ExistingEasyAccount.prototype.localData;
        /**
         * @type {?}
         * @private
         */
        ExistingEasyAccount.prototype.onChainData;
    }

    /**
     * @fileoverview added by tsickle
     * Generated from: lib/easy-account/model/account-manager.ts
     * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    var EasyAccountManager = /** @class */ (function () {
        function EasyAccountManager() {
            this.account = new ExistingEasyAccount();
            this.account.load();
        }
        return EasyAccountManager;
    }());
    if (false) {
        /** @type {?} */
        EasyAccountManager.prototype.account;
    }

    /**
     * @fileoverview added by tsickle
     * Generated from: lib/translations/translation.ts
     * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    var TranslationId = /** @class */ (function () {
        function TranslationId() {
        }
        TranslationId.ENGLISH = 'EN';
        TranslationId.CHINESE = 'CH';
        TranslationId.KOREAN = 'KO';
        TranslationId.JAPANESE = 'JP';
        return TranslationId;
    }());
    if (false) {
        /** @type {?} */
        TranslationId.ENGLISH;
        /** @type {?} */
        TranslationId.CHINESE;
        /** @type {?} */
        TranslationId.KOREAN;
        /** @type {?} */
        TranslationId.JAPANESE;
    }
    /**
     * @record
     */
    function ITranslationBase() { }
    if (false) {
        /** @type {?} */
        ITranslationBase.prototype.translationId;
        /** @type {?} */
        ITranslationBase.prototype.altForFlag;
    }
    /**
     * @record
     */
    function ITranslationManager() { }
    if (false) {
        /**
         * @return {?}
         */
        ITranslationManager.prototype.english = function () { };
        /**
         * @return {?}
         */
        ITranslationManager.prototype.chinese = function () { };
        /**
         * @return {?}
         */
        ITranslationManager.prototype.lynx = function () { };
    }

    /**
     * @fileoverview added by tsickle
     * Generated from: lib/translations/translation-manager.ts
     * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    var BrowserLanguageCode = /** @class */ (function () {
        function BrowserLanguageCode() {
        }
        BrowserLanguageCode.ENGLISH = 'en';
        BrowserLanguageCode.CHINESE = 'zh';
        BrowserLanguageCode.KOREAN = 'ko';
        BrowserLanguageCode.JAPANESE = 'ja';
        return BrowserLanguageCode;
    }());
    if (false) {
        /** @type {?} */
        BrowserLanguageCode.ENGLISH;
        /** @type {?} */
        BrowserLanguageCode.CHINESE;
        /** @type {?} */
        BrowserLanguageCode.KOREAN;
        /** @type {?} */
        BrowserLanguageCode.JAPANESE;
    }
    /**
     * @abstract
     * @template T
     */
    var   /**
     * @abstract
     * @template T
     */
    TranslationManagerBase = /** @class */ (function () {
        function TranslationManagerBase() {
            this.savedLanguage = new LocalStorageData('selectedLanguage', false);
        }
        /**
         * @return {?}
         */
        TranslationManagerBase.prototype.init = /**
         * @return {?}
         */
        function () {
            /** @type {?} */
            var lang = earnbetCommon.getUrlParameterByName('lang');
            /** @type {?} */
            var isLanguageInUrl = lang != null;
            if (isLanguageInUrl) {
                this.selectLanguage(lang);
            }
            else {
                /** @type {?} */
                var lang_1 = this.savedLanguage.get();
                if (lang_1 == null) {
                    lang_1 = this.getBrowserLanguage();
                }
                this.selectLanguage(lang_1);
            }
        };
        /**
         * @private
         * @return {?}
         */
        TranslationManagerBase.prototype.getBrowserLanguage = /**
         * @private
         * @return {?}
         */
        function () {
            /** @type {?} */
            var lang = window.navigator.languages ? window.navigator.languages[0] : null;
            lang = lang || window.navigator.language ||
                window.navigator['browserLanguage'] ||
                window.navigator['userLanguage'];
            if (lang.indexOf('-') !== -1)
                lang = lang.split('-')[0];
            if (lang.indexOf('_') !== -1)
                lang = lang.split('_')[0];
            lang = lang.toLowerCase();
            /** @type {?} */
            var id = TranslationId.ENGLISH;
            switch (lang) {
                case BrowserLanguageCode.CHINESE:
                    id = TranslationId.CHINESE;
                    break;
                case BrowserLanguageCode.KOREAN:
                    id = TranslationId.KOREAN;
                    break;
                case BrowserLanguageCode.JAPANESE:
                    id = TranslationId.JAPANESE;
                    break;
            }
            return id;
        };
        /**
         * @param {?} languageId
         * @return {?}
         */
        TranslationManagerBase.prototype.selectLanguage = /**
         * @param {?} languageId
         * @return {?}
         */
        function (languageId) {
            switch (languageId) {
                case TranslationId.CHINESE:
                    this.chinese();
                    break;
                case TranslationId.KOREAN:
                    this.korean();
                    break;
                case TranslationId.JAPANESE:
                    this.japanese();
                    break;
                default:
                    this.english();
            }
        };
        /**
         * @return {?}
         */
        TranslationManagerBase.prototype.english = /**
         * @return {?}
         */
        function () {
            this._translation = this.getEnglish();
            this.savedLanguage.save(TranslationId.ENGLISH);
        };
        /**
         * @return {?}
         */
        TranslationManagerBase.prototype.chinese = /**
         * @return {?}
         */
        function () {
            this._translation = this.getChinese();
            this.savedLanguage.save(TranslationId.CHINESE);
        };
        /**
         * @return {?}
         */
        TranslationManagerBase.prototype.korean = /**
         * @return {?}
         */
        function () {
            this._translation = this.getKorean();
            this.savedLanguage.save(TranslationId.KOREAN);
        };
        /**
         * @return {?}
         */
        TranslationManagerBase.prototype.japanese = /**
         * @return {?}
         */
        function () {
            this._translation = this.getJapanese();
            this.savedLanguage.save(TranslationId.JAPANESE);
        };
        /**
         * @return {?}
         */
        TranslationManagerBase.prototype.lynx = /**
         * @return {?}
         */
        function () {
            this._translation = this.getLynx();
        };
        Object.defineProperty(TranslationManagerBase.prototype, "isEnglish", {
            get: /**
             * @return {?}
             */
            function () {
                return this.translation.translationId ==
                    TranslationId.ENGLISH;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(TranslationManagerBase.prototype, "isChinese", {
            get: /**
             * @return {?}
             */
            function () {
                return this.translation.translationId ==
                    TranslationId.CHINESE;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(TranslationManagerBase.prototype, "isKorean", {
            get: /**
             * @return {?}
             */
            function () {
                return this.translation.translationId ==
                    TranslationId.KOREAN;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(TranslationManagerBase.prototype, "isJapanese", {
            get: /**
             * @return {?}
             */
            function () {
                return this.translation.translationId ==
                    TranslationId.JAPANESE;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(TranslationManagerBase.prototype, "translation", {
            get: /**
             * @return {?}
             */
            function () {
                return this._translation;
            },
            enumerable: true,
            configurable: true
        });
        return TranslationManagerBase;
    }());
    if (false) {
        /**
         * @type {?}
         * @private
         */
        TranslationManagerBase.prototype.savedLanguage;
        /**
         * @type {?}
         * @private
         */
        TranslationManagerBase.prototype._translation;
        /**
         * @abstract
         * @protected
         * @return {?}
         */
        TranslationManagerBase.prototype.getEnglish = function () { };
        /**
         * @abstract
         * @protected
         * @return {?}
         */
        TranslationManagerBase.prototype.getChinese = function () { };
        /**
         * @abstract
         * @protected
         * @return {?}
         */
        TranslationManagerBase.prototype.getKorean = function () { };
        /**
         * @abstract
         * @protected
         * @return {?}
         */
        TranslationManagerBase.prototype.getJapanese = function () { };
        /**
         * @abstract
         * @protected
         * @return {?}
         */
        TranslationManagerBase.prototype.getLynx = function () { };
    }

    exports.EasyAccountManager = EasyAccountManager;
    exports.ExistingEasyAccount = ExistingEasyAccount;
    exports.LocalStorageData = LocalStorageData;
    exports.RepeatedValue = RepeatedValue;
    exports.SocketClientBase = SocketClientBase;
    exports.StringValue = StringValue;
    exports.TranslationId = TranslationId;
    exports.TranslationManagerBase = TranslationManagerBase;
    exports.byteArrayToHexString = byteArrayToHexString;
    exports.easyAccountDataKey = easyAccountDataKey;
    exports.hexStringToByteArray = hexStringToByteArray;

    Object.defineProperty(exports, '__esModule', { value: true });

})));
//# sourceMappingURL=earnbet-common-front-end.umd.js.map
