/**
 * @fileoverview added by tsickle
 * Generated from: lib/translations/translation.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
export class TranslationId {
}
TranslationId.ENGLISH = 'EN';
TranslationId.CHINESE = 'CH';
TranslationId.KOREAN = 'KO';
TranslationId.JAPANESE = 'JP';
if (false) {
    /** @type {?} */
    TranslationId.ENGLISH;
    /** @type {?} */
    TranslationId.CHINESE;
    /** @type {?} */
    TranslationId.KOREAN;
    /** @type {?} */
    TranslationId.JAPANESE;
}
/**
 * @record
 */
export function ITranslationBase() { }
if (false) {
    /** @type {?} */
    ITranslationBase.prototype.translationId;
    /** @type {?} */
    ITranslationBase.prototype.altForFlag;
}
/**
 * @record
 */
export function ITranslationManager() { }
if (false) {
    /**
     * @return {?}
     */
    ITranslationManager.prototype.english = function () { };
    /**
     * @return {?}
     */
    ITranslationManager.prototype.chinese = function () { };
    /**
     * @return {?}
     */
    ITranslationManager.prototype.lynx = function () { };
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidHJhbnNsYXRpb24uanMiLCJzb3VyY2VSb290Ijoibmc6Ly9lYXJuYmV0LWNvbW1vbi1mcm9udC1lbmQvIiwic291cmNlcyI6WyJsaWIvdHJhbnNsYXRpb25zL3RyYW5zbGF0aW9uLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7O0FBQUEsTUFBTSxPQUFPLGFBQWE7O0FBRWYscUJBQU8sR0FBRyxJQUFJLENBQUM7QUFDZixxQkFBTyxHQUFHLElBQUksQ0FBQztBQUNmLG9CQUFNLEdBQUcsSUFBSSxDQUFDO0FBQ2Qsc0JBQVEsR0FBRyxJQUFJLENBQUM7OztJQUh2QixzQkFBc0I7O0lBQ3RCLHNCQUFzQjs7SUFDdEIscUJBQXFCOztJQUNyQix1QkFBdUI7Ozs7O0FBRzNCLHNDQUlDOzs7SUFGRyx5Q0FBcUI7O0lBQ3JCLHNDQUFrQjs7Ozs7QUFHdEIseUNBS0M7Ozs7O0lBSEcsd0RBQWU7Ozs7SUFDZix3REFBZTs7OztJQUNmLHFEQUFZIiwic291cmNlc0NvbnRlbnQiOlsiZXhwb3J0IGNsYXNzIFRyYW5zbGF0aW9uSWRcbntcbiAgICBzdGF0aWMgRU5HTElTSCA9ICdFTic7XG4gICAgc3RhdGljIENISU5FU0UgPSAnQ0gnO1xuICAgIHN0YXRpYyBLT1JFQU4gPSAnS08nO1xuICAgIHN0YXRpYyBKQVBBTkVTRSA9ICdKUCc7XG59XG5cbmV4cG9ydCBpbnRlcmZhY2UgSVRyYW5zbGF0aW9uQmFzZVxue1xuICAgIHRyYW5zbGF0aW9uSWQ6c3RyaW5nO1xuICAgIGFsdEZvckZsYWc6c3RyaW5nO1xufVxuXG5leHBvcnQgaW50ZXJmYWNlIElUcmFuc2xhdGlvbk1hbmFnZXJcbntcbiAgICBlbmdsaXNoKCk6dm9pZDtcbiAgICBjaGluZXNlKCk6dm9pZDtcbiAgICBseW54KCk6dm9pZDtcbn0iXX0=