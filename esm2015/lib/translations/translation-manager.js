/**
 * @fileoverview added by tsickle
 * Generated from: lib/translations/translation-manager.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { getUrlParameterByName } from "earnbet-common";
import { TranslationId } from "./translation";
import { LocalStorageData } from "../util/local-storage-util";
class BrowserLanguageCode {
}
BrowserLanguageCode.ENGLISH = 'en';
BrowserLanguageCode.CHINESE = 'zh';
BrowserLanguageCode.KOREAN = 'ko';
BrowserLanguageCode.JAPANESE = 'ja';
if (false) {
    /** @type {?} */
    BrowserLanguageCode.ENGLISH;
    /** @type {?} */
    BrowserLanguageCode.CHINESE;
    /** @type {?} */
    BrowserLanguageCode.KOREAN;
    /** @type {?} */
    BrowserLanguageCode.JAPANESE;
}
/**
 * @abstract
 * @template T
 */
export class TranslationManagerBase {
    constructor() {
        this.savedLanguage = new LocalStorageData('selectedLanguage', false);
    }
    /**
     * @return {?}
     */
    init() {
        /** @type {?} */
        const lang = getUrlParameterByName('lang');
        /** @type {?} */
        const isLanguageInUrl = lang != null;
        if (isLanguageInUrl) {
            this.selectLanguage(lang);
        }
        else {
            /** @type {?} */
            let lang = this.savedLanguage.get();
            if (lang == null) {
                lang = this.getBrowserLanguage();
            }
            this.selectLanguage(lang);
        }
    }
    /**
     * @private
     * @return {?}
     */
    getBrowserLanguage() {
        /** @type {?} */
        let lang = window.navigator.languages ? window.navigator.languages[0] : null;
        lang = lang || window.navigator.language ||
            window.navigator['browserLanguage'] ||
            window.navigator['userLanguage'];
        if (lang.indexOf('-') !== -1)
            lang = lang.split('-')[0];
        if (lang.indexOf('_') !== -1)
            lang = lang.split('_')[0];
        lang = lang.toLowerCase();
        /** @type {?} */
        let id = TranslationId.ENGLISH;
        switch (lang) {
            case BrowserLanguageCode.CHINESE:
                id = TranslationId.CHINESE;
                break;
            case BrowserLanguageCode.KOREAN:
                id = TranslationId.KOREAN;
                break;
            case BrowserLanguageCode.JAPANESE:
                id = TranslationId.JAPANESE;
                break;
        }
        return id;
    }
    /**
     * @param {?} languageId
     * @return {?}
     */
    selectLanguage(languageId) {
        switch (languageId) {
            case TranslationId.CHINESE:
                this.chinese();
                break;
            case TranslationId.KOREAN:
                this.korean();
                break;
            case TranslationId.JAPANESE:
                this.japanese();
                break;
            default:
                this.english();
        }
    }
    /**
     * @return {?}
     */
    english() {
        this._translation = this.getEnglish();
        this.savedLanguage.save(TranslationId.ENGLISH);
    }
    /**
     * @return {?}
     */
    chinese() {
        this._translation = this.getChinese();
        this.savedLanguage.save(TranslationId.CHINESE);
    }
    /**
     * @return {?}
     */
    korean() {
        this._translation = this.getKorean();
        this.savedLanguage.save(TranslationId.KOREAN);
    }
    /**
     * @return {?}
     */
    japanese() {
        this._translation = this.getJapanese();
        this.savedLanguage.save(TranslationId.JAPANESE);
    }
    /**
     * @return {?}
     */
    lynx() {
        this._translation = this.getLynx();
    }
    /**
     * @return {?}
     */
    get isEnglish() {
        return this.translation.translationId ==
            TranslationId.ENGLISH;
    }
    /**
     * @return {?}
     */
    get isChinese() {
        return this.translation.translationId ==
            TranslationId.CHINESE;
    }
    /**
     * @return {?}
     */
    get isKorean() {
        return this.translation.translationId ==
            TranslationId.KOREAN;
    }
    /**
     * @return {?}
     */
    get isJapanese() {
        return this.translation.translationId ==
            TranslationId.JAPANESE;
    }
    /**
     * @return {?}
     */
    get translation() {
        return this._translation;
    }
}
if (false) {
    /**
     * @type {?}
     * @private
     */
    TranslationManagerBase.prototype.savedLanguage;
    /**
     * @type {?}
     * @private
     */
    TranslationManagerBase.prototype._translation;
    /**
     * @abstract
     * @protected
     * @return {?}
     */
    TranslationManagerBase.prototype.getEnglish = function () { };
    /**
     * @abstract
     * @protected
     * @return {?}
     */
    TranslationManagerBase.prototype.getChinese = function () { };
    /**
     * @abstract
     * @protected
     * @return {?}
     */
    TranslationManagerBase.prototype.getKorean = function () { };
    /**
     * @abstract
     * @protected
     * @return {?}
     */
    TranslationManagerBase.prototype.getJapanese = function () { };
    /**
     * @abstract
     * @protected
     * @return {?}
     */
    TranslationManagerBase.prototype.getLynx = function () { };
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidHJhbnNsYXRpb24tbWFuYWdlci5qcyIsInNvdXJjZVJvb3QiOiJuZzovL2Vhcm5iZXQtY29tbW9uLWZyb250LWVuZC8iLCJzb3VyY2VzIjpbImxpYi90cmFuc2xhdGlvbnMvdHJhbnNsYXRpb24tbWFuYWdlci50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7OztBQUFBLE9BQU8sRUFBRSxxQkFBcUIsRUFBRSxNQUFNLGdCQUFnQixDQUFDO0FBRXZELE9BQU8sRUFBRSxhQUFhLEVBQXlDLE1BQU0sZUFBZSxDQUFDO0FBQ3JGLE9BQU8sRUFBRSxnQkFBZ0IsRUFBRSxNQUFNLDRCQUE0QixDQUFDO0FBRzlELE1BQU0sbUJBQW1COztBQUVkLDJCQUFPLEdBQUcsSUFBSSxDQUFDO0FBQ2YsMkJBQU8sR0FBRyxJQUFJLENBQUM7QUFDZiwwQkFBTSxHQUFHLElBQUksQ0FBQztBQUNkLDRCQUFRLEdBQUcsSUFBSSxDQUFDOzs7SUFIdkIsNEJBQXNCOztJQUN0Qiw0QkFBc0I7O0lBQ3RCLDJCQUFxQjs7SUFDckIsNkJBQXVCOzs7Ozs7QUFJM0IsTUFBTSxPQUFnQixzQkFBc0I7SUFVeEM7UUFQUSxrQkFBYSxHQUE0QixJQUFJLGdCQUFnQixDQUNqRSxrQkFBa0IsRUFDbEIsS0FBSyxDQUNSLENBQUM7SUFLRixDQUFDOzs7O0lBQ0QsSUFBSTs7Y0FDTSxJQUFJLEdBQUcscUJBQXFCLENBQUMsTUFBTSxDQUFDOztjQUVwQyxlQUFlLEdBQUcsSUFBSSxJQUFJLElBQUk7UUFFcEMsSUFBSSxlQUFlLEVBQUU7WUFDakIsSUFBSSxDQUFDLGNBQWMsQ0FBQyxJQUFJLENBQUMsQ0FBQztTQUM3QjthQUFNOztnQkFDQyxJQUFJLEdBQUcsSUFBSSxDQUFDLGFBQWEsQ0FBQyxHQUFHLEVBQUU7WUFFbkMsSUFBSSxJQUFJLElBQUksSUFBSSxFQUFFO2dCQUNkLElBQUksR0FBRyxJQUFJLENBQUMsa0JBQWtCLEVBQUUsQ0FBQzthQUNwQztZQUVELElBQUksQ0FBQyxjQUFjLENBQUMsSUFBSSxDQUFDLENBQUM7U0FDN0I7SUFDTCxDQUFDOzs7OztJQUVPLGtCQUFrQjs7WUFDbEIsSUFBSSxHQUFHLE1BQU0sQ0FBQyxTQUFTLENBQUMsU0FBUyxDQUFDLENBQUMsQ0FBQyxNQUFNLENBQUMsU0FBUyxDQUFDLFNBQVMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSTtRQUU1RSxJQUFJLEdBQUcsSUFBSSxJQUFJLE1BQU0sQ0FBQyxTQUFTLENBQUMsUUFBUTtZQUNoQyxNQUFNLENBQUMsU0FBUyxDQUFDLGlCQUFpQixDQUFDO1lBQ25DLE1BQU0sQ0FBQyxTQUFTLENBQUMsY0FBYyxDQUFDLENBQUM7UUFFekMsSUFBSSxJQUFJLENBQUMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxLQUFLLENBQUMsQ0FBQztZQUN4QixJQUFJLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQztRQUU5QixJQUFJLElBQUksQ0FBQyxPQUFPLENBQUMsR0FBRyxDQUFDLEtBQUssQ0FBQyxDQUFDO1lBQ3hCLElBQUksR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDO1FBRTlCLElBQUksR0FBRyxJQUFJLENBQUMsV0FBVyxFQUFFLENBQUM7O1lBR3RCLEVBQUUsR0FBRyxhQUFhLENBQUMsT0FBTztRQUU5QixRQUFRLElBQUksRUFBRTtZQUNWLEtBQUssbUJBQW1CLENBQUMsT0FBTztnQkFDNUIsRUFBRSxHQUFHLGFBQWEsQ0FBQyxPQUFPLENBQUM7Z0JBQy9CLE1BQU07WUFFTixLQUFLLG1CQUFtQixDQUFDLE1BQU07Z0JBQzNCLEVBQUUsR0FBRyxhQUFhLENBQUMsTUFBTSxDQUFDO2dCQUM5QixNQUFNO1lBRU4sS0FBSyxtQkFBbUIsQ0FBQyxRQUFRO2dCQUM3QixFQUFFLEdBQUcsYUFBYSxDQUFDLFFBQVEsQ0FBQztnQkFDaEMsTUFBTTtTQUNUO1FBRUQsT0FBTyxFQUFFLENBQUM7SUFDZCxDQUFDOzs7OztJQUdELGNBQWMsQ0FBQyxVQUFpQjtRQUM1QixRQUFRLFVBQVUsRUFBRTtZQUNoQixLQUFLLGFBQWEsQ0FBQyxPQUFPO2dCQUN0QixJQUFJLENBQUMsT0FBTyxFQUFFLENBQUM7Z0JBQ25CLE1BQU07WUFFTixLQUFLLGFBQWEsQ0FBQyxNQUFNO2dCQUNyQixJQUFJLENBQUMsTUFBTSxFQUFFLENBQUM7Z0JBQ2xCLE1BQU07WUFFTixLQUFLLGFBQWEsQ0FBQyxRQUFRO2dCQUN2QixJQUFJLENBQUMsUUFBUSxFQUFFLENBQUM7Z0JBQ3BCLE1BQU07WUFFTjtnQkFDSSxJQUFJLENBQUMsT0FBTyxFQUFFLENBQUM7U0FDdEI7SUFDTCxDQUFDOzs7O0lBR0QsT0FBTztRQUNILElBQUksQ0FBQyxZQUFZLEdBQUcsSUFBSSxDQUFDLFVBQVUsRUFBRSxDQUFDO1FBQ3RDLElBQUksQ0FBQyxhQUFhLENBQUMsSUFBSSxDQUFDLGFBQWEsQ0FBQyxPQUFPLENBQUMsQ0FBQztJQUNuRCxDQUFDOzs7O0lBR0QsT0FBTztRQUNILElBQUksQ0FBQyxZQUFZLEdBQUcsSUFBSSxDQUFDLFVBQVUsRUFBRSxDQUFDO1FBQ3RDLElBQUksQ0FBQyxhQUFhLENBQUMsSUFBSSxDQUFDLGFBQWEsQ0FBQyxPQUFPLENBQUMsQ0FBQztJQUNuRCxDQUFDOzs7O0lBR0QsTUFBTTtRQUNGLElBQUksQ0FBQyxZQUFZLEdBQUcsSUFBSSxDQUFDLFNBQVMsRUFBRSxDQUFDO1FBQ3JDLElBQUksQ0FBQyxhQUFhLENBQUMsSUFBSSxDQUFDLGFBQWEsQ0FBQyxNQUFNLENBQUMsQ0FBQztJQUNsRCxDQUFDOzs7O0lBR0QsUUFBUTtRQUNKLElBQUksQ0FBQyxZQUFZLEdBQUcsSUFBSSxDQUFDLFdBQVcsRUFBRSxDQUFDO1FBQ3ZDLElBQUksQ0FBQyxhQUFhLENBQUMsSUFBSSxDQUFDLGFBQWEsQ0FBQyxRQUFRLENBQUMsQ0FBQztJQUNwRCxDQUFDOzs7O0lBR0QsSUFBSTtRQUNBLElBQUksQ0FBQyxZQUFZLEdBQUcsSUFBSSxDQUFDLE9BQU8sRUFBRSxDQUFDO0lBQ3ZDLENBQUM7Ozs7SUFJRCxJQUFJLFNBQVM7UUFDVCxPQUFPLElBQUksQ0FBQyxXQUFXLENBQUMsYUFBYTtZQUM3QixhQUFhLENBQUMsT0FBTyxDQUFDO0lBQ2xDLENBQUM7Ozs7SUFDRCxJQUFJLFNBQVM7UUFDVCxPQUFPLElBQUksQ0FBQyxXQUFXLENBQUMsYUFBYTtZQUM3QixhQUFhLENBQUMsT0FBTyxDQUFDO0lBQ2xDLENBQUM7Ozs7SUFDRCxJQUFJLFFBQVE7UUFDUixPQUFPLElBQUksQ0FBQyxXQUFXLENBQUMsYUFBYTtZQUM3QixhQUFhLENBQUMsTUFBTSxDQUFDO0lBQ2pDLENBQUM7Ozs7SUFDRCxJQUFJLFVBQVU7UUFDVixPQUFPLElBQUksQ0FBQyxXQUFXLENBQUMsYUFBYTtZQUM3QixhQUFhLENBQUMsUUFBUSxDQUFDO0lBQ25DLENBQUM7Ozs7SUFHRCxJQUFJLFdBQVc7UUFDWCxPQUFPLElBQUksQ0FBQyxZQUFZLENBQUM7SUFDN0IsQ0FBQztDQUNKOzs7Ozs7SUF0SUcsK0NBR0U7Ozs7O0lBRUYsOENBQXVCOzs7Ozs7SUFrRnZCLDhEQUFrQzs7Ozs7O0lBTWxDLDhEQUFrQzs7Ozs7O0lBTWxDLDZEQUFpQzs7Ozs7O0lBTWpDLCtEQUFtQzs7Ozs7O0lBS25DLDJEQUErQiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IGdldFVybFBhcmFtZXRlckJ5TmFtZSB9IGZyb20gXCJlYXJuYmV0LWNvbW1vblwiO1xuXG5pbXBvcnQgeyBUcmFuc2xhdGlvbklkLCBJVHJhbnNsYXRpb25CYXNlLCBJVHJhbnNsYXRpb25NYW5hZ2VyIH0gZnJvbSBcIi4vdHJhbnNsYXRpb25cIjtcbmltcG9ydCB7IExvY2FsU3RvcmFnZURhdGEgfSBmcm9tIFwiLi4vdXRpbC9sb2NhbC1zdG9yYWdlLXV0aWxcIjtcblxuXG5jbGFzcyBCcm93c2VyTGFuZ3VhZ2VDb2RlXG57XG4gICAgc3RhdGljIEVOR0xJU0ggPSAnZW4nO1xuICAgIHN0YXRpYyBDSElORVNFID0gJ3poJztcbiAgICBzdGF0aWMgS09SRUFOID0gJ2tvJztcbiAgICBzdGF0aWMgSkFQQU5FU0UgPSAnamEnO1xufVxuXG5cbmV4cG9ydCBhYnN0cmFjdCBjbGFzcyBUcmFuc2xhdGlvbk1hbmFnZXJCYXNlPFQgZXh0ZW5kcyBJVHJhbnNsYXRpb25CYXNlPlxuICAgIGltcGxlbWVudHMgSVRyYW5zbGF0aW9uTWFuYWdlclxue1xuICAgIHByaXZhdGUgc2F2ZWRMYW5ndWFnZTpMb2NhbFN0b3JhZ2VEYXRhPHN0cmluZz4gPSBuZXcgTG9jYWxTdG9yYWdlRGF0YShcbiAgICAgICAgJ3NlbGVjdGVkTGFuZ3VhZ2UnLFxuICAgICAgICBmYWxzZVxuICAgICk7XG4gICAgXG4gICAgcHJpdmF0ZSBfdHJhbnNsYXRpb246VDtcblxuICAgIGNvbnN0cnVjdG9yKCkge1xuICAgIH1cbiAgICBpbml0KCkge1xuICAgICAgICBjb25zdCBsYW5nID0gZ2V0VXJsUGFyYW1ldGVyQnlOYW1lKCdsYW5nJyk7XG5cbiAgICAgICAgY29uc3QgaXNMYW5ndWFnZUluVXJsID0gbGFuZyAhPSBudWxsO1xuXG4gICAgICAgIGlmIChpc0xhbmd1YWdlSW5VcmwpIHtcbiAgICAgICAgICAgIHRoaXMuc2VsZWN0TGFuZ3VhZ2UobGFuZyk7XG4gICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICBsZXQgbGFuZyA9IHRoaXMuc2F2ZWRMYW5ndWFnZS5nZXQoKTtcblxuICAgICAgICAgICAgaWYgKGxhbmcgPT0gbnVsbCkge1xuICAgICAgICAgICAgICAgIGxhbmcgPSB0aGlzLmdldEJyb3dzZXJMYW5ndWFnZSgpO1xuICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICB0aGlzLnNlbGVjdExhbmd1YWdlKGxhbmcpO1xuICAgICAgICB9XG4gICAgfVxuXG4gICAgcHJpdmF0ZSBnZXRCcm93c2VyTGFuZ3VhZ2UoKSB7XG4gICAgICAgIGxldCBsYW5nID0gd2luZG93Lm5hdmlnYXRvci5sYW5ndWFnZXMgPyB3aW5kb3cubmF2aWdhdG9yLmxhbmd1YWdlc1swXSA6IG51bGw7XG5cbiAgICAgICAgbGFuZyA9IGxhbmcgfHwgd2luZG93Lm5hdmlnYXRvci5sYW5ndWFnZSB8fCBcbiAgICAgICAgICAgICAgICB3aW5kb3cubmF2aWdhdG9yWydicm93c2VyTGFuZ3VhZ2UnXSB8fCBcbiAgICAgICAgICAgICAgICB3aW5kb3cubmF2aWdhdG9yWyd1c2VyTGFuZ3VhZ2UnXTtcbiAgICAgICAgXG4gICAgICAgIGlmIChsYW5nLmluZGV4T2YoJy0nKSAhPT0gLTEpXG4gICAgICAgICAgICBsYW5nID0gbGFuZy5zcGxpdCgnLScpWzBdO1xuXG4gICAgICAgIGlmIChsYW5nLmluZGV4T2YoJ18nKSAhPT0gLTEpXG4gICAgICAgICAgICBsYW5nID0gbGFuZy5zcGxpdCgnXycpWzBdO1xuICAgICAgICBcbiAgICAgICAgbGFuZyA9IGxhbmcudG9Mb3dlckNhc2UoKTtcblxuXG4gICAgICAgIGxldCBpZCA9IFRyYW5zbGF0aW9uSWQuRU5HTElTSDtcblxuICAgICAgICBzd2l0Y2ggKGxhbmcpIHtcbiAgICAgICAgICAgIGNhc2UgQnJvd3Nlckxhbmd1YWdlQ29kZS5DSElORVNFOlxuICAgICAgICAgICAgICAgIGlkID0gVHJhbnNsYXRpb25JZC5DSElORVNFO1xuICAgICAgICAgICAgYnJlYWs7XG5cbiAgICAgICAgICAgIGNhc2UgQnJvd3Nlckxhbmd1YWdlQ29kZS5LT1JFQU46XG4gICAgICAgICAgICAgICAgaWQgPSBUcmFuc2xhdGlvbklkLktPUkVBTjtcbiAgICAgICAgICAgIGJyZWFrO1xuXG4gICAgICAgICAgICBjYXNlIEJyb3dzZXJMYW5ndWFnZUNvZGUuSkFQQU5FU0U6XG4gICAgICAgICAgICAgICAgaWQgPSBUcmFuc2xhdGlvbklkLkpBUEFORVNFO1xuICAgICAgICAgICAgYnJlYWs7XG4gICAgICAgIH1cblxuICAgICAgICByZXR1cm4gaWQ7XG4gICAgfVxuICAgIFxuICAgIFxuICAgIHNlbGVjdExhbmd1YWdlKGxhbmd1YWdlSWQ6c3RyaW5nKSB7XG4gICAgICAgIHN3aXRjaCAobGFuZ3VhZ2VJZCkge1xuICAgICAgICAgICAgY2FzZSBUcmFuc2xhdGlvbklkLkNISU5FU0U6XG4gICAgICAgICAgICAgICAgdGhpcy5jaGluZXNlKCk7XG4gICAgICAgICAgICBicmVhaztcblxuICAgICAgICAgICAgY2FzZSBUcmFuc2xhdGlvbklkLktPUkVBTjpcbiAgICAgICAgICAgICAgICB0aGlzLmtvcmVhbigpO1xuICAgICAgICAgICAgYnJlYWs7XG5cbiAgICAgICAgICAgIGNhc2UgVHJhbnNsYXRpb25JZC5KQVBBTkVTRTpcbiAgICAgICAgICAgICAgICB0aGlzLmphcGFuZXNlKCk7XG4gICAgICAgICAgICBicmVhaztcblxuICAgICAgICAgICAgZGVmYXVsdDpcbiAgICAgICAgICAgICAgICB0aGlzLmVuZ2xpc2goKTtcbiAgICAgICAgfVxuICAgIH1cblxuXG4gICAgZW5nbGlzaCgpIHtcbiAgICAgICAgdGhpcy5fdHJhbnNsYXRpb24gPSB0aGlzLmdldEVuZ2xpc2goKTtcbiAgICAgICAgdGhpcy5zYXZlZExhbmd1YWdlLnNhdmUoVHJhbnNsYXRpb25JZC5FTkdMSVNIKTtcbiAgICB9XG4gICAgcHJvdGVjdGVkIGFic3RyYWN0IGdldEVuZ2xpc2goKTpUO1xuXG4gICAgY2hpbmVzZSgpIHtcbiAgICAgICAgdGhpcy5fdHJhbnNsYXRpb24gPSB0aGlzLmdldENoaW5lc2UoKTtcbiAgICAgICAgdGhpcy5zYXZlZExhbmd1YWdlLnNhdmUoVHJhbnNsYXRpb25JZC5DSElORVNFKTtcbiAgICB9XG4gICAgcHJvdGVjdGVkIGFic3RyYWN0IGdldENoaW5lc2UoKTpUO1xuXG4gICAga29yZWFuKCkge1xuICAgICAgICB0aGlzLl90cmFuc2xhdGlvbiA9IHRoaXMuZ2V0S29yZWFuKCk7XG4gICAgICAgIHRoaXMuc2F2ZWRMYW5ndWFnZS5zYXZlKFRyYW5zbGF0aW9uSWQuS09SRUFOKTtcbiAgICB9XG4gICAgcHJvdGVjdGVkIGFic3RyYWN0IGdldEtvcmVhbigpOlQ7XG5cbiAgICBqYXBhbmVzZSgpIHtcbiAgICAgICAgdGhpcy5fdHJhbnNsYXRpb24gPSB0aGlzLmdldEphcGFuZXNlKCk7XG4gICAgICAgIHRoaXMuc2F2ZWRMYW5ndWFnZS5zYXZlKFRyYW5zbGF0aW9uSWQuSkFQQU5FU0UpO1xuICAgIH1cbiAgICBwcm90ZWN0ZWQgYWJzdHJhY3QgZ2V0SmFwYW5lc2UoKTpUO1xuXG4gICAgbHlueCgpIHtcbiAgICAgICAgdGhpcy5fdHJhbnNsYXRpb24gPSB0aGlzLmdldEx5bngoKTtcbiAgICB9XG4gICAgcHJvdGVjdGVkIGFic3RyYWN0IGdldEx5bngoKTpUO1xuXG5cbiAgICBnZXQgaXNFbmdsaXNoKCkge1xuICAgICAgICByZXR1cm4gdGhpcy50cmFuc2xhdGlvbi50cmFuc2xhdGlvbklkID09XG4gICAgICAgICAgICAgICAgVHJhbnNsYXRpb25JZC5FTkdMSVNIO1xuICAgIH1cbiAgICBnZXQgaXNDaGluZXNlKCkge1xuICAgICAgICByZXR1cm4gdGhpcy50cmFuc2xhdGlvbi50cmFuc2xhdGlvbklkID09XG4gICAgICAgICAgICAgICAgVHJhbnNsYXRpb25JZC5DSElORVNFO1xuICAgIH1cbiAgICBnZXQgaXNLb3JlYW4oKSB7XG4gICAgICAgIHJldHVybiB0aGlzLnRyYW5zbGF0aW9uLnRyYW5zbGF0aW9uSWQgPT1cbiAgICAgICAgICAgICAgICBUcmFuc2xhdGlvbklkLktPUkVBTjtcbiAgICB9XG4gICAgZ2V0IGlzSmFwYW5lc2UoKSB7XG4gICAgICAgIHJldHVybiB0aGlzLnRyYW5zbGF0aW9uLnRyYW5zbGF0aW9uSWQgPT1cbiAgICAgICAgICAgICAgICBUcmFuc2xhdGlvbklkLkpBUEFORVNFO1xuICAgIH1cblxuXG4gICAgZ2V0IHRyYW5zbGF0aW9uKCkge1xuICAgICAgICByZXR1cm4gdGhpcy5fdHJhbnNsYXRpb247XG4gICAgfVxufSJdfQ==