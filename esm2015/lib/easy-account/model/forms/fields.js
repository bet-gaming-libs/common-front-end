/**
 * @fileoverview added by tsickle
 * Generated from: lib/easy-account/model/forms/fields.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { trimString } from "earnbet-common";
export class StringValue {
    /**
     * @param {?} minimumLength
     * @param {?=} maximumLength
     * @param {?=} shouldTrim
     */
    constructor(minimumLength, maximumLength = 256, shouldTrim = false) {
        this.minimumLength = minimumLength;
        this.maximumLength = maximumLength;
        this.shouldTrim = shouldTrim;
        this.value = '';
        this._isInvalid = false;
    }
    /**
     * @return {?}
     */
    validate() {
        this._isInvalid = false;
        if (this.shouldTrim) {
            this.value = trimString(this.value);
        }
        /** @type {?} */
        const isValid = this.value.length >= this.minimumLength &&
            this.value.length <= this.maximumLength;
        this._isInvalid = !isValid;
        return isValid;
    }
    /**
     * @return {?}
     */
    get isInvalid() {
        return this._isInvalid;
    }
}
if (false) {
    /** @type {?} */
    StringValue.prototype.value;
    /**
     * @type {?}
     * @private
     */
    StringValue.prototype._isInvalid;
    /** @type {?} */
    StringValue.prototype.minimumLength;
    /** @type {?} */
    StringValue.prototype.maximumLength;
    /** @type {?} */
    StringValue.prototype.shouldTrim;
}
export class RepeatedValue extends StringValue {
    /**
     * @param {?=} minimumLength
     */
    constructor(minimumLength = 1) {
        super(minimumLength);
        this.copy = '';
    }
    /**
     * @return {?}
     */
    validate() {
        return super.validate() &&
            this.value == this.copy;
    }
}
if (false) {
    /** @type {?} */
    RepeatedValue.prototype.copy;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZmllbGRzLmpzIiwic291cmNlUm9vdCI6Im5nOi8vZWFybmJldC1jb21tb24tZnJvbnQtZW5kLyIsInNvdXJjZXMiOlsibGliL2Vhc3ktYWNjb3VudC9tb2RlbC9mb3Jtcy9maWVsZHMudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7QUFBQSxPQUFPLEVBQUUsVUFBVSxFQUFFLE1BQU0sZ0JBQWdCLENBQUM7QUFHNUMsTUFBTSxPQUFPLFdBQVc7Ozs7OztJQU1wQixZQUNhLGFBQW9CLEVBQ3BCLGdCQUF1QixHQUFHLEVBQzFCLGFBQXFCLEtBQUs7UUFGMUIsa0JBQWEsR0FBYixhQUFhLENBQU87UUFDcEIsa0JBQWEsR0FBYixhQUFhLENBQWE7UUFDMUIsZUFBVSxHQUFWLFVBQVUsQ0FBZ0I7UUFQdkMsVUFBSyxHQUFHLEVBQUUsQ0FBQztRQUVILGVBQVUsR0FBRyxLQUFLLENBQUM7SUFPM0IsQ0FBQzs7OztJQUVELFFBQVE7UUFFSixJQUFJLENBQUMsVUFBVSxHQUFHLEtBQUssQ0FBQztRQUV4QixJQUFJLElBQUksQ0FBQyxVQUFVLEVBQUU7WUFDakIsSUFBSSxDQUFDLEtBQUssR0FBRyxVQUFVLENBQ25CLElBQUksQ0FBQyxLQUFLLENBQ2IsQ0FBQztTQUNMOztjQUVLLE9BQU8sR0FDVCxJQUFJLENBQUMsS0FBSyxDQUFDLE1BQU0sSUFBSSxJQUFJLENBQUMsYUFBYTtZQUN2QyxJQUFJLENBQUMsS0FBSyxDQUFDLE1BQU0sSUFBSSxJQUFJLENBQUMsYUFBYTtRQUUzQyxJQUFJLENBQUMsVUFBVSxHQUFHLENBQUMsT0FBTyxDQUFDO1FBRTNCLE9BQU8sT0FBTyxDQUFDO0lBQ25CLENBQUM7Ozs7SUFFRCxJQUFJLFNBQVM7UUFDVCxPQUFPLElBQUksQ0FBQyxVQUFVLENBQUM7SUFDM0IsQ0FBQztDQUNKOzs7SUFqQ0csNEJBQVc7Ozs7O0lBRVgsaUNBQTJCOztJQUd2QixvQ0FBNkI7O0lBQzdCLG9DQUFtQzs7SUFDbkMsaUNBQW1DOztBQTZCM0MsTUFBTSxPQUFPLGFBQWMsU0FBUSxXQUFXOzs7O0lBSTFDLFlBQVksYUFBYSxHQUFHLENBQUM7UUFDekIsS0FBSyxDQUFDLGFBQWEsQ0FBQyxDQUFDO1FBSHpCLFNBQUksR0FBRyxFQUFFLENBQUM7SUFJVixDQUFDOzs7O0lBRUQsUUFBUTtRQUNKLE9BQU8sS0FBSyxDQUFDLFFBQVEsRUFBRTtZQUNmLElBQUksQ0FBQyxLQUFLLElBQUksSUFBSSxDQUFDLElBQUksQ0FBQztJQUNwQyxDQUFDO0NBQ0o7OztJQVZHLDZCQUFVIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgdHJpbVN0cmluZyB9IGZyb20gXCJlYXJuYmV0LWNvbW1vblwiO1xuXG5cbmV4cG9ydCBjbGFzcyBTdHJpbmdWYWx1ZVxue1xuICAgIHZhbHVlID0gJyc7XG5cbiAgICBwcml2YXRlIF9pc0ludmFsaWQgPSBmYWxzZTtcblxuICAgIGNvbnN0cnVjdG9yKFxuICAgICAgICByZWFkb25seSBtaW5pbXVtTGVuZ3RoOm51bWJlcixcbiAgICAgICAgcmVhZG9ubHkgbWF4aW11bUxlbmd0aDpudW1iZXIgPSAyNTYsXG4gICAgICAgIHJlYWRvbmx5IHNob3VsZFRyaW06Ym9vbGVhbiA9IGZhbHNlXG4gICAgKSB7XG4gICAgfVxuXG4gICAgdmFsaWRhdGUoKTpib29sZWFuXG4gICAge1xuICAgICAgICB0aGlzLl9pc0ludmFsaWQgPSBmYWxzZTtcblxuICAgICAgICBpZiAodGhpcy5zaG91bGRUcmltKSB7XG4gICAgICAgICAgICB0aGlzLnZhbHVlID0gdHJpbVN0cmluZyhcbiAgICAgICAgICAgICAgICB0aGlzLnZhbHVlXG4gICAgICAgICAgICApO1xuICAgICAgICB9XG5cbiAgICAgICAgY29uc3QgaXNWYWxpZCA9IFxuICAgICAgICAgICAgdGhpcy52YWx1ZS5sZW5ndGggPj0gdGhpcy5taW5pbXVtTGVuZ3RoICYmXG4gICAgICAgICAgICB0aGlzLnZhbHVlLmxlbmd0aCA8PSB0aGlzLm1heGltdW1MZW5ndGhcbiAgICAgICAgXG4gICAgICAgIHRoaXMuX2lzSW52YWxpZCA9ICFpc1ZhbGlkO1xuXG4gICAgICAgIHJldHVybiBpc1ZhbGlkO1xuICAgIH1cblxuICAgIGdldCBpc0ludmFsaWQoKSB7XG4gICAgICAgIHJldHVybiB0aGlzLl9pc0ludmFsaWQ7XG4gICAgfVxufVxuXG5cbmV4cG9ydCBjbGFzcyBSZXBlYXRlZFZhbHVlIGV4dGVuZHMgU3RyaW5nVmFsdWVcbntcbiAgICBjb3B5ID0gJyc7XG5cbiAgICBjb25zdHJ1Y3RvcihtaW5pbXVtTGVuZ3RoID0gMSkge1xuICAgICAgICBzdXBlcihtaW5pbXVtTGVuZ3RoKTtcbiAgICB9XG5cbiAgICB2YWxpZGF0ZSgpOmJvb2xlYW4ge1xuICAgICAgICByZXR1cm4gc3VwZXIudmFsaWRhdGUoKSAmJlxuICAgICAgICAgICAgICAgIHRoaXMudmFsdWUgPT0gdGhpcy5jb3B5O1xuICAgIH1cbn0iXX0=