/**
 * @fileoverview added by tsickle
 * Generated from: lib/easy-account/model/existing-account.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { easyAccountDataKey } from "./data";
export class ExistingEasyAccount {
    constructor() {
        /*
        setInterval(
            this.getDataFromChain,
            1000*60
        );
        */
    }
    /**
     * @return {?}
     */
    load() {
        this.localData = undefined;
        /** @type {?} */
        const savedAccountData = localStorage.getItem(easyAccountDataKey);
        if (savedAccountData != null) {
            this.localData = JSON.parse(savedAccountData);
        }
    }
    /*
        login(password:string):boolean {
            if (!this.isCreated) {
                return false;
            }
    
            this.password = password;
    
            const derivedPublicKey = eos_ecc.privateToPublic(
                this.privateKey
            );
    
            const isAuthenticated =
                derivedPublicKey == this.publicKey;
    
            this._isLoggedIn = isAuthenticated;
    
            return isAuthenticated;
        }
        */
    /**
     * @return {?}
     */
    login() {
        if (!this.isCreated) {
            return;
        }
        this.localData.isLoggedIn = true;
        this.save();
    }
    /**
     * @return {?}
     */
    logout() {
        if (!this.isCreated) {
            return;
        }
        this.localData.isLoggedIn = false;
        this.save();
    }
    /**
     * @private
     * @return {?}
     */
    save() {
        if (!this.isCreated) {
            return;
        }
        localStorage.setItem(easyAccountDataKey, JSON.stringify(this.localData));
    }
    /**
     * @return {?}
     */
    get eosBalance() {
        return this.onChainData ?
            this.onChainData.eos_balance :
            '0.0000 EOS';
    }
    /**
     * @return {?}
     */
    get id() {
        return this.isCreated ?
            this.localData.id :
            0;
    }
    /**
     * @return {?}
     */
    get name() {
        return this.localData ?
            this.localData.name :
            '';
    }
    /**
     * @return {?}
     */
    get publicKey() {
        return this.localData ?
            this.localData.pub :
            '';
    }
    /*
        get privateKey():string {
            if (!this.isCreated) {
                return '';
            }
    
            return AES.decrypt(
                this.encryptedPrivateKey,
                this.password
            );
        }
        private get encryptedPrivateKey() {
            return this.localData ?
                    this.localData.priv :
                    '';
        }
        */
    /**
     * @return {?}
     */
    get privateKey() {
        return this.localData ?
            this.localData.priv :
            '';
    }
    /**
     * @return {?}
     */
    get isLoggedIn() {
        return this.isCreated ?
            this.localData.isLoggedIn :
            false;
    }
    /**
     * @return {?}
     */
    get isCreated() {
        return this.localData != undefined &&
            this.localData.name != undefined;
    }
}
if (false) {
    /**
     * @type {?}
     * @private
     */
    ExistingEasyAccount.prototype.localData;
    /**
     * @type {?}
     * @private
     */
    ExistingEasyAccount.prototype.onChainData;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZXhpc3RpbmctYWNjb3VudC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL2Vhcm5iZXQtY29tbW9uLWZyb250LWVuZC8iLCJzb3VyY2VzIjpbImxpYi9lYXN5LWFjY291bnQvbW9kZWwvZXhpc3RpbmctYWNjb3VudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7OztBQUFBLE9BQU8sRUFBRSxrQkFBa0IsRUFBMEMsTUFDNUQsUUFBUSxDQUFDO0FBR2xCLE1BQU0sT0FBTyxtQkFBbUI7SUFPNUI7UUFDSTs7Ozs7VUFLRTtJQUNOLENBQUM7Ozs7SUFFRCxJQUFJO1FBQ0EsSUFBSSxDQUFDLFNBQVMsR0FBRyxTQUFTLENBQUM7O2NBR3JCLGdCQUFnQixHQUNsQixZQUFZLENBQUMsT0FBTyxDQUFDLGtCQUFrQixDQUFDO1FBRTVDLElBQUksZ0JBQWdCLElBQUksSUFBSSxFQUFFO1lBQzFCLElBQUksQ0FBQyxTQUFTLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FDdkIsZ0JBQWdCLENBQ25CLENBQUM7U0FDTDtJQUNMLENBQUM7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztJQXNCRCxLQUFLO1FBQ0QsSUFBSSxDQUFDLElBQUksQ0FBQyxTQUFTLEVBQUU7WUFDakIsT0FBTztTQUNWO1FBR0QsSUFBSSxDQUFDLFNBQVMsQ0FBQyxVQUFVLEdBQUcsSUFBSSxDQUFDO1FBQ2pDLElBQUksQ0FBQyxJQUFJLEVBQUUsQ0FBQztJQUNoQixDQUFDOzs7O0lBQ0QsTUFBTTtRQUNGLElBQUksQ0FBQyxJQUFJLENBQUMsU0FBUyxFQUFFO1lBQ2pCLE9BQU87U0FDVjtRQUdELElBQUksQ0FBQyxTQUFTLENBQUMsVUFBVSxHQUFHLEtBQUssQ0FBQztRQUNsQyxJQUFJLENBQUMsSUFBSSxFQUFFLENBQUM7SUFDaEIsQ0FBQzs7Ozs7SUFFTyxJQUFJO1FBQ1IsSUFBSSxDQUFDLElBQUksQ0FBQyxTQUFTLEVBQUU7WUFDakIsT0FBTztTQUNWO1FBR0QsWUFBWSxDQUFDLE9BQU8sQ0FDaEIsa0JBQWtCLEVBQ2xCLElBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxDQUNqQyxDQUFDO0lBQ04sQ0FBQzs7OztJQUdELElBQUksVUFBVTtRQUNWLE9BQU8sSUFBSSxDQUFDLFdBQVcsQ0FBQyxDQUFDO1lBQ2pCLElBQUksQ0FBQyxXQUFXLENBQUMsV0FBVyxDQUFDLENBQUM7WUFDOUIsWUFBWSxDQUFDO0lBQ3pCLENBQUM7Ozs7SUFHRCxJQUFJLEVBQUU7UUFDRixPQUFPLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQztZQUNmLElBQUksQ0FBQyxTQUFTLENBQUMsRUFBRSxDQUFDLENBQUM7WUFDbkIsQ0FBQyxDQUFDO0lBQ2QsQ0FBQzs7OztJQUVELElBQUksSUFBSTtRQUNKLE9BQU8sSUFBSSxDQUFDLFNBQVMsQ0FBQyxDQUFDO1lBQ2YsSUFBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsQ0FBQztZQUNyQixFQUFFLENBQUM7SUFDZixDQUFDOzs7O0lBRUQsSUFBSSxTQUFTO1FBQ1QsT0FBTyxJQUFJLENBQUMsU0FBUyxDQUFDLENBQUM7WUFDZixJQUFJLENBQUMsU0FBUyxDQUFDLEdBQUcsQ0FBQyxDQUFDO1lBQ3BCLEVBQUUsQ0FBQztJQUNmLENBQUM7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztJQW1CRCxJQUFJLFVBQVU7UUFDVixPQUFPLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQztZQUNmLElBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLENBQUM7WUFDckIsRUFBRSxDQUFDO0lBQ2YsQ0FBQzs7OztJQUVELElBQUksVUFBVTtRQUNWLE9BQU8sSUFBSSxDQUFDLFNBQVMsQ0FBQyxDQUFDO1lBQ2YsSUFBSSxDQUFDLFNBQVMsQ0FBQyxVQUFVLENBQUMsQ0FBQztZQUMzQixLQUFLLENBQUM7SUFDbEIsQ0FBQzs7OztJQUVELElBQUksU0FBUztRQUVULE9BQU8sSUFBSSxDQUFDLFNBQVMsSUFBSSxTQUFTO1lBQzFCLElBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxJQUFJLFNBQVMsQ0FBQztJQUM3QyxDQUFDO0NBQ0o7Ozs7OztJQXpJRyx3Q0FBbUM7Ozs7O0lBQ25DLDBDQUF5QyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IGVhc3lBY2NvdW50RGF0YUtleSwgSUVhc3lBY2NvdW50RGF0YSwgSUVhc3lBY2NvdW50VGFibGVSb3cgfSBcbiAgICBmcm9tIFwiLi9kYXRhXCI7XG5cblxuZXhwb3J0IGNsYXNzIEV4aXN0aW5nRWFzeUFjY291bnRcbntcbiAgICAvL3ByaXZhdGUgcGFzc3dvcmQ6c3RyaW5nO1xuXG4gICAgcHJpdmF0ZSBsb2NhbERhdGE6SUVhc3lBY2NvdW50RGF0YTtcbiAgICBwcml2YXRlIG9uQ2hhaW5EYXRhOklFYXN5QWNjb3VudFRhYmxlUm93O1xuXG4gICAgY29uc3RydWN0b3IoKSB7XG4gICAgICAgIC8qXG4gICAgICAgIHNldEludGVydmFsKFxuICAgICAgICAgICAgdGhpcy5nZXREYXRhRnJvbUNoYWluLFxuICAgICAgICAgICAgMTAwMCo2MFxuICAgICAgICApO1xuICAgICAgICAqL1xuICAgIH1cblxuICAgIGxvYWQoKSB7XG4gICAgICAgIHRoaXMubG9jYWxEYXRhID0gdW5kZWZpbmVkO1xuXG5cbiAgICAgICAgY29uc3Qgc2F2ZWRBY2NvdW50RGF0YSA9IFxuICAgICAgICAgICAgbG9jYWxTdG9yYWdlLmdldEl0ZW0oZWFzeUFjY291bnREYXRhS2V5KTtcblxuICAgICAgICBpZiAoc2F2ZWRBY2NvdW50RGF0YSAhPSBudWxsKSB7XG4gICAgICAgICAgICB0aGlzLmxvY2FsRGF0YSA9IEpTT04ucGFyc2UoXG4gICAgICAgICAgICAgICAgc2F2ZWRBY2NvdW50RGF0YVxuICAgICAgICAgICAgKTtcbiAgICAgICAgfVxuICAgIH1cblxuICAgIC8qXG4gICAgbG9naW4ocGFzc3dvcmQ6c3RyaW5nKTpib29sZWFuIHtcbiAgICAgICAgaWYgKCF0aGlzLmlzQ3JlYXRlZCkge1xuICAgICAgICAgICAgcmV0dXJuIGZhbHNlO1xuICAgICAgICB9XG5cbiAgICAgICAgdGhpcy5wYXNzd29yZCA9IHBhc3N3b3JkO1xuXG4gICAgICAgIGNvbnN0IGRlcml2ZWRQdWJsaWNLZXkgPSBlb3NfZWNjLnByaXZhdGVUb1B1YmxpYyhcbiAgICAgICAgICAgIHRoaXMucHJpdmF0ZUtleVxuICAgICAgICApO1xuXG4gICAgICAgIGNvbnN0IGlzQXV0aGVudGljYXRlZCA9IFxuICAgICAgICAgICAgZGVyaXZlZFB1YmxpY0tleSA9PSB0aGlzLnB1YmxpY0tleTtcblxuICAgICAgICB0aGlzLl9pc0xvZ2dlZEluID0gaXNBdXRoZW50aWNhdGVkO1xuXG4gICAgICAgIHJldHVybiBpc0F1dGhlbnRpY2F0ZWQ7XG4gICAgfVxuICAgICovXG4gICAgbG9naW4oKSB7XG4gICAgICAgIGlmICghdGhpcy5pc0NyZWF0ZWQpIHtcbiAgICAgICAgICAgIHJldHVybjtcbiAgICAgICAgfVxuXG5cbiAgICAgICAgdGhpcy5sb2NhbERhdGEuaXNMb2dnZWRJbiA9IHRydWU7XG4gICAgICAgIHRoaXMuc2F2ZSgpO1xuICAgIH1cbiAgICBsb2dvdXQoKSB7XG4gICAgICAgIGlmICghdGhpcy5pc0NyZWF0ZWQpIHtcbiAgICAgICAgICAgIHJldHVybjtcbiAgICAgICAgfVxuXG5cbiAgICAgICAgdGhpcy5sb2NhbERhdGEuaXNMb2dnZWRJbiA9IGZhbHNlO1xuICAgICAgICB0aGlzLnNhdmUoKTtcbiAgICB9XG5cbiAgICBwcml2YXRlIHNhdmUoKSB7XG4gICAgICAgIGlmICghdGhpcy5pc0NyZWF0ZWQpIHtcbiAgICAgICAgICAgIHJldHVybjtcbiAgICAgICAgfVxuXG5cbiAgICAgICAgbG9jYWxTdG9yYWdlLnNldEl0ZW0oXG4gICAgICAgICAgICBlYXN5QWNjb3VudERhdGFLZXksXG4gICAgICAgICAgICBKU09OLnN0cmluZ2lmeSh0aGlzLmxvY2FsRGF0YSlcbiAgICAgICAgKTtcbiAgICB9XG5cbiAgICBcbiAgICBnZXQgZW9zQmFsYW5jZSgpIHtcbiAgICAgICAgcmV0dXJuIHRoaXMub25DaGFpbkRhdGEgP1xuICAgICAgICAgICAgICAgIHRoaXMub25DaGFpbkRhdGEuZW9zX2JhbGFuY2UgOlxuICAgICAgICAgICAgICAgICcwLjAwMDAgRU9TJztcbiAgICB9XG5cblxuICAgIGdldCBpZCgpIHtcbiAgICAgICAgcmV0dXJuIHRoaXMuaXNDcmVhdGVkID9cbiAgICAgICAgICAgICAgICB0aGlzLmxvY2FsRGF0YS5pZCA6XG4gICAgICAgICAgICAgICAgMDtcbiAgICB9XG5cbiAgICBnZXQgbmFtZSgpIHtcbiAgICAgICAgcmV0dXJuIHRoaXMubG9jYWxEYXRhID9cbiAgICAgICAgICAgICAgICB0aGlzLmxvY2FsRGF0YS5uYW1lIDpcbiAgICAgICAgICAgICAgICAnJztcbiAgICB9XG5cbiAgICBnZXQgcHVibGljS2V5KCkge1xuICAgICAgICByZXR1cm4gdGhpcy5sb2NhbERhdGEgP1xuICAgICAgICAgICAgICAgIHRoaXMubG9jYWxEYXRhLnB1YiA6XG4gICAgICAgICAgICAgICAgJyc7XG4gICAgfVxuXG4gICAgLypcbiAgICBnZXQgcHJpdmF0ZUtleSgpOnN0cmluZyB7XG4gICAgICAgIGlmICghdGhpcy5pc0NyZWF0ZWQpIHtcbiAgICAgICAgICAgIHJldHVybiAnJztcbiAgICAgICAgfVxuXG4gICAgICAgIHJldHVybiBBRVMuZGVjcnlwdChcbiAgICAgICAgICAgIHRoaXMuZW5jcnlwdGVkUHJpdmF0ZUtleSxcbiAgICAgICAgICAgIHRoaXMucGFzc3dvcmRcbiAgICAgICAgKTtcbiAgICB9XG4gICAgcHJpdmF0ZSBnZXQgZW5jcnlwdGVkUHJpdmF0ZUtleSgpIHtcbiAgICAgICAgcmV0dXJuIHRoaXMubG9jYWxEYXRhID9cbiAgICAgICAgICAgICAgICB0aGlzLmxvY2FsRGF0YS5wcml2IDpcbiAgICAgICAgICAgICAgICAnJztcbiAgICB9XG4gICAgKi9cbiAgICBnZXQgcHJpdmF0ZUtleSgpIHtcbiAgICAgICAgcmV0dXJuIHRoaXMubG9jYWxEYXRhID9cbiAgICAgICAgICAgICAgICB0aGlzLmxvY2FsRGF0YS5wcml2IDpcbiAgICAgICAgICAgICAgICAnJztcbiAgICB9XG5cbiAgICBnZXQgaXNMb2dnZWRJbigpIHtcbiAgICAgICAgcmV0dXJuIHRoaXMuaXNDcmVhdGVkID9cbiAgICAgICAgICAgICAgICB0aGlzLmxvY2FsRGF0YS5pc0xvZ2dlZEluIDpcbiAgICAgICAgICAgICAgICBmYWxzZTtcbiAgICB9XG5cbiAgICBnZXQgaXNDcmVhdGVkKCk6Ym9vbGVhblxuICAgIHtcbiAgICAgICAgcmV0dXJuIHRoaXMubG9jYWxEYXRhICE9IHVuZGVmaW5lZCAmJlxuICAgICAgICAgICAgICAgIHRoaXMubG9jYWxEYXRhLm5hbWUgIT0gdW5kZWZpbmVkO1xuICAgIH1cbn0iXX0=