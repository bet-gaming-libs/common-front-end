/**
 * @fileoverview added by tsickle
 * Generated from: lib/easy-account/model/account-manager.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { ExistingEasyAccount } from "./existing-account";
export class EasyAccountManager {
    constructor() {
        this.account = new ExistingEasyAccount();
        this.account.load();
    }
}
if (false) {
    /** @type {?} */
    EasyAccountManager.prototype.account;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYWNjb3VudC1tYW5hZ2VyLmpzIiwic291cmNlUm9vdCI6Im5nOi8vZWFybmJldC1jb21tb24tZnJvbnQtZW5kLyIsInNvdXJjZXMiOlsibGliL2Vhc3ktYWNjb3VudC9tb2RlbC9hY2NvdW50LW1hbmFnZXIudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7QUFBQSxPQUFPLEVBQUUsbUJBQW1CLEVBQUUsTUFBTSxvQkFBb0IsQ0FBQztBQUV6RCxNQUFNLE9BQU8sa0JBQWtCO0lBSTNCO1FBRlMsWUFBTyxHQUFHLElBQUksbUJBQW1CLEVBQUUsQ0FBQztRQUd6QyxJQUFJLENBQUMsT0FBTyxDQUFDLElBQUksRUFBRSxDQUFDO0lBQ3hCLENBQUM7Q0FDSjs7O0lBTEcscUNBQTZDIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgRXhpc3RpbmdFYXN5QWNjb3VudCB9IGZyb20gXCIuL2V4aXN0aW5nLWFjY291bnRcIjtcblxuZXhwb3J0IGNsYXNzIEVhc3lBY2NvdW50TWFuYWdlclxue1xuICAgIHJlYWRvbmx5IGFjY291bnQgPSBuZXcgRXhpc3RpbmdFYXN5QWNjb3VudCgpO1xuICAgIFxuICAgIGNvbnN0cnVjdG9yKCkge1xuICAgICAgICB0aGlzLmFjY291bnQubG9hZCgpO1xuICAgIH1cbn0iXX0=