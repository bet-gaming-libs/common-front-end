/**
 * @fileoverview added by tsickle
 * Generated from: lib/util/local-storage-util.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/**
 * @template T
 */
export class LocalStorageData {
    /**
     * @param {?} dataKey
     * @param {?} isObject
     */
    constructor(dataKey, isObject) {
        this.dataKey = dataKey;
        this.isObject = isObject;
    }
    /**
     * @return {?}
     */
    isSaved() {
        return this.get() != null;
    }
    /**
     * @return {?}
     */
    get() {
        /** @type {?} */
        const data = localStorage.getItem(this.dataKey);
        return data != null ?
            (this.isObject ?
                JSON.parse(data) :
                data) :
            null;
    }
    /**
     * @param {?} data
     * @return {?}
     */
    save(data) {
        localStorage.setItem(this.dataKey, (this.isObject ?
            JSON.stringify(data) :
            '' + data));
    }
}
if (false) {
    /**
     * @type {?}
     * @private
     */
    LocalStorageData.prototype.dataKey;
    /**
     * @type {?}
     * @private
     */
    LocalStorageData.prototype.isObject;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibG9jYWwtc3RvcmFnZS11dGlsLmpzIiwic291cmNlUm9vdCI6Im5nOi8vZWFybmJldC1jb21tb24tZnJvbnQtZW5kLyIsInNvdXJjZXMiOlsibGliL3V0aWwvbG9jYWwtc3RvcmFnZS11dGlsLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7O0FBQUEsTUFBTSxPQUFPLGdCQUFnQjs7Ozs7SUFFekIsWUFDWSxPQUFjLEVBQ2QsUUFBZ0I7UUFEaEIsWUFBTyxHQUFQLE9BQU8sQ0FBTztRQUNkLGFBQVEsR0FBUixRQUFRLENBQVE7SUFFNUIsQ0FBQzs7OztJQUVELE9BQU87UUFDSCxPQUFPLElBQUksQ0FBQyxHQUFHLEVBQUUsSUFBSSxJQUFJLENBQUM7SUFDOUIsQ0FBQzs7OztJQUVELEdBQUc7O2NBQ08sSUFBSSxHQUFHLFlBQVksQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQztRQUUvQyxPQUFPLElBQUksSUFBSSxJQUFJLENBQUMsQ0FBQztZQUNiLENBQ0ksSUFBSSxDQUFDLFFBQVEsQ0FBQyxDQUFDO2dCQUNYLElBQUksQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQztnQkFDbEIsSUFBSSxDQUNYLENBQUMsQ0FBQztZQUNILElBQUksQ0FBQztJQUNqQixDQUFDOzs7OztJQUVELElBQUksQ0FBQyxJQUFNO1FBQ1AsWUFBWSxDQUFDLE9BQU8sQ0FDaEIsSUFBSSxDQUFDLE9BQU8sRUFDWixDQUNJLElBQUksQ0FBQyxRQUFRLENBQUMsQ0FBQztZQUNmLElBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQztZQUN0QixFQUFFLEdBQUMsSUFBSSxDQUNWLENBQ0osQ0FBQztJQUNOLENBQUM7Q0FDSjs7Ozs7O0lBL0JPLG1DQUFzQjs7Ozs7SUFDdEIsb0NBQXdCIiwic291cmNlc0NvbnRlbnQiOlsiZXhwb3J0IGNsYXNzIExvY2FsU3RvcmFnZURhdGE8VD5cbntcbiAgICBjb25zdHJ1Y3RvcihcbiAgICAgICAgcHJpdmF0ZSBkYXRhS2V5OnN0cmluZyxcbiAgICAgICAgcHJpdmF0ZSBpc09iamVjdDpib29sZWFuXG4gICAgKSB7XG4gICAgfVxuXG4gICAgaXNTYXZlZCgpOmJvb2xlYW4ge1xuICAgICAgICByZXR1cm4gdGhpcy5nZXQoKSAhPSBudWxsO1xuICAgIH1cblxuICAgIGdldCgpOlQge1xuICAgICAgICBjb25zdCBkYXRhID0gbG9jYWxTdG9yYWdlLmdldEl0ZW0odGhpcy5kYXRhS2V5KTtcblxuICAgICAgICByZXR1cm4gZGF0YSAhPSBudWxsID9cbiAgICAgICAgICAgICAgICAoXG4gICAgICAgICAgICAgICAgICAgIHRoaXMuaXNPYmplY3QgP1xuICAgICAgICAgICAgICAgICAgICAgICAgSlNPTi5wYXJzZShkYXRhKSA6XG4gICAgICAgICAgICAgICAgICAgICAgICBkYXRhXG4gICAgICAgICAgICAgICAgKSA6XG4gICAgICAgICAgICAgICAgbnVsbDtcbiAgICB9XG5cbiAgICBzYXZlKGRhdGE6VCkge1xuICAgICAgICBsb2NhbFN0b3JhZ2Uuc2V0SXRlbShcbiAgICAgICAgICAgIHRoaXMuZGF0YUtleSxcbiAgICAgICAgICAgIChcbiAgICAgICAgICAgICAgICB0aGlzLmlzT2JqZWN0ID9cbiAgICAgICAgICAgICAgICBKU09OLnN0cmluZ2lmeShkYXRhKSA6XG4gICAgICAgICAgICAgICAgJycrZGF0YVxuICAgICAgICAgICAgKVxuICAgICAgICApO1xuICAgIH1cbn0iXX0=