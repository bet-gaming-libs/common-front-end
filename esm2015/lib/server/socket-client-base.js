/**
 * @fileoverview added by tsickle
 * Generated from: lib/server/socket-client-base.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/**
 * @abstract
 */
export class SocketClientBase {
    // CLASS members:
    // INSTANCE members:
    /**
     * @param {?} config
     * @param {?} nameSpace
     */
    constructor(config, nameSpace) {
        this.config = config;
        this.nameSpace = nameSpace;
        this.onConnect = (/**
         * @return {?}
         */
        () => {
            this.onConnected();
        });
        this.onError = (/**
         * @param {?} error
         * @return {?}
         */
        (error) => {
            console.log('onError');
            console.log(error);
        });
        this.onDisconnect = (/**
         * @return {?}
         */
        () => {
            this.onDisconnected();
        });
    }
    // *** ESTABLISH CONNECTION to SocketServer ***
    /**
     * @protected
     * @return {?}
     */
    connectSocket() {
        // connect scoket
        /** @type {?} */
        const url = 'http' +
            (this.config.ssl === true ? 's' : '') +
            '://' + this.config.host + ':' +
            this.config.port + this.nameSpace;
        /** @type {?} */
        const socket = io(url);
        socket.on('connect', this.onConnect);
        socket.on('disconnect', this.onConnect);
        socket.on('connect_error', this.onError);
        socket.on('error', this.onError);
        return socket;
    }
}
if (false) {
    /**
     * @type {?}
     * @private
     */
    SocketClientBase.prototype.onConnect;
    /**
     * @type {?}
     * @private
     */
    SocketClientBase.prototype.onError;
    /**
     * @type {?}
     * @private
     */
    SocketClientBase.prototype.onDisconnect;
    /**
     * @type {?}
     * @private
     */
    SocketClientBase.prototype.config;
    /**
     * @type {?}
     * @private
     */
    SocketClientBase.prototype.nameSpace;
    /**
     * @abstract
     * @protected
     * @return {?}
     */
    SocketClientBase.prototype.onConnected = function () { };
    /**
     * @abstract
     * @protected
     * @return {?}
     */
    SocketClientBase.prototype.onDisconnected = function () { };
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic29ja2V0LWNsaWVudC1iYXNlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vZWFybmJldC1jb21tb24tZnJvbnQtZW5kLyIsInNvdXJjZXMiOlsibGliL3NlcnZlci9zb2NrZXQtY2xpZW50LWJhc2UudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7QUFRQSxNQUFNLE9BQWdCLGdCQUFnQjs7Ozs7OztJQU1sQyxZQUNZLE1BQW9CLEVBQ3BCLFNBQWdCO1FBRGhCLFdBQU0sR0FBTixNQUFNLENBQWM7UUFDcEIsY0FBUyxHQUFULFNBQVMsQ0FBTztRQXVCcEIsY0FBUzs7O1FBQUcsR0FBRyxFQUFFO1lBQ3JCLElBQUksQ0FBQyxXQUFXLEVBQUUsQ0FBQztRQUN2QixDQUFDLEVBQUE7UUFFTyxZQUFPOzs7O1FBQUcsQ0FBQyxLQUFLLEVBQUUsRUFBRTtZQUN4QixPQUFPLENBQUMsR0FBRyxDQUFDLFNBQVMsQ0FBQyxDQUFDO1lBQ3ZCLE9BQU8sQ0FBQyxHQUFHLENBQUMsS0FBSyxDQUFDLENBQUM7UUFDdkIsQ0FBQyxFQUFBO1FBQ08saUJBQVk7OztRQUFHLEdBQUcsRUFBRTtZQUN4QixJQUFJLENBQUMsY0FBYyxFQUFFLENBQUM7UUFDMUIsQ0FBQyxFQUFBO0lBL0JELENBQUM7Ozs7OztJQUdTLGFBQWE7OztjQUdiLEdBQUcsR0FBRyxNQUFNO1lBQ2QsQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLEdBQUcsS0FBSyxJQUFJLENBQUMsQ0FBQyxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDO1lBQ3JDLEtBQUssR0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLElBQUksR0FBQyxHQUFHO1lBQ3RCLElBQUksQ0FBQyxNQUFNLENBQUMsSUFBSSxHQUFHLElBQUksQ0FBQyxTQUFTOztjQUVuQyxNQUFNLEdBQUcsRUFBRSxDQUFDLEdBQUcsQ0FBQztRQUV0QixNQUFNLENBQUMsRUFBRSxDQUFDLFNBQVMsRUFBQyxJQUFJLENBQUMsU0FBUyxDQUFDLENBQUM7UUFDcEMsTUFBTSxDQUFDLEVBQUUsQ0FBQyxZQUFZLEVBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxDQUFDO1FBRXZDLE1BQU0sQ0FBQyxFQUFFLENBQUMsZUFBZSxFQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsQ0FBQztRQUN4QyxNQUFNLENBQUMsRUFBRSxDQUFDLE9BQU8sRUFBQyxJQUFJLENBQUMsT0FBTyxDQUFDLENBQUM7UUFFaEMsT0FBTyxNQUFNLENBQUM7SUFDbEIsQ0FBQztDQWFKOzs7Ozs7SUFaRyxxQ0FFQzs7Ozs7SUFFRCxtQ0FHQzs7Ozs7SUFDRCx3Q0FFQzs7Ozs7SUFsQ0csa0NBQTRCOzs7OztJQUM1QixxQ0FBd0I7Ozs7OztJQTBCNUIseURBQXNDOzs7Ozs7SUFRdEMsNERBQXlDIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHtJU2VydmVyQ29uZmlnIH0gZnJvbSAnZWFybmJldC1jb21tb24nO1xuXG5cblxuZGVjbGFyZSB2YXIgaW87XG5cblxuXG5leHBvcnQgYWJzdHJhY3QgY2xhc3MgU29ja2V0Q2xpZW50QmFzZVxue1xuICAgIC8vIENMQVNTIG1lbWJlcnM6XG4gICAgXG4gICAgXG4gICAgLy8gSU5TVEFOQ0UgbWVtYmVyczpcbiAgICBjb25zdHJ1Y3RvcihcbiAgICAgICAgcHJpdmF0ZSBjb25maWc6SVNlcnZlckNvbmZpZyxcbiAgICAgICAgcHJpdmF0ZSBuYW1lU3BhY2U6c3RyaW5nXG4gICAgKSB7XG4gICAgfVxuXG4gICAgLy8gKioqIEVTVEFCTElTSCBDT05ORUNUSU9OIHRvIFNvY2tldFNlcnZlciAqKipcbiAgICBwcm90ZWN0ZWQgY29ubmVjdFNvY2tldCgpOlNvY2tldElPQ2xpZW50LlNvY2tldFxuICAgIHtcbiAgICAgICAgLy8gY29ubmVjdCBzY29rZXRcbiAgICAgICAgY29uc3QgdXJsID0gJ2h0dHAnK1xuICAgICAgICAgICAgKHRoaXMuY29uZmlnLnNzbCA9PT0gdHJ1ZSA/ICdzJyA6ICcnKSArXG4gICAgICAgICAgICAnOi8vJyt0aGlzLmNvbmZpZy5ob3N0Kyc6JytcbiAgICAgICAgICAgICAgICB0aGlzLmNvbmZpZy5wb3J0ICsgdGhpcy5uYW1lU3BhY2U7XG5cbiAgICAgICAgY29uc3Qgc29ja2V0ID0gaW8odXJsKTtcblxuICAgICAgICBzb2NrZXQub24oJ2Nvbm5lY3QnLHRoaXMub25Db25uZWN0KTtcbiAgICAgICAgc29ja2V0Lm9uKCdkaXNjb25uZWN0Jyx0aGlzLm9uQ29ubmVjdCk7XG4gICAgICAgIFxuICAgICAgICBzb2NrZXQub24oJ2Nvbm5lY3RfZXJyb3InLHRoaXMub25FcnJvcik7XG4gICAgICAgIHNvY2tldC5vbignZXJyb3InLHRoaXMub25FcnJvcik7XG5cbiAgICAgICAgcmV0dXJuIHNvY2tldDtcbiAgICB9XG4gICAgcHJpdmF0ZSBvbkNvbm5lY3QgPSAoKSA9PiB7XG4gICAgICAgIHRoaXMub25Db25uZWN0ZWQoKTtcbiAgICB9XG4gICAgcHJvdGVjdGVkIGFic3RyYWN0IG9uQ29ubmVjdGVkKCk6dm9pZDtcbiAgICBwcml2YXRlIG9uRXJyb3IgPSAoZXJyb3IpID0+IHtcbiAgICAgICAgY29uc29sZS5sb2coJ29uRXJyb3InKTtcbiAgICAgICAgY29uc29sZS5sb2coZXJyb3IpO1xuICAgIH1cbiAgICBwcml2YXRlIG9uRGlzY29ubmVjdCA9ICgpID0+IHtcbiAgICAgICAgdGhpcy5vbkRpc2Nvbm5lY3RlZCgpO1xuICAgIH1cbiAgICBwcm90ZWN0ZWQgYWJzdHJhY3Qgb25EaXNjb25uZWN0ZWQoKTp2b2lkO1xufSJdfQ==