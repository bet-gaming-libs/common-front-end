export interface IEasyAccountDataForApi {
    id: number;
    publicKey: string;
    name: string;
    email: string;
    recaptcha: string;
}
