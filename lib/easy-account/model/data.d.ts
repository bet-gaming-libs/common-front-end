export declare const easyAccountDataKey = "EasyAccountData";
export interface IEasyAccountData {
    id: number;
    pub: string;
    priv: string;
    name: string;
    isLoggedIn: boolean;
}
export interface IEasyAccountTableRow {
    id: string;
    key: string;
    email_backup: number;
    nonce: number;
    bet_balance: string;
    eos_balance: string;
    eos_wagered: string;
    free_account: number;
    free_transfers: number;
    rewards_balance: string;
}
