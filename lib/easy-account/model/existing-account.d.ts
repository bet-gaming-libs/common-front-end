export declare class ExistingEasyAccount {
    private localData;
    private onChainData;
    constructor();
    load(): void;
    login(): void;
    logout(): void;
    private save;
    readonly eosBalance: string;
    readonly id: number;
    readonly name: string;
    readonly publicKey: string;
    readonly privateKey: string;
    readonly isLoggedIn: boolean;
    readonly isCreated: boolean;
}
