export declare class StringValue {
    readonly minimumLength: number;
    readonly maximumLength: number;
    readonly shouldTrim: boolean;
    value: string;
    private _isInvalid;
    constructor(minimumLength: number, maximumLength?: number, shouldTrim?: boolean);
    validate(): boolean;
    readonly isInvalid: boolean;
}
export declare class RepeatedValue extends StringValue {
    copy: string;
    constructor(minimumLength?: number);
    validate(): boolean;
}
