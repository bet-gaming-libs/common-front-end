export declare class LocalStorageData<T> {
    private dataKey;
    private isObject;
    constructor(dataKey: string, isObject: boolean);
    isSaved(): boolean;
    get(): T;
    save(data: T): void;
}
