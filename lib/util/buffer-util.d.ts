export declare function byteArrayToHexString(uint8arr: Uint8Array): string;
export declare function hexStringToByteArray(str: string): Uint8Array;
