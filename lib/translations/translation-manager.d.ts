import { ITranslationBase, ITranslationManager } from "./translation";
export declare abstract class TranslationManagerBase<T extends ITranslationBase> implements ITranslationManager {
    private savedLanguage;
    private _translation;
    constructor();
    init(): void;
    private getBrowserLanguage;
    selectLanguage(languageId: string): void;
    english(): void;
    protected abstract getEnglish(): T;
    chinese(): void;
    protected abstract getChinese(): T;
    korean(): void;
    protected abstract getKorean(): T;
    japanese(): void;
    protected abstract getJapanese(): T;
    lynx(): void;
    protected abstract getLynx(): T;
    readonly isEnglish: boolean;
    readonly isChinese: boolean;
    readonly isKorean: boolean;
    readonly isJapanese: boolean;
    readonly translation: T;
}
