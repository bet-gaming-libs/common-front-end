export declare class TranslationId {
    static ENGLISH: string;
    static CHINESE: string;
    static KOREAN: string;
    static JAPANESE: string;
}
export interface ITranslationBase {
    translationId: string;
    altForFlag: string;
}
export interface ITranslationManager {
    english(): void;
    chinese(): void;
    lynx(): void;
}
