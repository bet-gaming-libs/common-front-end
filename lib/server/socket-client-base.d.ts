/// <reference types="socket.io-client" />
import { IServerConfig } from 'earnbet-common';
export declare abstract class SocketClientBase {
    private config;
    private nameSpace;
    constructor(config: IServerConfig, nameSpace: string);
    protected connectSocket(): SocketIOClient.Socket;
    private onConnect;
    protected abstract onConnected(): void;
    private onError;
    private onDisconnect;
    protected abstract onDisconnected(): void;
}
